//
//  main.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
//
#import <UIKit/UIKit.h>

#import "KPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KPAppDelegate class]));
    }
}
