//
//  KPAppDelegate.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPAppDelegate.h"
#import "KPLoginViewController.h"
#import "KPSingletonClass.h"
#import "KPPlayListViewController.h"
#import "KPVideoViewController.h"
#import "KPSettingViewController.h"
#import "KPCategoryViewController.h"
@implementation KPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self playSoundOnLoading];
    [GPPSignIn sharedInstance].clientID = kClientID;
    [GPPDeepLink setDelegate:(id)self];
    [GPPDeepLink readDeepLinkAfterInstall];
    self.mDeviceToken=@"";
    [SCTwitter initWithConsumerKey:@"Oc2MlyBvkeeir7FPyUjXGwK6P" consumerSecret:@"ryp4kpcs7UFII8qVkKDv9RZ2HLfUlohaZt24fxNL2WFgalzLwc"];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    //Create refrence for Home view controller
    [FBLoginView class];
    KPLoginViewController *loginCintroller = [[KPLoginViewController alloc] init];
    //Create refrence for Navigation Bar controller
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginCintroller];
    //Make window visible
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBarHidden =YES;
    [self.window makeKeyAndVisible];

    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
    {
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
                                    {
                                          [self sessionStateChanged:session state:state error:error];
                                          //return ;
                                          NSLog(@"fb already logged in");
                                      }];
    }
    self.window.rootViewController = self.navigationController;

    NSArray *titles = [NSArray arrayWithObjects:@"Like us on Facebook", @"Share App", @"Upgrade", @"Profile",@"Logout",@"Login", nil];    NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity:[titles count]];
    for (int index=0;index<titles.count;index++) {
        if ([[titles objectAtIndex:index] isEqualToString:@"Profile"]) {
        
            KPSettingViewController *setting=[KPSettingViewController new];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:setting];
            setting.title =[titles objectAtIndex:index];
            [viewControllers addObject:navigationController];
        }
        else
        {
        KPHomeViewController *viewController = [[KPHomeViewController alloc] init];
            viewController.title =[titles objectAtIndex:index];

        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        [viewControllers addObject:navigationController];
        }
    }
    
    
    self.tabListController = [[HHTabListController alloc] initWithViewControllers:viewControllers];
    self.tabListController.titleArray = [NSArray arrayWithArray:titles];

    return YES;
}

-(void)playSoundOnLoading
{
    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    resourcePath = [resourcePath stringByAppendingString:@"/sound.mp3"];
    NSLog(@"Path to play: %@", resourcePath);
    NSError* err;

    //Initialize our player pointing to the path to our resource
   self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:
              [NSURL fileURLWithPath:resourcePath] error:&err];
    
    if( err )
    {
        //bail!
        NSLog(@"Failed with reason: %@", [err localizedDescription]);
    }
    else
    {
        //set our delegate and begin playback
        self.player.delegate = self;
        [self.player play];
        self.player.numberOfLoops = -1;
        self.player.currentTime = 0;
        self.player.volume = 1.0;
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    //self.mDeviceToken = deviceToken;
    
    //Removing the brackets from the device token
    self.mDeviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSLog(@"Push Notification tokenstring is %@",self.mDeviceToken);
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
    NSString* s=[[NSString alloc] initWithFormat:@"%@",error];
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@",s);
    
    // lert because your device will not show log
}
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        [KPSingletonClass sharedObject].userID=[[NSUserDefaults standardUserDefaults]valueForKey:@"user_id"];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        NSLog(@"Session closed");
    }
    
    // Handle errors
    if (error)
    {
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES)
        {
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
        } else
        {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled)
            {
                NSLog(@"User cancelled login");
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession)
            {
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                // [self showMessage:alertText withTitle:alertTitle];
                
            }
            else
            {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{

    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication] || [GPPURLHandler handleURL:url  sourceApplication:sourceApplication annotation:annotation];
    // You can add your app-specific url handling code here if needed
    return wasHandled;
}
+(NSArray*)addTabControllers
{
    
    KPHomeViewController *home=[[KPHomeViewController alloc]init];
    KPPlayListViewController *playList=[[KPPlayListViewController alloc]init];
    playList.playListType=KPNormalPlayListType;
    KPPlayListViewController *favourite=[[KPPlayListViewController alloc]init];
    favourite.playListType=KPFavouriteType;

    UINavigationController *homeNavigation=[[UINavigationController alloc]initWithRootViewController:home];
    UINavigationController *playListNavigation=[[UINavigationController alloc]initWithRootViewController:playList];
    UINavigationController *favNavigation=[[UINavigationController alloc]initWithRootViewController:favourite];
    NSArray *tabContollerArray=[NSArray arrayWithObjects:homeNavigation,playListNavigation,favNavigation, nil];
    
    return tabContollerArray;
}

#pragma mark - GPPDeepLinkDelegate

- (void)didReceiveDeepLink:(GPPDeepLink *)deepLink
{
    // An example to handle the deep link data.
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Deep-link Data"
                          message:[deepLink deepLinkID]
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
    [alert show];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([self.navigationController.topViewController isKindOfClass:[KPVideoViewController class]] && [KPSingletonClass sharedObject].isVideoFullScreen)
    {
        return UIInterfaceOrientationMaskAll;
        //else return UIInterfaceOrientationMaskPortrait;
    }
    else return UIInterfaceOrientationMaskPortrait;
}


@end
