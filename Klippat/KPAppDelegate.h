//
//  KPAppDelegate.h
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import "SCTwitter.h"
#import <AVFoundation/AVFoundation.h>
#import "HHTabListController.h"

@class GTMOAuth2Authentication;
@class HHTabListController;
static NSString * const kClientID =
@"565475071890-bart1ft4uksvlt573k7th98l3p1nd4qt.apps.googleusercontent.com";

@interface KPAppDelegate : UIResponder <UIApplicationDelegate ,AVAudioPlayerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong,nonatomic) NSString *mDeviceToken;
@property(nonatomic,strong)AVAudioPlayer *player;
@property(nonatomic,strong) HHTabListController *tabListController;
+(NSArray*)addTabControllers;
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

@end
