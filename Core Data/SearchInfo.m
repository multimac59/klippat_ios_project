//
//  SearchInfo.m
//  Klippat
//
//  Created by Komal Kumar on 04/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "SearchInfo.h"


@implementation SearchInfo

@dynamic file_id;
@dynamic file_url;
@dynamic file_name;
@dynamic person_name;
@dynamic file_duration;
@dynamic category_id;
@dynamic type;
@dynamic is_favourite;
@end
