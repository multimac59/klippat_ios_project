//
//  KPDownLoadInfo.h
//  Klippat
//
//  Created by Komal Kumar on 05/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPDownLoadInfo : NSObject

@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * file_id;
@property (nonatomic, retain) NSString * file_duration;
@property (nonatomic, retain) NSString * file_name;
@property (nonatomic, retain) NSString * file_url;
@property (nonatomic, retain) NSString * person_name;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * localPath;
@property (nonatomic, retain) NSString * serverURL;
@property (nonatomic, retain) NSString * is_favourite;

@property (nonatomic, retain) NSString * downloadtype;
@property (nonatomic, retain) NSString * user_Id;


@end
