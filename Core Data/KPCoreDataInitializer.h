//
//  KPCoreDataInitializer.h
//  Klippat
//
//  Created by Komal Kumar on 04/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

#import "SearchInfo.h"
#import "KPSearchDataINfo.h"
@class KPDownLoadInfo;
@interface KPCoreDataInitializer : NSObject
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator ;

+(KPCoreDataInitializer *)sharedMySingleton;
-(void)deleteAllRows;
-(NSMutableArray *)fetchSearchInfo;
-(NSMutableArray *)fetchDownLoadInfo;
-(void)initializeTheCoreDataModelClasses;
-(NSMutableArray *)getDistinctDataForDownLoad;
-(BOOL)addSearchInformation:(KPSearchDataINfo *)contactInfo;
-(BOOL)addDownLoadInformation:(KPDownLoadInfo *)contactInfo;
-(BOOL)updateFavStatusForSearch:(KPSearchDataINfo*)info;
-(NSMutableArray *)fetchDataAccordingToFilter:(DownLoadCategoryType)categoryType;
@end
