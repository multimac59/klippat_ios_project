//
//  DownloadInfo.m
//  Klippat
//
//  Created by Komal Kumar on 21/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "DownloadInfo.h"


@implementation DownloadInfo

@dynamic category_id;
@dynamic downloadtype;
@dynamic file_duration;
@dynamic file_id;
@dynamic file_name;
@dynamic file_url;
@dynamic localPath;
@dynamic person_name;
@dynamic type;
@dynamic user_Id;
@dynamic is_favourite;

@end
