//
//  KPCoreDataInitializer.m
//  Klippat
//
//  Created by Komal Kumar on 04/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPUtility.h"
#import "DownloadInfo.h"
#import "KPDownLoadInfo.h"
#import "KPSingletonClass.h"
#import "KPCoreDataInitializer.h"

@implementation KPCoreDataInitializer

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static KPCoreDataInitializer* _sharedMySingleton = nil;

/**
 *  Singleton object for this class
 *
 *  @return shared object
 */
+(KPCoreDataInitializer*)sharedMySingleton
{
    @synchronized([KPCoreDataInitializer class])
    {
        if (!_sharedMySingleton) {
            _sharedMySingleton = [[self alloc] init];
        }
        return _sharedMySingleton;
    }
    return nil;
}
/**
 *  Initialize core data with model class
 */

-(void)initializeTheCoreDataModelClasses {
    NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }
   
}
//1
- (NSManagedObjectContext *) managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return _managedObjectContext;
}
/**
 *  <#Description#>
 *
 *  @param  <# description#>
 *
 *  @return <#return value description#>
 */
//2
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

/**
 *  <#Description#>
 *
 *  @param  <# description#>
 *
 *  @return <#return value description#>
 */
//3
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
	
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"database.sqlite"]];
	
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
        // Error for store creation should be handled in here
    }
	
    return _persistentStoreCoordinator;
    /*
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"Kilppat.sqlite"]];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]initWithManagedObjectModel:[self managedObjectModel]];
   
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
configuration:nil URL:storeUrl options:nil error:&error]) {
        Error for store creation should be handled in here
       
    }
    return _persistentStoreCoordinator;
    */
}
/**
 *  Docment directory path
 *
 *  @return Docment directory path
 */
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(BOOL)addSearchInformation:(KPSearchDataINfo *)contactInfo {
    SearchInfo * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"SearchInfo"
                                                           inManagedObjectContext:self.managedObjectContext];
    //  2
    newEntry.file_url = [NSString stringWithFormat:@"%@", contactInfo.file_url];
    newEntry.file_id = contactInfo.file_id;
    newEntry.person_name = [NSString stringWithFormat:@"%@", contactInfo.person_name];
    newEntry.category_id = [NSString stringWithFormat:@"%@", contactInfo.category_id];
    newEntry.file_duration = [NSString stringWithFormat:@"%@", contactInfo.file_duration];
    newEntry.file_name = [NSString stringWithFormat:@"%@", contactInfo.file_name];
    newEntry.type = [NSString stringWithFormat:@"%@", contactInfo.type];
    newEntry.is_favourite=[NSString stringWithFormat:@"%@", contactInfo.is_favourite];
    //  3
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    return YES;
}


-(BOOL)addDownLoadInformation:(KPDownLoadInfo *)contactInfo {
    DownloadInfo * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"DownloadInfo"
                                                          inManagedObjectContext:self.managedObjectContext];
    //  2
    newEntry.file_url = [NSString stringWithFormat:@"%@", contactInfo.file_url];
    newEntry.file_id = contactInfo.file_id;
    newEntry.person_name = [NSString stringWithFormat:@"%@", contactInfo.person_name];
    newEntry.category_id = [NSString stringWithFormat:@"%@", contactInfo.category_id];
    newEntry.file_duration = [NSString stringWithFormat:@"%@", contactInfo.file_duration];
    newEntry.file_name = [NSString stringWithFormat:@"%@", contactInfo.file_name];
    newEntry.type = [NSString stringWithFormat:@"%@", contactInfo.type];
    newEntry.localPath = [NSString stringWithFormat:@"%@", contactInfo.localPath];
    newEntry.downloadtype = [NSString stringWithFormat:@"%@", contactInfo.downloadtype];
    newEntry.user_Id = [NSString stringWithFormat:@"%@", [[KPSingletonClass sharedObject] userID]];
    newEntry.is_favourite=[NSString stringWithFormat:@"%@", contactInfo.is_favourite];

    //  3
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        return NO;
    }
    return YES;
}

-(BOOL)updateFavStatusForSearch:(KPSearchDataINfo*)info
{
    NSFetchRequest *request=[NSFetchRequest new];
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"SearchInfo" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"category_id==%@",info.category_id];
    [request setPredicate:predicate];
    NSError *error=nil;
    NSArray *searhRecord=[self.managedObjectContext executeFetchRequest:request error:&error];
    SearchInfo *first=[searhRecord firstObject];
    for (SearchInfo *search in searhRecord) {
//        if (([search.type isEqual:@"notes"]||[search.type isEqual:@"photos"])&& [search.file_id isEqualToString:info.file_id]) {
        NSLog(@"info.is_favourite in core data %@", info.is_favourite);
            search.is_favourite = info.is_favourite;
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            }
           // break;
        //}
    }
    NSLog(@"searched Data %@ and %@photos/notes",first,first.category_id);
    return YES;
}
-(NSMutableArray *)fetchDownLoadInfo {
    
    NSLog(@"set predicate %@", [[KPSingletonClass sharedObject] userID]);
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"DownloadInfo" inManagedObjectContext:self.managedObjectContext];
	[request setEntity:entity];
	// Order the events by creation date, most recent first.
	// Execute the fetch -- create a mutable copy of the result.
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_Id == %@",[[KPSingletonClass sharedObject] userID]];
//    [request setPredicate:predicate];
	NSError *error = nil;
	NSMutableArray *temp = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
	// Set self's events array to the mutable array, then clean up.
	return temp;
}

-(NSMutableArray *)fetchSearchInfo {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"SearchInfo" inManagedObjectContext:self.managedObjectContext];
	[request setEntity:entity];
	// Order the events by creation date, most recent first.
	// Execute the fetch -- create a mutable copy of the result.
	NSError *error = nil;
	NSMutableArray *temp = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
	// Set self's events array to the mutable array, then clean up.
	return temp;
}

-(void)deleteAllRows {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"SearchInfo" inManagedObjectContext:self.managedObjectContext];
	[request setEntity:entity];    NSError *error;
    NSArray *objects = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (objects == nil) {
        // handle error
    } else {
        for (NSManagedObject *object in objects) {
            [self.managedObjectContext deleteObject:object];
        }
        [self.managedObjectContext save:&error];
    }
}


-(NSMutableArray *)fetchDataAccordingToFilter:(DownLoadCategoryType)categoryType {
    
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DownloadInfo"
                                              inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"downloadtype == %@AND user_Id == %@",getCategoryType(categoryType),[[KPSingletonClass sharedObject] userID]];
    [fetchRequest setPredicate:predicate];

    NSError *error=nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *obj in fetchedObjects)
    {
        [dataArray addObject:obj];
    }
    //Save changes
    error = nil;
    if (![self.managedObjectContext save:&error])
    {
        NSLog(@"error saving");
    }
    //reload the table view
    return dataArray;
}
-(NSMutableArray *)getDistinctDataForDownLoad
{
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"DownloadInfo"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DownloadInfo" inManagedObjectContext:self.managedObjectContext];
    
    // Required! Unless you set the resultType to NSDictionaryResultType, distinct can't work.
    // All objects in the backing store are implicitly distinct, but two dictionaries can be duplicates.
    // Since you only want distinct names, only ask for the 'name' property.
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"downloadtype"]];
    fetchRequest.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog (@"names: %@",dictionaries);
    NSMutableArray *muteArray = (NSMutableArray *)dictionaries;
    return muteArray;
}

@end
