//
//  SearchInfo.h
//  Klippat
//
//  Created by Komal Kumar on 04/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SearchInfo : NSManagedObject

@property (nonatomic, retain) NSString * file_id;
@property (nonatomic, retain) NSString * file_url;
@property (nonatomic, retain) NSString * file_name;
@property (nonatomic, retain) NSString * person_name;
@property (nonatomic, retain) NSString * file_duration;
@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * is_favourite;

@end
