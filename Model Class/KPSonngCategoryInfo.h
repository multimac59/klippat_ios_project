//
//  KPSonngCategoryInfo.h
//  Klippat
//
//  Created by Komal Kumar on 02/08/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KPSonngCategoryInfo : NSObject

@property (nonatomic, strong) NSString *category_id;
@property (nonatomic, strong) NSString *favourite_status;
@property (nonatomic, strong) NSString *file_duration;
@property (nonatomic, strong) NSString *file_id;
@property (nonatomic, strong) NSString *file_name;
@property (nonatomic, strong) NSString *file_url;
@property (nonatomic, strong) NSString *person_name;
@property (nonatomic, strong) NSString *type;
@property(nonatomic,strong)  NSString *serverURL;

@end
