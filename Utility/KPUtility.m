//
//  KPUtility.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPUtility.h"

/*
 * Function for drawing rect having Stroke width 1px.
 */

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color) {
    
    CGContextSaveGState(context);
    CGContextSetLineCap(context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, startPoint.x + 0.5, startPoint.y + 0.5);
    CGContextAddLineToPoint(context, endPoint.x + 0.5, endPoint.y + 0.5);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

NSString * getCategoryType(DownLoadCategoryType downloadType){
    
    switch (downloadType) {
        case KPVideo:
            return [NSString stringWithFormat:@"%@", @"Video"];
            break;
        case KPImage:
            return [NSString stringWithFormat:@"%@", @"Images"];
            break;
        case KPClips:
            return [NSString stringWithFormat:@"%@", @"Clips"];
            break;
        case KPNotes:
            return [NSString stringWithFormat:@"%@", @"Notes"];
            break;
        default:
            break;
    }
}

DownLoadCategoryType getDownLoadedCategoryType(NSString * extension_value) {
    
    NSString *lookup = [NSString stringWithFormat:@"%@", extension_value]; // The value you want to switch on
    __block BOOL drawType = NO;
    // Squint and this looks like a proper switch block!
    // New ObjC syntax makes the NSDictionary creation cleaner.
    NSDictionary *d = @{
                        @"Video":
                            ^() {
                                drawType =  KPVideo;
                            },
                        @"Images":
                            ^() {
                                drawType = KPImage;
                            },
                        @"Clips":
                            ^() {
                                drawType = KPClips;
                            },
                        @"Notes":
                            ^() {
                                drawType = KPNotes;
                            }
                        };
    CaseBlock c = d[lookup];
    if (c)
        c();
    else {
        NSLog(@"Not Found");
    }
    return drawType;
}
