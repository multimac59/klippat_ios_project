//
//  UIButton+Block.h
//  FitHealthy
//
//  Created by Komal Kumar on 01/04/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//


#define kUIButtonBlockTouchUpInside @"TouchInside"

#import <UIKit/UIKit.h>

@interface UIButton (Block)

@property (nonatomic, strong) NSMutableDictionary *actions;

- (void) setAction:(NSString*)action withBlock:(void(^)())block;

@end