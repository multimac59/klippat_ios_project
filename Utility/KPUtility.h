//
//  KPUtility.h
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    KPVideo=0,
    KPImage=1,
    KPNotes=2,
    KPClips=3,
    
}DownLoadCategoryType;

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint, CGColorRef color) ;

NSString * getCategoryType(DownLoadCategoryType downloadType);

typedef void (^CaseBlock)();

DownLoadCategoryType getDownLoadedCategoryType(NSString * extension_value);