//
//  KPDrawingView.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPDrawingView.h"

@implementation KPDrawingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 * Overriding the drawRect: method for UIView Parent class. Only override drawRect: if you perform custom drawing.
 * We are using this to draw the line, image, text, transparent layer and path
 * An empty implementation adversely affects performance during animation.
 */
-(void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextRetain(context);
    CGColorRef separatorColor = [UIColor colorWithRed:220.0/255.0f green:220.0/255.0f blue:220.0/255.0f alpha:1.0].CGColor;
    CGRect paperRect = self.bounds;
    // Add separator
    CGPoint startPoint = CGPointMake(paperRect.origin.x, paperRect.origin.y);
    CGPoint endPoint = CGPointMake(paperRect.origin.x, paperRect.size.height);
    
	draw1PxStroke(context, startPoint, endPoint, separatorColor);
    CGContextRelease(context);
}


@end
