//
//  KPLanguageManager.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPLanguageManager.h"

NSString *const ANLanguageChangedNotification = @"ANLanguageChangedNotification";

@interface KPLanguageManager ()

@property (strong, nonatomic) NSBundle *bundle;

@end

@implementation KPLanguageManager

@synthesize currentLanguage = _currentLanguage;

#pragma mark - NSObject

- (id)init
{
    self = [super init];
    
    if (self) {
        _currentLanguage = nil;
        _bundle = [NSBundle mainBundle];
    }
    
    return self;
}

#pragma mark - Public

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (void)setCurrentLanguage:(NSString *)currentLanguage
{
    if (_currentLanguage && [currentLanguage isEqualToString:_currentLanguage]) {
        return;
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:currentLanguage ofType:@"lproj"];
    
    if (path) {
        self.bundle = [NSBundle bundleWithPath:path];
        _currentLanguage = [currentLanguage copy];
    } else {
        NSLog(@"Localization for language identifier %@ is not found.", currentLanguage);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ANLanguageChangedNotification object:_currentLanguage];
}

- (NSString *)currentLanguage
{
    if (!_currentLanguage) {
        NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        _currentLanguage = [languages objectAtIndex:0];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:_currentLanguage ofType:@"lproj"];
        
        if (!path) {
            self.currentLanguage = @"en";
            self.bundle = [NSBundle mainBundle];
        }
    }
    return _currentLanguage;
}

- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
    return [self.bundle localizedStringForKey:key value:comment table:nil];
}

@end
