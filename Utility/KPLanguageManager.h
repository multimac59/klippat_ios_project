//
//  KPLanguageManager.h
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Posted when language is changed.
 The object is the current language identifier.
 */
extern NSString *const ANLanguageChangedNotification;

/**
 A singleton object that manages the localization.
 */
@interface KPLanguageManager : NSObject

/**
 The current language used for translation.
 */
@property (copy, nonatomic) NSString *currentLanguage;

/**
 The singleton instance of ANLanguageManager.
 @return The singleton instance of ANLanguageManager.
 */
+ (instancetype)sharedInstance;

/**
 Returns a localized version of the string.
 @param key The key for a string.
 @param value The value to return if key is nil or if a localized string for key can’t be found.
 @return A localized version of the string
 */
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

@end

#define ANLocalizedString(key, comment) \
[[KPLanguageManager sharedInstance] localizedStringForKey:(key) value:@""]
