//
//  KPImageCategoryCell.h
//  Klippat
//
//  Created by Zahid on 17/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPCategoryCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLable;
@property(nonatomic,strong)UILabel *descLable;
@property(nonatomic,strong)UIImageView *thumbnailImage;
@property(nonatomic,strong)UIButton *accessoryButton;

@end
