//
//  KPImageCategoryViewController.m
//  Klippat
//
//  Created by Ravi kumar on 18/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCategoryViewController.h"
#import "KPGridGalleryViewController.h"
#import "KPHomeViewController.h"
#import "HHTabListController.h"


@interface KPCategoryViewController ()
@property(nonatomic,strong)NSMutableArray *filteredArray;
@property(nonatomic)BOOL isSearchActive;
@end

@implementation KPCategoryViewController
@synthesize tableView=_tableView;
@synthesize categoriesArray=_categoriesArray;
-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    return _tableView;
}
-(NSMutableArray*)filteredArray
{
    if (!_filteredArray) {
        _filteredArray=[NSMutableArray new];
    }
    return _filteredArray;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(NSMutableArray*)imageArray
{
    if (!_categoriesArray)
    {
        _categoriesArray=[[NSMutableArray alloc]init];
        for (int i=0; i<14; i++)
        {
            //[_categoriesArray addObject:@"Categaries 1"];
        }
    }
    return _categoriesArray;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getCategoriesFromServer:NO];// server response
    //[self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?125:122, self.view.frame.size.width-10, 378*deviceFactor)];
    [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?75:75, self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?75:75))];
    [self.tableView setTag:1313];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    if ([UIDevice currentDevice].systemVersion.floatValue>=7)
    {
        self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:self.tableView];
    //[self addBottomBarWithHeadfoneIcon];
    [self addTopView];
    [self addBottomView];
    //[self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];//navigation bar on the bottom side
    //[self addCustomSearchBar];//search TextField
    
    //[self addGridViewWithImages];
    // Do any additional setup after loading the view.
}

-(void)addBottomView
{
    UIImageView * bottomImage = [[UIImageView alloc]init];
    [bottomImage setFrame:CGRectMake(0.0, self.view.frame.size.height-120, 320.0, 120.0)];
    [bottomImage setImage:[UIImage imageNamed:@"blur-footer"]];
    [self.view addSubview:bottomImage];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(klipsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-80, 60.0, 60.0)];
    [button setImage:[UIImage imageNamed:@"klip"] forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(IBAction)klipsButtonAction:(id)sender
{
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
    //[[self getPlayerView] removeFromSuperview];
    KPHomeViewController *vc;
    for(id controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[KPHomeViewController class]])
        {
            vc=(KPHomeViewController*)controller;
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    if (!vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
    }
}
/*
 type=movies or clips or notes or photos and language_type=english or arabic
 
 */
-(void)getCategoriesFromServer:(BOOL)flag
{
    NSString *type;
    showActivity(self.navigationController.view);
    if (self.categoryType==KPCategoryTypeImage)
    {
        type=@"photos";
    }
    else if(self.categoryType==KPCategoryTypeMusic)
    {
        type=@"clips";
    }
    else if(self.categoryType==KPCategoryTypeNotes)
    {
    type=@"notes";
    }
    else
        type=@"movies";
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"show_category_list" forKeyPath:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].language forKeyPath:@"language_type"];
    [infoDict setValue:type forKeyPath:@"type"];
[[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPGetCategoryListType responseBlock:^(id response, NSError *error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (!response)
            {
                if (!flag)
                {
                    [self getCategoriesFromServer:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
                [self.tableView  reloadData];
            }
            else
            {
                NSDictionary *responseDict=[response valueForKey:@"response"];
                if ([[responseDict valueForKey:@"success"]isEqualToString:@"true"])
                {
                    self.categoriesArray =[responseDict valueForKey:@"category_list"];
                    NSLog(@"categoreies %@",self.categoriesArray);
                    [self.tableView  reloadData];
                }
            }
        });
    NSLog(@"getCategoriesFromServer response--%@",response);
}];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addLocalNavigationBar:(CGRect)frame
{
    //[self addTopView];
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow"];
    UIImage *arrow1 = [UIImage imageNamed:@""];
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:arrow1 forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2-16, arrow.size.width , arrow.size.height);
    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    [self.view addSubview:viewNavigation];
}

-(IBAction)arrowAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"back_blue"];
    UIImage * searchImage = [UIImage imageNamed:@"search_1"];
    NSString *titleString;
    if (self.categoryType==KPCategoryTypeImage)
    {
        titleString=@"PICTURES";
    }
    else  if (self.categoryType==KPCategoryTypeNotes)
    {
        titleString=@"QUOTES";
    }
    else  if (self.categoryType==KPCategoryTypeVideo)
    {
        titleString=@"MOVIE KLIPS";
    }
    else
    {
        titleString=@"MUSIC KLIPS";
    }
    
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 11, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchDown];
    
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:titleString];
    [titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:14.0/255 green:26.0/255 blue:93.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    
    UIButton * searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton addTarget:self action:@selector(searchButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [searchButton setBackgroundImage:searchImage forState:UIControlStateNormal];
    [searchButton setFrame:CGRectMake(280.0, 13.0, searchImage.size.width, searchImage.size.height)];
    [topStripImgView addSubview:searchButton];
    
    
    
    [self.view addSubview:topStripImgView];
}

-(IBAction)searchButton:(UIButton*)sender
{
    NSLog(@"Search Icon");
    sender.selected=!sender.selected;
    if (sender.selected)
    {
        [self addCustomSearchBar];
        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?110:110, self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?110:110))];
    }
    else
    {
        UITextField *searchTF=(UITextField*)[self.view viewWithTag:8934];
        [searchTF removeFromSuperview];
        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?75:75, self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?75:75))];
    }
    
}

-(void)addBottomBarWithHeadfoneIcon
{
    UIImage *bottomBG=[UIImage imageNamed:@"blur-footer"];
    UIImageView *bottomImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-bottomBG.size.height ,self.view.frame.size.width , bottomBG.size.height)];
    [bottomImage setImage:bottomBG];
    [self.view addSubview:bottomImage];

    UIButton *headfoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *headFone=[UIImage imageNamed:@"klipatt_headPhone"];
    [headfoneBtn setFrame:CGRectMake(self.view.frame.size.width/2-headFone.size.width/2, bottomBG.size.height/2-headFone.size.height/2+24, headFone.size.width, headFone.size.height)];
    [headfoneBtn setImage:headFone forState:UIControlStateNormal];
    [headfoneBtn addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomImage addSubview:headfoneBtn];
    [bottomImage bringSubviewToFront:headfoneBtn];
    [self.view bringSubviewToFront:bottomImage];
}

-(IBAction)homeButtonAction:(id)sender
{
    NSLog(@"home");
    [self.navigationController popViewControllerAnimated:YES];
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
    HHTabListController *vc;
    for(id controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[HHTabListController class]]) {
            vc=(HHTabListController*)controller;
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    if (!vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
}
-(void)addCustomSearchBar
{
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:CGRectMake(0, 75,self.view.frame.size.width, 30)];
    [search_txtF setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [search_txtF setTag:8934];
    [search_txtF setDelegate:(id)self];
    UIColor *color =[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color,NSFontAttributeName:[UIFont italicSystemFontOfSize:12.0]}];
    UIImage *searchImage=[UIImage imageNamed:@"search"];

    [search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];

    [search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
    search_txtF.textColor=[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];

    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 25)];
    
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 6, searchImage.size.width, searchImage.size.height)];
    [searchIcon setImage:searchImage];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    
    [self.view addSubview:search_txtF];
}
-(IBAction)backButtonAction :(id)sender
{
    NSLog(@"backButtonAction");
    [self.navigationController popViewControllerAnimated:YES];
    //[self navigateToHomeScreen];
}
-(void)navigateToHomeScreen
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPHONE_5?70.5:57.14;//63
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld row selected",(long)indexPath.row+1);
    NSMutableArray *infoArray=[NSMutableArray arrayWithArray:(!self.isSearchActive)?self.categoriesArray:self.filteredArray] ;
    NSDictionary *infoDict=[infoArray objectAtIndex:indexPath.row];
    KPCategoryCell *cell=(KPCategoryCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (self.categoryType==KPCategoryTypeImage) {
        KPGridGalleryViewController *obj=[KPGridGalleryViewController new];
        [obj setAllCategoryArray:[NSMutableArray arrayWithArray:infoArray]];
        obj.categoryName=[cell.titleLable.text uppercaseString];
        obj.gridViewType=KPGrideViewImagesType;
        obj.selectedIndex = indexPath.row;
        obj.category_id=[infoDict valueForKey:@"category_id"];
        [self.navigationController pushViewController:obj animated:YES];
    }
    else if (self.categoryType==KPCategoryTypeMusic) {
        KPSongListViewController *vc=[[KPSongListViewController alloc]init];
        vc.categoryTitleText=[cell.titleLable.text uppercaseString];
        vc.songListType=KPSongListNormal;
        vc.downLoadcategoryType = KPVideo;
        vc.rootCategoryTitle=@"MUSIC KLIPS";
        vc.allCategoryArray=[NSMutableArray arrayWithArray:infoArray];
        vc.selectedIndex=indexPath.row;
        vc.category_id=[infoDict valueForKey:@"category_id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.categoryType==KPCategoryTypeVideo) {
        KPSongListViewController *vc=[[KPSongListViewController alloc]init];
        vc.categoryTitleText=[cell.titleLable.text uppercaseString];
        vc.songListType=KPSongListNormal;
        vc.downLoadcategoryType = KPVideo;
        vc.allCategoryArray=[NSMutableArray arrayWithArray:infoArray];
        vc.rootCategoryTitle=@"MOVIE KLIPS";
        vc.selectedIndex=indexPath.row;
        vc.category_id=[infoDict valueForKey:@"category_id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else  if (self.categoryType==KPCategoryTypeNotes)
    {
        KPGridGalleryViewController *obj=[KPGridGalleryViewController new];
        obj.gridViewType=KPGrideViewQuotesType;
        [obj setAllCategoryArray:[NSMutableArray arrayWithArray:infoArray]];
        obj.selectedIndex = indexPath.row;
        obj.categoryName=[cell.titleLable.text uppercaseString];
        obj.category_id=[infoDict valueForKey:@"category_id"];
        [self.navigationController pushViewController:obj animated:YES];
    }
}
-(void)addNoDataFoundLabel
{
    UILabel *label=(UILabel*)[self.view viewWithTag:8493];
    if (label)
    {
        [label removeFromSuperview];
        label=nil;
    }
    label=[[UILabel alloc]initWithFrame:CGRectMake(20, 160, 280, 20)];
    [label setText:@"No Data Found!"];
    [label setTag:8493];
    [label setTextColor:[UIColor redColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [self.view addSubview:label];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount=(!self.isSearchActive)?self.categoriesArray.count:self.filteredArray.count;
    if (rowCount==0 && self.isSearchActive) {
        [self addNoDataFoundLabel];
    }
    else
    {
        UILabel *label=(UILabel*)[self.view viewWithTag:8493];
        if (label)
        {
            [label removeFromSuperview];
            label=nil;
        }
        
    }
    return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPCategoryCell *cell=(KPCategoryCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[KPCategoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    NSDictionary *tempDict=[(!self.isSearchActive)?self.categoriesArray:self.filteredArray objectAtIndex:indexPath.row];
    [cell.textLabel setFrame:CGRectMake(0, 8, 150, 14)];

    
    NSString *thumbnailURLString=@"http://www.examiner.com/images/blog/EXID9626/images/TheLastSongPoster.jpg";
    [cell.thumbnailImage setImageWithURL:[NSURL URLWithString:thumbnailURLString] placeholderImage:nil];
    [cell.titleLable setText:[tempDict valueForKey:@"category_name"]];
    [cell.descLable setText:@"39 Songs | 2 hours 35 minutes"];
    NSString *str1=[[tempDict valueForKey:@"category_name"] substringToIndex:1];
    NSString *result=[str1 uppercaseString];
    NSString *substr=[[tempDict valueForKey:@"category_name"]substringFromIndex:1];
    [cell.titleLable setText:[result stringByAppendingString:substr]];
    
    

    //[cell.titleLable setText:[NSString stringWithFormat:@"Categories %d",indexPath.row+1]];
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *textSearch=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textSearch.length>0) {
        self.isSearchActive=YES;
        [self filterListForText:textSearch];
    }
    else
    {    self.isSearchActive=NO;
        [self.tableView reloadData];
    }
    return YES;
}
-(void)filterListForText:(NSString*)textSearch
{
    [self.filteredArray removeAllObjects];
    for (NSDictionary *tempDict in self.categoriesArray) {
#define kOptions1 (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
        NSComparisonResult result=[[tempDict valueForKey:@"category_name"]compare:textSearch options:kOptions1 range:[[tempDict valueForKey:@"category_name"]rangeOfString:textSearch options:kOptions1]];
        
        if (result==NSOrderedSame) {
            [self.filteredArray addObject:tempDict];
        }
    }
    [self.tableView reloadData];
}
@end