//
//  KPCategoryViewController.h
//  Klippat
//
//  Created by Ravi kumar on 18/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"
#import "KPImagePlayerViewController.h"
#import "KPSongListViewController.h"
#import "KPCategoryCell.h"
typedef enum
{
    KPCategoryTypeImage,
    KPCategoryTypeMusic,
    KPCategoryTypeNotes,
    KPCategoryTypeVideo,
}KPCategoryType;

@interface KPCategoryViewController : KPCommonViewController
@property(nonatomic)KPCategoryType categoryType;
@property(nonatomic,strong)NSMutableArray *categoriesArray;
@property(nonatomic,strong)UITableView *tableView;

@end
