//
//  KPGalleryCell.m
//  Klippat
//
//  Created by Zahid on 17/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCategoryCell.h"

@implementation KPCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *containerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, IS_IPHONE_5?69:56.14)];
        [containerView setBackgroundColor:[UIColor grayColor]];//ck

        self.titleLable =[[UILabel alloc]initWithFrame:CGRectMake(77, IS_IPHONE_5?11:5, 170, 30)];
        [self.titleLable setFont:[UIFont fontWithName:@"Helvetica" size:16]];
        [self.titleLable setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
        self.thumbnailImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 70, IS_IPHONE_5?69:56.14)];
        [containerView addSubview:self.thumbnailImage];
        
        self.descLable=[[UILabel alloc]initWithFrame:CGRectMake(77, IS_IPHONE_5?35:29, 170, 18)];
        [self.descLable setBackgroundColor:[UIColor clearColor]];
        [self.descLable setFont:[UIFont fontWithName:@"Helvetica" size:12]];

        [self.descLable setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
        UIImage *accesoryIcon=[UIImage imageNamed:@"play-button"];
        self.accessoryButton=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.accessoryButton setFrame:IS_IPHONE_5?CGRectMake(self.frame.size.width-accesoryIcon.size.width, 0, accesoryIcon.size.width, accesoryIcon.size.height):CGRectMake(self.frame.size.width-accesoryIcon.size.width, 0, accesoryIcon.size.width, accesoryIcon.size.height-15)];
        [self.accessoryButton setImage:accesoryIcon forState:UIControlStateNormal];
        [containerView addSubview:self.accessoryButton];
        
        [containerView addSubview:self.descLable];
        [containerView addSubview:self.titleLable];
        [self addSubview:containerView];

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
