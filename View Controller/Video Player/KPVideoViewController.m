//
//  KPVideoViewController.m
//  Klippat
//
//  Created by Zahid on 17/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPDownLoadInfo.h"
#import "KPCoreDataInitializer.h"
#import "KPVideoViewController.h"
#import "KPHomeViewController.h"


@interface KPVideoViewController ()
{
    int counter;
    float timeCounter;
    int index;
    BOOL IsRandom;
    UIButton *playPauseBtn;
    UIView *viewNavigation;
    NSString *imageString;
    float prevoiusChangedValue;
    BOOL showAllert;
    BOOL isFirstTime;
    BOOL isFullScreen,isLandscapeMode;
    UIView *toolBarView;
}
@property(nonatomic,strong)UILabel *durationlbl;
@property(nonatomic,strong)UILabel *playTimelbl;
@end
@implementation KPVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    isLandscapeMode=NO;
//    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
//    
//    float   angle = M_PI/2;  //rotate 180°, or 1 π radians
//    self.view.layer.transform = CATransform3DMakeRotation(angle, 0, 0.0, 1.0);
    [super viewDidLoad];
    counter = 0;
    isFullScreen =NO;
    showAllert=YES;
    UIImage *image = [UIImage imageNamed:@"video_i4"];
    imageString =[self getStringFromImage:image];
    [self addTopView];
    [self songTitleLable]; // label for song title
    [self videoPlayListFunctionality];// video playlist buttons for eg:play,pause,seekbar...
    [self video]; //plays video according to its path.
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 49, 320, 49)];//navigation bar where downloads,fav and share is shown.
    
    if(&UIApplicationDidEnterBackgroundNotification != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterInBackgroundState:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    // On iOS 4.0+ only, listen for foreground notification
    if(&UIApplicationWillEnterForegroundNotification != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForegroundState:) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    // Do any additional setup after loading the view.
}
-(void)appDidEnterInBackgroundState:(NSNotification*)notification
{
    [playPauseBtn setSelected:NO];

}
-(void)appWillEnterForegroundState:(NSNotification*)notification
{
  //  [playPauseBtn setSelected:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"back_blue"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 30, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 11, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    if ([[self.categoryInfo.type lowercaseString]isEqualToString:@"movies"] ) {
        [titleLbl setText:@"MOVIES"];
    }
    else
    [titleLbl setText:@"MUSIC"];

    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:14.0/255 green:26.0/255 blue:93.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    [self.view addSubview:topStripImgView];
}

/*
 * Label for Song Title on video player...
 */
-(void)songTitleLable
{
    // UIView *titalView=[[UIView alloc]initWithFrame:CGRectMake(0, 103, 320, 40)];
    [self.view addSubview:[self customeView:CGRectMake(0, 103, 320, 40) bgColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"vid-bg"]]tag:5555]];
    UIView *view = [self.view viewWithTag:5555];
    UILabel *songTitle=[[UILabel alloc]initWithFrame:CGRectMake(23, 7, 320-23, 17)];
    songTitle.text= self.categoryInfo.file_name;
    songTitle.tag=1331;
    [songTitle setBackgroundColor:[UIColor clearColor]];
    songTitle.textColor=[UIColor whiteColor];
    [songTitle setFont:[UIFont fontWithName:@"Helvetica-bold" size:12]];
    [view addSubview:songTitle];
    UILabel *singerNameLable=[[UILabel alloc]initWithFrame:CGRectMake(23, 21, 320-23, 13)];
    singerNameLable.text= self.categoryInfo.person_name;
    singerNameLable.tag=1332;
    [singerNameLable setBackgroundColor:[UIColor clearColor]];
    singerNameLable.textColor=[UIColor whiteColor];
    [singerNameLable setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    [view addSubview:singerNameLable];
    
}

-(UIView *)getPlayerView
{
    return (UIView *)[self.navigationController.view viewWithTag:5556];
}

-(void)video
{
    [self.navigationController.view addSubview:[self customeView:CGRectMake(0, (IS_IPHONE_5)?150:145, 320, (IS_IPHONE_5)?286*deviceFactor:245*deviceFactor) bgColor:[UIColor clearColor]tag:5556]];
    
    UIView *view = [self getPlayerView];
    [view setBackgroundColor:[UIColor clearColor]];
    NSLog(@"play__video");
    
    showActivity(self.navigationController.view); // Shows activity indicator
    NSLog(@"image url %@",self.categoryInfo.file_url);
    
//     NSString * pathv = [[NSBundle mainBundle] pathForResource:@"aish" ofType:@"mov"];
//     NSString *str=[[self.fileArray objectAtIndex:self.selectedIndex]valueForKey:@"file_url"];
    
    self.player = [[MPMoviePlayerController alloc] init];
    
    NSURL *url;
    if (self.isDownloads)
    {
        url = [NSURL fileURLWithPath:self.categoryInfo.file_url];
        url = [NSURL fileURLWithPath:self.categoryInfo.file_url];
    }
    else
    {
        url = [NSURL URLWithString:[self.categoryInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"url is:----------------- %@",url);
        //NSLog(@"The Value of counter is : %d",counter);
    }
    
    [self.player setContentURL:url]; // Set url to media player
    
    //[self.player setMovieSourceType:MPMovieSourceTypeFile];
    [self.player prepareToPlay];
    
    UITapGestureRecognizer *doubleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapActionOnPlayer:)];
    [self.player.view addGestureRecognizer:doubleTap];
    [doubleTap setDelegate:(id)self];
    [doubleTap setNumberOfTapsRequired:2];

    UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showToolBarOnFullScreenTouch:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setDelegate:(id)self];

    [self.player.view  addGestureRecognizer:singleTap];
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    [self.player.view setUserInteractionEnabled:YES];
    [view addSubview:self.player.view];
    self.player.controlStyle=MPMovieControlStyleNone;
    
    self.player.view.frame = CGRectMake(view.bounds.origin.x + 5, view.bounds.origin.y+3, view.frame.size.width - 10, view.frame.size.height+10);//frame of a video player
    self.player.scalingMode=MPMovieScalingModeAspectFit;
    [self.player prepareToPlay];
    self.slider.maximumValue=[self.player duration];
    NSLog(@"[self.player duration] %f", [self.player duration]);
    [self.player play];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayerStateDidChange:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayerPlaybackStateDidChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(movieDurationAvailable:) name:MPMovieDurationAvailableNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(moviePlayerDidFinishPlayingVideo:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    self.slider.maximumValue=0.0;
    
    timeCounter=-1;
    NSLog(@"video initiated with duration %f %d",self.player.duration,[self.player isPreparedToPlay]);
    // [view addSubview:imageView];
}

/*
 * video player buttons method
 */
-(void)moviePlayerPlaybackStateDidChange:(NSNotification*)notification
{
    if (self.player.playbackState==MPMoviePlaybackStateInterrupted)
    {
        NSLog(@"MPMoviePlaybackStateInterrupted");
    }
    else  if (self.player.playbackState==MPMoviePlaybackStatePaused)
    {
        NSLog(@"MPMoviePlaybackStatePaused");
        //showActivity(self.navigationController.view);
    }
    else  if (self.player.playbackState==MPMoviePlaybackStateSeekingBackward)
    {
        NSLog(@"MPMoviePlaybackStateSeekingBackward");
    }
    else  if (self.player.playbackState==MPMoviePlaybackStateSeekingForward)
    {
        NSLog(@"MPMoviePlaybackStateSeekingForward ");
    }
    else  if (self.player.playbackState==MPMoviePlaybackStatePlaying)
    {
        if (isDragged)
        {
           // [self buttonClickActionOnVideoView:playPauseBtn];
            isDragged=NO;
        }
        NSLog(@"MPMoviePlaybackStateSeekingForward MPMoviePlaybackStatePlaying");
        //dismissHUD(self.navigationController.view);
        
    }
//    else if (self.player.playbackState ==MPMoviePlaybackStateStopped)
//    {
//        NSLog(@"MPMoviePlaybackStateStopped");
//        showActivity(self.navigationController.view);
//    }
}


-(void)moviePlayerStateDidChange:(NSNotification*)notification
{
    if (self.player.loadState == MPMoviePlaybackStateStopped)
    {
        NSLog(@"Video Stopped");
    }
//    if (self.player.loadState==MPMovieLoadStateStalled) {
//        NSLog(@"MPMovieLoadStateStalled");
////        showActivity(self.navigationController.view);
//    }
//    else if(self.player.loadState==MPMovieLoadStateUnknown)
//    {
//        NSLog(@"MPMOVOieloadStateunknown");
//    }
//    else if (self.player.loadState==MPMovieLoadStatePlaythroughOK) {
//        NSLog(@"MPMovieLoadStatePlaythroughOK");
//    }
//    else if(self.player.loadState==MPMovieLoadStatePlayable)
//    {
//        NSLog(@"MPMovieLoadStatePlayable");
//    }
}
/*
 * Method for when video finish playing
 */
-(void)moviePlayerDidFinishPlayingVideo:(NSNotification*)notification
{
    self.slider.value=0;
    [self.playTimelbl setText:@"00:00:00"];
    self->prevoiusChangedValue = 0;

    NSLog(@"dismissHUD palay__didfinish");
    [playPauseBtn setSelected:NO];
    dismissHUD(self.navigationController.view);
    showActivity(self.navigationController.view);// Shows activity indicator
    UIView *titleView=[self.view viewWithTag:5555];
    UILabel *lblTitle=(UILabel*)[titleView viewWithTag:1331];
    UILabel *lblName=(UILabel*)[titleView viewWithTag:1332];
    UIButton *favBtn=(UIButton *)[viewNavigation viewWithTag:5502];
    
    
    UIView * view = (UIView *)[self.view viewWithTag:5557];
    UIButton * loopButton = (UIButton * )[view viewWithTag:5517];
    UIButton * playButton = (UIButton *)[view viewWithTag:5508];
    [playButton setSelected:YES];
    //loopButton.selected =! loopButton.selected;
    
    if (!loopButton.selected)
    {
        [self.player play];
        [playButton setSelected:YES];
        --self.selectedIndex;
        NSLog(@"Chetan");
    }
    else
    {
        UIView * view = (UIView *)[self.view viewWithTag:5557];
        UIButton * play = (UIButton *)[view viewWithTag:5508];
        [play setSelected:NO];
        //[loopButton setSelected:YES];
        NSLog(@"Ravi");
    }
    
    
    if (IsRandom)
    {
        self.selectedIndex=[self getRandomNumber]-1;
    }
    if (self.videoArray.count>++self.selectedIndex)
    {
        self.categoryInfo=[self.videoArray objectAtIndex:self.selectedIndex];
        [lblName setText:self.categoryInfo.person_name];
        [lblTitle setText:self.categoryInfo.file_name];
        @try {
             [favBtn setSelected:(self.categoryInfo.favourite_status)?[self.categoryInfo.favourite_status isEqualToString:@"1"]:NO];
        }
        @catch (NSException *exception) {
            
        }
        @finally {
            
        }
       
        // NSURL *url=[NSURL URLWithString:[songInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
        NSURL *url;
        if (self.isDownloads)
        {
            url=[NSURL fileURLWithPath:self.categoryInfo.file_url];
        }
        else
            url=[NSURL URLWithString:[self.categoryInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
        
        [self.player setContentURL:url];
        [self.player prepareToPlay];
    }
    else
    {
        dismissHUD(self.navigationController.view);
        NSLog(@"dismissHUD movididfinish");
    }
    NSLog(@"moviePlayerDidFinishPlayingVideo");
    
}

/*
 * Method for duration of a video playback time
 */
-(NSString*)formattedTimeforDuration:(float)time
{
    int min,sec,hr;
    if (time<60) {
        sec=time;
        hr=0;
        min=0;
    }
    else if (time>60 && time<3600)
    {
        hr=0;
        min=time/60;
        sec=(int)time%60;
    }
    else
    {
        hr=time/3600;
        sec=(int)time%3600;
        min=sec/60;
        sec=sec%60;
    }
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hr,min,sec];
}

/*
 * Movie Duration and prev and next button enabled...
 */
-(void)movieDurationAvailable:(id)sender
{
    NSLog(@"movieDurationAvailable %f",self.player.duration);
    NSLog(@"dissmis__durationABL");
   // UIView *view = [self.view viewWithTag:5557];
    UIView *view=toolBarView;
    UIButton *prevBtn=(UIButton*)[view viewWithTag:5507];
    UIButton *nextBtn=(UIButton*)[view viewWithTag:5509];
    if (self.videoArray.count==1 ||!self.videoArray ||self.videoArray.count==0)
    {
        [prevBtn setEnabled:NO];
        [nextBtn setEnabled:NO];
    }
    else if (self.selectedIndex==0)
    {
        [prevBtn setEnabled:NO];
        if (self.videoArray.count>1)
        {
            [nextBtn setEnabled:YES];
        }
        
    }
    else if (self.selectedIndex==self.videoArray.count-1)
    {
        [nextBtn setEnabled:NO];
        if (self.selectedIndex!=0)
        {
            [prevBtn setEnabled:YES];
        }
    }
    else if (self.selectedIndex>0 && !prevBtn.enabled)
    {
        [prevBtn setEnabled:YES];
    }
    else if (self.selectedIndex<self.videoArray.count-1 && !nextBtn.enabled)
        [nextBtn setEnabled:YES];
    [self.durationlbl setText:[self formattedTimeforDuration:self.player.duration]];
    playPauseBtn.selected=YES;

    self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateStatusOfPlayer:) userInfo:nil repeats:YES];
    [self.timer fire];
    [self.player play];
    self.slider.maximumValue=self.player.duration;
}

# pragma mark - UIControl/Touch Events

- (void)durationSliderTouchBegan:(UISlider *)slider
{
    [self.player pause];
}

- (void)durationSliderTouchEnded:(UISlider *)slider
{
    self->prevoiusChangedValue = floor(slider.value);
    [self.player setCurrentPlaybackTime:floor(slider.value)];
    [self.player play];
}

- (void)durationSliderValueChanged:(UISlider *)slider
{
    NSLog(@"durationSliderValueChanged");
}

-(void)updateStatusOfPlayer:(NSTimer*)timer
{
    dismissHUD(self.navigationController.view);
    NSLog(@"dismissHUD updatesStatus");
    NSLog(@"self.player.currentPlaybackTime %f", self.player.currentPlaybackTime);

    if ((int)self.player.currentPlaybackTime>=(int)self.player.duration)
    {
        NSLog(@"Prepare toi ccall");
        [timer invalidate];
        timeCounter=0;
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           
            NSLog(@"jsdhafkjsdagfgsdahgfhds %f and %f", self->prevoiusChangedValue, self.player.currentPlaybackTime);
            if ((self->prevoiusChangedValue - self.player.currentPlaybackTime) < 0.000000001)
            {
                self.slider.value=self.player.currentPlaybackTime;
                timeCounter++;
                [self.playTimelbl setText:[self formattedTimeforDuration:self.player.currentPlaybackTime]];
            }
            else
            {
                timeCounter++;
                [self.playTimelbl setText:[self formattedTimeforDuration:self.player.currentPlaybackTime]];
                NSLog(@"Condition not truee!!!");
            }
        });
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self getPlayerView] setHidden:NO];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.player pause];
    playPauseBtn.selected=NO;
    showAllert=NO;
    [self.timer invalidate];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMovieDurationAvailableNotification object:nil];
}
-(void)updateSliderForTimer:(NSTimer*)timer
{
    //    self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSliderForTimer:) userInfo:nil repeats:YES];
    //    [self.timer fire];
    NSLog(@"update timer %f and %f" ,timeCounter,self.player.duration);
    if ((int)self.player.duration<=timeCounter && (self.player.playbackState==MPMoviePlaybackStatePlaying)) {
        [timer invalidate];
        timeCounter=-1;
        self.slider.value=0;
        self->prevoiusChangedValue = 0;

        [self.playTimelbl setText:@"00:00:00"];
    }
    //self.slider.value=++timeCounter;
    
    
}

/*
 * Method for video playlist buttons
 */
-(void)videoPlayListFunctionality
{
    toolBarView=[self customeView:CGRectMake(0,self.view.frame.size.height-110, 320,59) bgColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"video_bot"]]tag:5557];
    UIView *view = toolBarView;//[self.view viewWithTag:5557];

    [self.view addSubview:view];
    self.slider=[[UISlider alloc]initWithFrame:CGRectMake(16, 9, 273, 10)];//287
    
    [self.slider addTarget:self action:@selector(durationSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.slider addTarget:self action:@selector(durationSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
    [self.slider addTarget:self action:@selector(durationSliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside];
    [self.slider addTarget:self action:@selector(durationSliderTouchEnded:) forControlEvents:UIControlEventTouchUpOutside];
    [self.slider addTarget:self action:@selector(sliderBeginDragging:) forControlEvents:UIControlEventTouchDown];
    [self.slider addTarget:self action:@selector(sliderDidEndDragging:) forControlEvents:UIControlEventTouchUpInside];
    self.slider.thumbTintColor =[UIColor colorWithRed:1 green:1 blue:1 alpha:0.2];
    [self.slider setMaximumTrackImage:[UIImage imageNamed:@"min_track"] forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:[UIImage imageNamed:@"max_track"] forState:UIControlStateNormal];
    [self.slider.layer setCornerRadius:4.0];
    [self.slider setThumbImage:[UIImage imageNamed:@"seek-btn"] forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed:@"seek-btn"] forState:UIControlStateDisabled];
    [self.slider setThumbImage:[UIImage imageNamed:@"seek-btn"] forState:UIControlStateSelected];
    self.slider.minimumValue=0;
    self.slider.value=0;
    self->prevoiusChangedValue = 0;

    [self.slider addTarget:self action:@selector(sliderDraggedAction:) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside)];
    [view addSubview:self.slider];
    self.playTimelbl=[[UILabel alloc]initWithFrame:CGRectMake(19, 25, 42,10)];
    [self.playTimelbl setText:@"00:00:00"];
    [self.playTimelbl setTextColor:[UIColor whiteColor]];
    [self.playTimelbl setBackgroundColor:[UIColor clearColor]];
    [self.playTimelbl setFont:[UIFont fontWithName:@"Helvetica-bold" size:10]];
    [view addSubview:self.playTimelbl];
    self.durationlbl=[[UILabel alloc]initWithFrame:CGRectMake(251, 25, 42, 10)];//263
    
    [self.durationlbl setTextColor:[UIColor whiteColor]];
    [self.durationlbl setBackgroundColor:[UIColor clearColor]];
    [self.durationlbl setFont:[UIFont fontWithName:@"Helvetica-bold" size:10]];
    [view addSubview:self.durationlbl];
    
    
    UIImage *shufleImg=[UIImage imageNamed:@"shuffle_icone"];
    UIImage *play=[UIImage imageNamed:@"play-paus"];
    UIImage *pause=[UIImage imageNamed:@"pause"];
    UIImage *prev=[UIImage imageNamed:@"rews"];
    UIImage *fwdImg=[UIImage imageNamed:@"frwd"];
    UIImage *volumeIcon=[UIImage imageNamed:@"volume_icon"];
    UIImage * volumeIcon_select = [UIImage imageNamed:@"volume-icon-mute"];
    UIImage *repeat=[UIImage imageNamed:@"repeatIcon"];
    UIImage * repeat_select = [UIImage imageNamed:@"repeat_active"];

    
    UIImage *shufle_select=[UIImage imageNamed:@"shuffle-active"];
    [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(80, 37, shufleImg.size.width, shufleImg.size.height) bgImage:shufleImg tag:5515 selectedImage:shufle_select]];
    [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(121-8, 28, prev.size.width+5, prev.size.height+5) bgImage:prev tag:5507 selectedImage:nil]];
    playPauseBtn=[self addNavigationBarButtonWithFrame:CGRectMake(155-8, 24, play.size.width+5, play.size.height+5) bgImage:play tag:5508 selectedImage:pause];
    [view addSubview:playPauseBtn];
    
    
    
    [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(193-8, 28, fwdImg.size.width+5, fwdImg.size.height+5) bgImage:fwdImg tag:5509 selectedImage:nil]];
    
    [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(self.view.frame.size.width-5-volumeIcon.size.width, 4, volumeIcon.size.width, volumeIcon.size.height) bgImage:volumeIcon_select tag:5516 selectedImage:volumeIcon]];

    [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(221, 37, repeat.size.width, repeat.size.height) bgImage:repeat_select tag:5517 selectedImage:repeat]];

    //    [[UIApplication sharedApplication].keyWindow addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(self.view.frame.size.width-70, 30, fullScreen.size.width, fullScreen.size.height) bgImage:fullScreen tag:5510 selectedImage:nil]];
  //  [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(self.view.frame.size.width-43, 31, fullScreen.size.width+5, fullScreen.size.height+5) bgImage:fullScreen tag:5510 selectedImage:nil]];
    NSLog(@"video initiated with funtionality");
}

bool isDragged;
-(void)sliderBeginDragging:(UISlider*)sender
{
//    isDragged=YES;
    //[self buttonClickActionOnVideoView:playPauseBtn];
}

-(IBAction)sliderDraggedAction:(UISlider*)sender
{
    
    // [self.player setInitialPlaybackTime:sender.value];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![self.timer isValid]) {
                self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateStatusOfPlayer:) userInfo:nil repeats:YES];
                [playPauseBtn setSelected:YES];
                [self.timer fire];
            }
            
            [self.player setCurrentPlaybackTime:floor(sender.value)];
            timeCounter=sender.value;
            //[self.player pause];
            //[self.player play];
            
        });
    });
    NSLog(@"slider value changed %f",sender.value);
    // [self.player setEndPlaybackTime:self.player.duration];
}



-(void)sliderDidEndDragging:(UISlider*)sender
{
   // [self buttonClickActionOnVideoView:playPauseBtn];

}
-(void)doubleTapActionOnPlayer:(UITapGestureRecognizer*)gesture
{
    NSLog(@"double tapped %d",gesture.numberOfTapsRequired);
    
    [[UIDevice currentDevice] setValue:
     [NSNumber numberWithInteger: UIInterfaceOrientationPortrait]
     forKey:@"orientation"];
    UIView *tempView=toolBarView;
    UIView *titleView=(UIView*)[self.player.view viewWithTag:7348];
    if(tempView.superview!=self.view)
    {
        [titleView removeFromSuperview];
        [tempView removeFromSuperview];
       // [[UIDevice currentDevice] setOrientation:UIInterfaceOrientationPortrait];
        [tempView setFrame:CGRectMake(0,self.view.frame.size.height-110, 320,59)];
        [self.view addSubview:tempView];
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationPortrait]
         forKey:@"orientation"];
    }
    isFullScreen=NO;
    [KPSingletonClass sharedObject].isVideoFullScreen=NO;

    UIView *playerView=[self getPlayerView];
    [playerView setFrame:CGRectMake(0, (IS_IPHONE_5)?150:145, 320, (IS_IPHONE_5)?286*deviceFactor:245*deviceFactor)];
    //self.player.view.frame = CGRectMake(playerView.bounds.origin.x + 5, playerView.bounds.origin.y + 5, playerView.frame.size.width - 10, playerView.frame.size.height - 10);
    self.player.view.frame = CGRectMake(playerView.bounds.origin.x + 5, playerView.bounds.origin.y+3, playerView.frame.size.width - 10, playerView.frame.size.height+10);
}
-(UIView*)customeView:(CGRect)frame bgColor:(UIColor*)color tag:(int)tag
{
    UIView *videoView=[[UIView alloc]init];
    [videoView setFrame:frame];
    [videoView setTag:tag];
    [videoView setBackgroundColor:color];
    return videoView;
}

-(void)addLocalNavigationBar:(CGRect)frame
{
    [self addTopView];
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    viewNavigation = [[UIView alloc] init];
    viewNavigation.frame = frame;
    //[viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    [viewNavigation setBackgroundColor:[UIColor colorWithRed:0.0 green:2/255.0 blue:30/255.0 alpha:1.0]];
    NSLog(@"ViaDownloads :- %@", [KPSingletonClass sharedObject].viaDownloads);
    NSLog(@"viaFavourites :- %@", [KPSingletonClass sharedObject].viaFavourites);
    NSLog(@"viaPlayList :- %@", [KPSingletonClass sharedObject].viaPlayList);
    float favWidth;
    float downloadWidth;
    float playListWidth;
    float shareListWidth;
    
    UIImage *favImage=[UIImage imageNamed:@"grayFav"];
    UIImage *threeline=[UIImage imageNamed:@"lineThree"];
    UIImage *zoominIcon=[UIImage imageNamed:@"zoomIn"];
    
    UIImage *favRedImage=[UIImage imageNamed:@"redFav"];
    UIImage *tumbImage=[UIImage imageNamed:@"klip5"];
    UIImage *shareImage=[UIImage imageNamed:@"shareIcon"];
    UIImage *download=[UIImage imageNamed:@"download1"];
    UIImage *playlist=[UIImage imageNamed:@"playlistIcon"];
    
    int x=0;
    if ([[KPSingletonClass sharedObject].viaFavourites isEqualToString:@"Favourites"]) {
        
        x=1;
        //        favWidth = self.view.frame.size.width+100;
        //        playListWidth = viewNavigation.frame.size.width/2 - 60+29;
        //        downloadWidth = 167;
        //        shareListWidth = viewNavigation.frame.size.width/2+43;
        //        x=28;
    } else if ([[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"]) {
        x=2;
        //        favWidth = self.view.frame.size.width+100;
        //        playListWidth = self.view.frame.size.width+100;
        //        downloadWidth = self.view.frame.size.width+100;
        //        shareListWidth = 170;
        //         x=65 ;
    }else if ([[KPSingletonClass sharedObject].viaPlayList isEqualToString:@"PlayList"]) {
        x=3;
        //        favWidth = viewNavigation.frame.size.width/2-60+29;
        //        downloadWidth = 168;
        //        playListWidth = self.view.frame.size.width+101;
        //        shareListWidth = viewNavigation.frame.size.width/2+44;
        //         x=28;
    }
    else
    {
        x=4;
        //        favWidth = viewNavigation.frame.size.width/2-54;
        //        downloadWidth = 194;
        //        playListWidth = viewNavigation.frame.size.width/2-9;
        //        shareListWidth = viewNavigation.frame.size.width/2+70;
    }
    
    
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2-11,threeline.size.width ,threeline.size.height) bgImage:threeline tag:5501 selectedImage:nil]];
    
    UIButton *favBtn=[self addNavigationBarButtonWithFrame:CGRectMake(53, viewNavigation.frame.size.height/2-8,favImage.size.width,favImage.size.height) bgImage:favImage tag:5502 selectedImage:favRedImage];//viewNavigation.frame.origin.x + 30+threeline.size.width
    
    
    [viewNavigation addSubview:favBtn];
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(favBtn.frame.origin.x+12+favBtn.frame.size.width, viewNavigation.frame.size.height/2-16, playlist.size.width,playlist.size.height) bgImage:playlist tag:5503 selectedImage:nil]];
    
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(145, viewNavigation.frame.size.height/2-16,tumbImage.size.width ,tumbImage.size.height) bgImage:tumbImage tag:5500 selectedImage:nil]];//favBtn.frame.origin.x+favBtn.frame.size.width+80
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(209, viewNavigation.frame.size.height/2-10, download.size.width, download.size.height) bgImage:download tag:5506 selectedImage:nil]];//favBtn.frame.origin.x+favBtn.frame.size.width+80+tumbImage.size.width+playlist.size.width
    
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(favBtn.frame.origin.x+favBtn.frame.size.width+130+tumbImage.size.width+download.size.width, viewNavigation.frame.size.height/2-9, shareImage.size.width, shareImage.size.height) bgImage:shareImage tag:5505 selectedImage:nil]];
    
    [viewNavigation addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(favBtn.frame.origin.x+favBtn.frame.size.width+150+tumbImage.size.width+shareImage.size.width+download.size.width, viewNavigation.frame.size.height/2-9, zoominIcon.size.width, zoominIcon.size.height) bgImage:zoominIcon tag:5510 selectedImage:nil]];
    
    if (x==1) {
        [favBtn setEnabled:NO];
    }
    else if (x==2)
    {
        UIButton *downloadBtn=(UIButton*)[viewNavigation viewWithTag:5506];
        [downloadBtn setEnabled:NO];
    }
    else if (x==3)
    {
        UIButton *playlistBtn=(UIButton*)[viewNavigation viewWithTag:5503];
        [playlistBtn setEnabled:NO];
    }
    
    @try
    {
        [favBtn setSelected:[self.categoryInfo.favourite_status isEqualToString:@"1"]];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    [self.view addSubview:viewNavigation];
}

-(UIButton*)addNavigationBarButtonWithFrame:(CGRect)frame bgImage:(UIImage*)image tag:(int)tagValue selectedImage:(UIImage*)selectedImg
{
    // NSLog(@"selected Image %@",selectedImg);
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tagValue];
    [button setFrame:frame];
    
    [button setBackgroundImage:image forState:UIControlStateNormal];
    if (selectedImg) {
        [button setBackgroundImage:selectedImg forState:UIControlStateSelected];
    }
    [button addTarget:self action:@selector(buttonClickActionOnVideoView:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
-(void)displayAlertView
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Not Login!" message:@"Please login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login"       , nil];
    alertView.tag=34343;
    [alertView show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==34343 && buttonIndex==1)
    {
        [[self getPlayerView] removeFromSuperview];
        KPAppDelegate *app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
        [app.tabListController logOutButtonClicked];
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if (alertView.tag==39493)
    {
//        [playPauseBtn setSelected:YES];
//        [self.player play];
        [self buttonClickActionOnVideoView:playPauseBtn];

    }
}
-(void)shareButtonClicked
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter", nil];
    [actionSheet showInView:self.view];
}

// This method is not in use..

-(void)buttonClickActionOnVideoView:(UIButton*)sender
{
    NSLog(@"ButtonClickAction %ld",(long)sender.tag);
    if (sender.tag==5500)
    {
        [[KPSingletonClass sharedObject] setViaDownloads:@""];
        [[KPSingletonClass sharedObject] setViaFavourites:@""];
        [[KPSingletonClass sharedObject] setViaPlayList:@""];
        
        [[self getPlayerView] removeFromSuperview];
        KPHomeViewController *vc;
        for(id controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[KPHomeViewController class]])
            {
                vc=(KPHomeViewController*)controller;
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        if (!vc)
        {
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if(sender.tag==5501)
    {
        [[self getPlayerView] removeFromSuperview];
        [self.navigationController popViewControllerAnimated:YES];
    } else if(sender.tag==5502)//add to favourits
    {
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"])
        {
            [self displayAlertView];
            return;
        }
        [self addToFavourites];
    }
    else if(sender.tag==5503)//Add to playlist
    {
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        [self addFileToPlaylist];
    }
    else if(sender.tag==5505)//Share
    {
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        [self shareButtonClicked];
    }
    else if(sender.tag==5506)//download
    {
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        NSLog(@"downloadself.navigationController.viewControllers %@",self.navigationController.viewControllers);
        [sender setEnabled:NO];
//        if (playPauseBtn.selected) {
//            [self buttonClickActionOnVideoView:playPauseBtn];
//        }
        showActivity(self.navigationController.view);// Shows activity indicator
        [self downLoadVideoInBackground:self.categoryInfo.file_url withSender:sender];
    }
    else if(sender.tag==5507) {
        
        [self playPreviousVideo];
    } else if (sender.tag==5508) {
        sender.selected=!sender.selected;
        if (!sender.selected) {
            [self.timer invalidate];
            [self.player pause];
            
        } else {
            [self.player play];
            [self.timer invalidate];
            self.timer=nil;
            self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateStatusOfPlayer:) userInfo:nil repeats:YES];
        }
    } else if(sender.tag==5509) {
        [self playNextSong];
    }
    else if ([sender tag] == 5510)
    {
        
        UIView *view = [self getPlayerView];
        isFullScreen=YES;
        [KPSingletonClass sharedObject].isVideoFullScreen=YES;

        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        NSLog(@"MMMCM %f and %f",height,view.frame.size.height);
        [UIView animateWithDuration:0.5f animations:^{
//            [view setFrame:(view.frame.size.height == height) ? CGRectMake(0, (IS_IPHONE_5)?150:145, 320, (IS_IPHONE_5)?286*deviceFactor:245*deviceFactor) : CGRectMake(0, 0 ,width, height)];
        
            [view setFrame:CGRectMake(0, 0 ,width, height)];
            self.player.view.frame = CGRectMake(0, 0, width, height);
        } completion:^(BOOL finished) {
           // [view addSubview:[self addNavigationBarButtonWithFrame:CGRectMake(self.view.frame.size.width-44, 28, fullScreen.size.width+5, fullScreen.size.height+5) bgImage:fullScreen tag:5520 selectedImage:nil]];
            
        }];
        
    } else if(sender.tag==5515){
        
        if (IsRandom) {
            sender.selected=YES;
            self.selectedIndex=index;
        } else {
            sender.selected=NO;
            index=self.selectedIndex;
        }
        IsRandom=!IsRandom;
    }else if(sender.tag==5520)
    {
        
        
        UIView *playerView=[self getPlayerView];
        
        [sender removeFromSuperview];
        [playerView setFrame:CGRectMake(0, (IS_IPHONE_5)?150:145, 320, (IS_IPHONE_5)?286*deviceFactor:245*deviceFactor)];
        //self.player.view.frame = CGRectMake(playerView.bounds.origin.x + 5, playerView.bounds.origin.y + 5, playerView.frame.size.width - 10, playerView.frame.size.height - 10);
        self.player.view.frame = CGRectMake(playerView.bounds.origin.x + 5, playerView.bounds.origin.y+3, playerView.frame.size.width - 10, playerView.frame.size.height+10);
    }
    else if (sender.tag==5516)
    {
        NSLog(@"Volume off/oon");
        sender.selected=!sender.selected;
        if (sender.selected)
        {
            [[MPMusicPlayerController systemMusicPlayer]setVolume:1.0];
        }
        else
            
        [[MPMusicPlayerController systemMusicPlayer]setVolume:0.0];
        
    }
    else if (sender.tag == 5517)
    {
        sender.selected =!sender.selected;
        
        if (!sender.selected)
        {
            [self.player play];
        }
        
        else
        {
            NSLog(@"hahhahaha");
        }
    }

    
}

/*
 * To show tool bar and tilte bar on taping video in full screen view
 */
-(void)showToolBarOnFullScreenTouch:(UIGestureRecognizer*)gesture
{
    
    if (!isFullScreen) {
        return;
    }
    
    //=[self.videoArray objectAtIndex:self.selectedIndex];
    UIView *titleView=(UIView*)[self.player.view viewWithTag:7348];

      UIImage *fullScreen=[UIImage imageNamed:@"zoomOut"];
    UIView *view =toolBarView;
    if (view.superview==self.view) {
        [view removeFromSuperview];
        NSLog(@"########## hello  %@",NSStringFromCGRect(self.view.frame));
        [view setFrame:CGRectMake((!isLandscapeMode)?0:(self.view.frame.size.width/2-view.frame.size.width/2),self.view.frame.size.height-view.frame.size.height-(isLandscapeMode?4:20), view.frame.size.width, view.frame.size.height)];
        [self.player.view addSubview:view];
    }
    else
    {
        [view removeFromSuperview];
        [self.view addSubview:view];
    }
    
   // [self.view bringSubviewToFront:view];
    if (!titleView) {
        titleView=[[UIView alloc]initWithFrame:CGRectMake(0, isLandscapeMode?15:20, self.view.frame.size.width, 35)];
        [titleView setBackgroundColor:[UIColor colorWithRed:61/255.0 green:61/255.0 blue:61/255.0 alpha:isLandscapeMode?0.5:1.0]];
        [self.player.view addSubview:titleView];
        [titleView setTag:7348];
        UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(12, 10, self.view.frame.size.width-fullScreen.size.width-24, 16)];
        [titleLbl setFont:[UIFont systemFontOfSize:13]];
        [titleLbl setText:self.categoryInfo.file_name];
        [titleLbl setBackgroundColor:[UIColor clearColor]];
        [titleLbl setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0]];
        [titleView addSubview:titleLbl];
        UIButton *exitFullScrBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [exitFullScrBtn setFrame:CGRectMake(self.view.frame.size.width-fullScreen.size.width-6-(isLandscapeMode?4:0), 6, fullScreen.size.width, fullScreen.size.height)];
        [exitFullScrBtn setImage:fullScreen forState:UIControlStateNormal];
        [exitFullScrBtn addTarget:self action:@selector(exitButtonActionToZoomOut:) forControlEvents:UIControlEventTouchUpInside];
        [titleView addSubview:exitFullScrBtn];
    }
    else
        [titleView removeFromSuperview];

    //UILabel *titleLbl=(UILabel*)[

}
-(void)exitButtonActionToZoomOut:(UIButton*)sender
{
    isFullScreen=NO;
    UIView *titleView=(UIView*)[self.player.view viewWithTag:7348];
    [titleView removeFromSuperview];
    [self doubleTapActionOnPlayer:[UITapGestureRecognizer new]];
    }

/*
 * Method for playing the next item
 */
-(void)playNextSong
{
    NSLog(@"dismissHUD dissmis__Next");
    dismissHUD(self.navigationController.view);
    if (self.videoArray.count==1 || !self.videoArray)
    {
        return;
    }
    else if (self.videoArray.count==++self.selectedIndex)
    {
        self.selectedIndex=0;
    }
    UIButton *favBtn=(UIButton *)[viewNavigation viewWithTag:5502];
    self.slider.value=0;
    self->prevoiusChangedValue = 0;

    [self.playTimelbl setText:@"00:00:00"];
    NSLog(@"palay__next Song");
    
    showActivity(self.navigationController.view);
    UIView *titleView=[self.view viewWithTag:5555];
    UILabel *lblTitle=(UILabel*)[titleView viewWithTag:1331];
    UILabel *lblName=(UILabel*)[titleView viewWithTag:1332];
    self.categoryInfo=[self.videoArray objectAtIndex:self.selectedIndex];
    [lblName setText:self.categoryInfo.person_name];
    [lblTitle setText:self.categoryInfo.file_name];
    
    [favBtn setSelected:(self.categoryInfo.favourite_status)?[self.categoryInfo.favourite_status isEqualToString:@"1"]:NO];
    //NSURL *url=[NSURL URLWithString:[songInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    NSURL *url;
    if (self.isDownloads)
    {
        url=[NSURL fileURLWithPath:self.categoryInfo.file_url];
    }
    else
        url=[NSURL URLWithString:[self.categoryInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    [playPauseBtn setSelected:NO];
    [self.player setContentURL:url];
    [self.player prepareToPlay];
}

/*
 * Method for playing the previous item
 */
-(void)playPreviousVideo
{
    NSLog(@"dismissHUD dissmis__prev");
    
    dismissHUD(self.navigationController.view);
    if (self.videoArray.count==1|| !self.videoArray) {
        return;
    }
    else if (--self.selectedIndex<0) {
        self.selectedIndex=self.videoArray.count-1;;
    }
    self.slider.value=0;
    self->prevoiusChangedValue = 0;

    [self.playTimelbl setText:@"00:00:00"];
    UIButton *favBtn=(UIButton *)[viewNavigation viewWithTag:5502];
    NSLog(@"palay__Prev");
    showActivity(self.navigationController.view);
    UIView *titleView=[self.view viewWithTag:5555];
    UILabel *lblTitle=(UILabel*)[titleView viewWithTag:1331];
    UILabel *lblName=(UILabel*)[titleView viewWithTag:1332];
    self.categoryInfo=[self.videoArray objectAtIndex:self.selectedIndex];
    [lblName setText:self.categoryInfo.person_name];
    [lblTitle setText:self.categoryInfo.file_name];
    [favBtn setSelected:(self.categoryInfo.favourite_status)?[self.categoryInfo.favourite_status isEqualToString:@"1"]:NO];
    //  NSURL *url=[NSURL URLWithString:[songInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    NSURL *url;
    if (self.isDownloads)
    {
        url=[NSURL fileURLWithPath:self.categoryInfo.file_url];
    }
    else
        url=[NSURL URLWithString:[self.categoryInfo.file_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ];
    [playPauseBtn setSelected:NO];
    [self.player setContentURL:url];
    [self.player prepareToPlay];
}


-(void)randomPlayAction:(UIButton*)sender
{
    sender.selected=!sender.selected;
    if (!sender.selected)
    {
        index=self.selectedIndex;
        IsRandom=YES;
        self.selectedIndex = arc4random() %(self.videoArray.count);
    }
    else
    {
        self.selectedIndex=index;
        IsRandom=NO;
    }
    int y = arc4random() %(self.videoArray.count);
    NSLog(@"random number %d",y);
    
}
-(int)getRandomNumber
{
    return arc4random() %(self.videoArray.count);
}

/*
 * Method for adding file to playlist
 */
-(void)addFileToPlaylist
{
    KPPlayListViewController *playlist=[KPPlayListViewController new];
    playlist.playListType=KPNormalPlayListType;
    playlist.file_id=self.categoryInfo.file_id;
    playlist.category_id=self.categoryInfo.category_id;
    [[self getPlayerView] setHidden:YES];
    [self.navigationController pushViewController:playlist animated:YES];
}

-(void)updatingFavouriteStatusDBWithInfo:(KPSonngCategoryInfo*)categoryInfo
{
    NSLog(@"is favvv %@",self.navigationController.viewControllers);
    //    if (![[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
    //    {
    KPSearchDataINfo *info=[KPSearchDataINfo new];
    info.file_duration=categoryInfo.file_duration;
    info.file_id=categoryInfo.file_id;
    info.file_name=categoryInfo.file_name;
    info.file_url=categoryInfo.file_url;
    info.type=categoryInfo.type;
    info.category_id=categoryInfo.category_id;
    info.person_name=categoryInfo.person_name;
    info.is_favourite=categoryInfo.favourite_status;
    BOOL yes=[[self getCoreDataInitializer] updateFavStatusForSearch:info];
    NSLog(@"is_favourite in video player %hhd",yes);
    //  }
    
}

/*
 * Method for adding the thing to favourite
 */
-(void)addToFavourites
{
    if ([self.categoryInfo.favourite_status isEqualToString:@"1"])
    {
        showAlertView(nil, @"File already added.");
        return;
    }
    UIButton *favBtn=(UIButton *)[viewNavigation viewWithTag:5502];
    __weak typeof(self) weakSelf = self;
    showActivity(self.navigationController.view);
    NSLog(@"palay__addFAv");
    
    [self addFileToFavouritesWithFileId:self.categoryInfo.file_id categoryId:self.categoryInfo.category_id callBack:^(id result,NSError *error) {
        NSLog(@"fabbbbbvvv result %@",result);
      //  dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"dismissHUD dissmis__fav");
            dismissHUD(weakSelf.navigationController.view);
            if (result==nil)
            {
                //chetan
                showAlertView(nil, @"Network error.");
            }
            else
            {   NSDictionary *response =[result valueForKey:@"response"];
                NSString *msg;
                if ([[response valueForKey:@"success"]isEqualToString:@"true"])
                {
                    msg=@"Successfully added";
                    weakSelf.categoryInfo.favourite_status=@"1";
                    [favBtn setSelected:YES];

                    NSLog(@"weakSelf.categoryInfo %@", weakSelf.categoryInfo.category_id);
                    [weakSelf updatingFavouriteStatusDBWithInfo:weakSelf.categoryInfo];

                }
                else
                    msg=[response valueForKey:@"error"];
                showAlertView(nil,[NSString stringWithFormat:@"%@.",msg]);
            }
     //   });
        
       
    NSLog(@"addfileto favvvv %@",result);
    }];
}

-(KPCoreDataInitializer *)getCoreDataInitializer
{
    
    KPCoreDataInitializer *dataInitilizer = [KPCoreDataInitializer sharedMySingleton];
    [dataInitilizer initializeTheCoreDataModelClasses];
    return dataInitilizer;
}

-(void)dismissActivityWithTimer:(NSTimer*)timer
{
    if (isFirstTime)
    {
        isFirstTime=NO;
        return;
    }
    NSLog(@"dismissActivityWithTimerdismissActivityWithTimerdismissActivityWithTimerXXX");
    dispatch_async(dispatch_get_main_queue(), ^{
        dismissHUD(self.navigationController.view);
        showAllert=NO;
    });
    [timer invalidate];
    
}

/*
 * Method for downloading videos in background
 */

-(void)downLoadVideoInBackground:(NSString *)urlString withSender:(UIButton*)sender
{
    
    isFirstTime=YES;
    NSTimer *timer=[NSTimer scheduledTimerWithTimeInterval:180.0 target:self selector:@selector(dismissActivityWithTimer:) userInfo:nil repeats:YES];
    
    [timer fire];
    NSLog(@"decoder.original_url %@ ADD <<%@>>", urlString,[self.categoryInfo description]);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSString *filename = [URL lastPathComponent];
    
    [documentsDirectoryURL URLByAppendingPathComponent:filename];
    //BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectoryURL path]];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSString *filename = [URL lastPathComponent];
        
        NSLog(@"jksdfhjksahfhsda %@ and %@", [response suggestedFilename], filename);
        return [documentsDirectoryURL URLByAppendingPathComponent:filename];
        
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error)
    {
        NSLog(@"Video File downloaded to: %@", filePath);

//        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"twice downloaded %@",[[[[UIApplication sharedApplication] keyWindow] subviews]firstObject]);
            NSLog(@"currentview %@",error);
            if (!error) {
                KPDownLoadInfo *downLoadInfo = [[KPDownLoadInfo alloc] init];
                [downLoadInfo setFile_duration:self.categoryInfo.file_duration];
                [downLoadInfo setFile_id:self.categoryInfo.file_id];
                [downLoadInfo setFile_name:self.categoryInfo.file_name];
                [downLoadInfo setFile_url:self.categoryInfo.file_url];
                [downLoadInfo setCategory_id:self.categoryInfo.category_id];
                [downLoadInfo setPerson_name:self.categoryInfo.person_name];
                [downLoadInfo setLocalPath:[filePath path]];
                [downLoadInfo setIs_favourite:self.categoryInfo.favourite_status];
                [downLoadInfo setDownloadtype: getCategoryType(KPVideo)];
                [downLoadInfo setType:self.categoryInfo.type];
                [[self getCoreDataInitializer] addDownLoadInformation:downLoadInfo];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //dismissHUD(self.navigationController.view);
                    [sender setEnabled:YES];

                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@ downloaded successfully.",self.categoryInfo.file_name] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                alertView.tag=39493;
                if (1)
                {
                    [alertView show];
                    
                }});
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //dismissHUD(self.navigationController.view);
                    [sender setEnabled:YES];

            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Network error." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            alertView.tag=39493;
            if (showAllert) {
                [alertView show];
            }
                });
            }
//        });
    }];
    [downloadTask resume];
}

/*
 * Method For Going to the home page when clicked on home button
 */

-(void)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    [[self getPlayerView] removeFromSuperview];
}


-(void)arrowAction:(id)sender
{
    [[self getPlayerView] removeFromSuperview];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"You have pressed the %@ button", [actionSheet buttonTitleAtIndex:buttonIndex]);
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqual:@"Facebook"]) {
        [self facebookPost];
    } else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqual:@"Twitter"]) {
        [self twitterPost];
    }
}


-(void)twitterPost{
    NSString *stringURL;
    if (self.isDownloads)
    {
        KPDownLoadInfo *info=[self.videoArray objectAtIndex:self.selectedIndex];
        stringURL=info.serverURL;
    }
    else
        stringURL=self.categoryInfo.file_url;
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet setInitialText:[NSString stringWithFormat:@"Klippat iOS App"]];
    [tweetSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];
    [tweetSheet addImage:[self.player thumbnailImageAtTime:timeCounter timeOption:MPMovieTimeOptionNearestKeyFrame]];
    [self presentModalViewController:tweetSheet animated:YES];
}


-(void)shareOnFBWithIfNotLogin {
    
    
    NSString *stringURL;
    if (self.isDownloads)
    {
        KPDownLoadInfo *info=[self.videoArray objectAtIndex:self.selectedIndex];
        stringURL=info.serverURL;
    }
    else
        stringURL=self.categoryInfo.file_url;
    
    SLComposeViewController *controller = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
    SLComposeViewControllerCompletionHandler myBlock =
    ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled) {
            NSLog(@"Cancelled");
        } else {
            NSLog(@"Done");
        }
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    controller.completionHandler =myBlock;
    [controller addImage:[self.player thumbnailImageAtTime:timeCounter timeOption:MPMovieTimeOptionNearestKeyFrame]];
    //Adding the Text to the facebook post value from iOS
    [controller setInitialText:@"Klippat iOS app \n"];
    
    [controller addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]];

    //Adding the URL to the facebook post value from iOS
    NSLog(@"video url %@",self.categoryInfo.file_url);

    //Adding the Text to the facebook post value from iOS
    [self presentViewController:controller animated:YES completion:nil];
}

-(NSString *)getStringFromImage:(UIImage *)image
{
	if(image)
    {
		NSData *dataObj = UIImagePNGRepresentation(image);
        NSLog(@"string base 64endcoding");
		return [dataObj base64Encoding];
	} else {
		return @"";
	}
}

-(IBAction)facebookPost
{
    
    NSLog(@"FB session is open or not : %hhd", [FBSession.activeSession isOpen]);
    
    if (!FBSession.activeSession.isOpen)
    {
        [self shareOnFBWithIfNotLogin];
    } else {
        [self postPhoto];
    }
}

-(void)postPhoto
{
    // We're going to assume you have a UIImage named image_ stored somewhere.

    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    UIImage *image=[self.player thumbnailImageAtTime:timeCounter timeOption:MPMovieTimeOptionNearestKeyFrame];
    NSString *stringURL;
    if (self.isDownloads)
    {
        KPDownLoadInfo *info=[self.videoArray objectAtIndex:self.selectedIndex];
        stringURL=info.serverURL;
    }
    else
        stringURL=self.categoryInfo.file_url;

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSString stringWithFormat:@"%@ %@", @"Kilppat has shared video.NOW WATCH ?", [stringURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forKey:@"message"];

//    [params setObject:@"http://www.google.com" forKey:@"url"];
//    [params setObject:@"it is fun" forKey:@"description"];
//    [params setObject:@"http://itsti.me/" forKey:@"href"];
    [params setObject:image forKey:@"picture"];
    
        // First request uploads the photo.
    FBRequest *request1 = [FBRequest requestForUploadPhoto:image];
    [[request1 parameters] setDictionary:params];
    [connection addRequest:request1 completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSString *ID = [result objectForKey:@"id"];
             NSLog(@"photo idddd %@",ID);
             //[self postDataWithPhoto:ID];
             showAlertView(nil, @"Shared successfully.");
             

         }
     }
      batchEntryName:@"photopost"
     ];
    
//    // Second request retrieves photo information for just-created
//    // photo so we can grab its source.
//    FBRequest *request2 = [FBRequest  requestForGraphPath:@"{result=photopost:$.id}"];
//    [connection addRequest:request2
//         completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//             if (!error && result) {
//                 NSString *ID = [result objectForKey:@"id"];
//                 NSLog(@"photo idddd %@",ID);
//                 [self postDataWithPhoto:ID];
//             } else {
//                 NSLog(@"id fb not login");
//                 [self shareOnFBWithIfNotLogin];
//             }
//         }];
    [connection start];
}


-(void)postDataWithPhoto:(NSString*)photoID
{
    NSString *stringURL;
    if (self.isDownloads)
    {
        KPDownLoadInfo *info=[self.videoArray objectAtIndex:self.selectedIndex];
        stringURL=info.serverURL;
    }
    else
        stringURL=self.categoryInfo.file_url;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"Shared by klippat iOS App" forKey:@"message"];
   // [params setObject:self.categoryInfo.file_url forKey:@"picture"];
    [params setObject:stringURL forKey:@"link"];
    if(photoID) {
        [params setObject:photoID forKey:@"object_attachment"];
    }
    
    [FBRequestConnection startForPostWithGraphPath:@"me/feed"
                                       graphObject:(FBGraphObject*)[NSDictionary dictionaryWithDictionary:params]
                                 completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error) {
         
         NSLog(@"FBRequestConnection :- %@", error);
         
////    NSMutableDictionary *params = [NSMutableDictionary dictionary];
////    [params setObject:@"Klippat iOS App!" forKey:@"message"];
////    [params setObject:imageData forKey:@"source"];
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Mickey Mouse copy" ofType:@"mp4"];
//    NSData *videoData = [NSData dataWithContentsOfFile:filePath];
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   videoData,@"video.mp4",
//                                   @"video/mp4", @"contentType",
//                                   @"Movie", @"name",
//                                   @"This is good video", @"description",
//                                   nil];
//    [FBRequestConnection startWithGraphPath:@"me/videos"
//                                 parameters:params
//                                 HTTPMethod:@"POST"
//                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             [[[UIAlertView alloc] initWithTitle:nil
                                         message:@"Successfully shared on Facebook."
                                        delegate:self
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil] show];
         } else {
             
             [[[UIAlertView alloc] initWithTitle:@"Error"
                                         message:[NSString stringWithFormat:@"%@",@"Failed to share on Facebook"]
                                        delegate:nil
                               cancelButtonTitle:@"Ok"
                               otherButtonTitles:nil] show];
         }
     }
     ];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    NSLog(@"willRotateToInterfaceOrientation %d",toInterfaceOrientation);
    if (toInterfaceOrientation ==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight) {
        NSLog(@"UIInterfaceOrientationLandscape");
        
        [self.player.view setFrame:CGRectMake(0, 0, IS_IPHONE_5?568:480, 320)];
        NSLog(@"height %f and width %f",self.view.frame.size.height,self.view.frame.size.width);
        isLandscapeMode=YES;
        UIView *tempView=toolBarView;
        UIView *titleView=(UIView*)[self.player.view viewWithTag:7348];
        if(tempView.superview!=self.view)
        {
            [titleView removeFromSuperview];
            [tempView removeFromSuperview];
            [tempView setFrame:CGRectMake(0,self.view.frame.size.height-110, 320,59)];
            [self.view addSubview:tempView];
        }

    }
    else
    {
        [self.player.view setFrame:CGRectMake(0, 0, 320, IS_IPHONE_5?568:480)];
        UIView *tempView=toolBarView;
        UIView *titleView=(UIView*)[self.player.view viewWithTag:7348];
        if(tempView.superview!=self.view)
        {
            [titleView removeFromSuperview];
            [tempView removeFromSuperview];
            [tempView setFrame:CGRectMake(0,self.view.frame.size.height-110, 320,59)];
            [self.view addSubview:tempView];

        }

        isLandscapeMode=NO;
    }
}
@end
@interface UIDevice (MyPrivateNameThatAppleWouldNeverUseGoesHere)
- (void) setOrientation:(UIInterfaceOrientation)orientation;
@end