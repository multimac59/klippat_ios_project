//
//  KPVideoViewController.h
//  Klippat
//
//  Created by Zahid on 17/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPSonngCategoryInfo.h"
#import "AFURLSessionManager.h"
#import "KPCommonViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "KPPlayListViewController.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface KPVideoViewController : KPCommonViewController
{
    
}
@property (nonatomic, strong) SLComposeViewController *myFacebookSLComposerSheet;

@property(nonatomic,strong)MPMoviePlayerController *player;
@property(nonatomic,strong)UISlider *slider;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,strong)NSArray *videoArray;
@property(nonatomic)NSInteger selectedIndex;
@property(nonatomic)BOOL isDownloads;

@property (nonatomic, strong) KPSonngCategoryInfo *categoryInfo;
@end
