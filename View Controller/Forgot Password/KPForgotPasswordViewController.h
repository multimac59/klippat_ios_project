//
//  KPForgotPasswordViewController.h
//  Klippat
//
//  Created by Ravi kumar on 17/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"

@interface KPForgotPasswordViewController : KPCommonViewController
@property(strong,nonatomic)UITextField *textField;

@end
