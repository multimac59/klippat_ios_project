//
//  KPForgotPasswordViewController.m
//  Klippat
//
//  Created by Ravi kumar on 17/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPForgotPasswordViewController.h"
#import "KPHomeViewController.h"
@interface KPForgotPasswordViewController ()
@end

@implementation KPForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self addTextFieldWithFrame:CGRectMake(25, 134, self.view.frame.size.width-50, 33) placeholder:NSLocalizedString(@"Email ID", nil) tag:50];
    [self addButtonWithFrame:CGRectMake(115, 179, 85, 33) titel:NSLocalizedString(@"Submit", nil) tag:11 ];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 49, 320, 49)];
    [self addTopView];
}

-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"hos"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 41)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 8, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:[NSString stringWithFormat:@"FORGOT PASSWORD"]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
}

-(IBAction)homeButtonAction:(id)sender
{
    if (![KPSingletonClass sharedObject].userID) {
       [KPSingletonClass sharedObject].userID=@"0";
    }
    KPAppDelegate *app=(KPAppDelegate*)[UIApplication sharedApplication].delegate;
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController pushViewController:app.tabListController animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addTextFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder  tag:(int)tag
{
    self.textField=[[UITextField alloc]initWithFrame:frame];
    [self.textField setTag:tag];
    [self.textField setBackgroundColor:[UIColor whiteColor]];
    [self.textField setBackgroundColor:[UIColor colorWithRed:207/255.0 green:208/255.0 blue:212/255.0 alpha:1.0]];
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, frame.size.height)];
    [self.textField setLeftViewMode:UITextFieldViewModeAlways];
    [self.textField setTextAlignment:NSTextAlignmentLeft];
    NSAttributedString *attPlaceholder=[[NSAttributedString alloc]initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:125/255.0 green:145/255.0 blue:190/255.0 alpha:1.0],NSFontAttributeName:[UIFont systemFontOfSize:13]}];
    [self.textField setAttributedPlaceholder:attPlaceholder];
    self.textField .contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.textField setLeftView:leftView];
    [self.textField setFont:[UIFont systemFontOfSize:13]];
    //self.textField.delegate=self;
    [self.textField setDelegate:(id)self];
    [self.view addSubview:self.textField];
}
-(void)addButtonWithFrame:(CGRect)frame titel:(NSString*)title  tag:(int)tag
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTag:tag];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [button setTitle:title forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"button"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button];
    
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(IBAction)buttonClickAction:(id)sender
{
    NSLog(@"forgetpassword");
   //UITextField *email=(UITextField*)[self.view viewWithTag:50];
    [self.textField resignFirstResponder];
    self.textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [self.textField setTextAlignment:NSTextAlignmentNatural]
    ;
    NSString *email= self.textField.text;
    
    if ([email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0) {
        showAlertView(nil, @"Please enter Email ID");
        return;
    }
    else if (![self validateEmailWithString:[email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]])
    {
        showAlertView(nil, @"Please provide valid Email ID");
        return;
    }
    
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:email forKey:@"email"];
    [infoDict setValue:@"user_forgot_password" forKey:@"task"];
    [self serviceRequestForForgotPasswordWithInfo:infoDict flag:NO];
}

-(void)serviceRequestForForgotPasswordWithInfo:(NSMutableDictionary*)infoDict flag:(BOOL)tempFlag
{
    KPParser *parser=[KPParser new];
    __block BOOL flag=tempFlag;
    showActivity(self.navigationController.view);
    [parser serviceRequestWithInfo:infoDict serviceType:KPForgotPasswordType responseBlock:^(id response, NSError *error) {
        NSDictionary *resDict=(NSDictionary*)[response valueForKey:@"response"];
        NSLog(@"forgot passward %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response ==nil) {
                if (!flag) {
                    [self serviceRequestForForgotPasswordWithInfo:infoDict flag:YES];
                }
                else
                showAlertView(nil, @"Network error.");
            }
            else if ([[resDict valueForKey:@"success"] isEqualToString:@"true"]) {
                NSLog(@"success forgot password");
                showAlertView(nil,[resDict valueForKey:@"user_information"]);
            }
            else if ([[resDict valueForKey:@"success"] isEqualToString:@"false"])
            {
                showAlertView(nil, @"Email address does not exist.");
            }
        });
    }];

}
-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow"];
   // UIImage *arrow1 = [UIImage imageNamed:@""];
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:nil forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 -15, arrow.size.width , arrow.size.height);
    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    
    [self.view addSubview:viewNavigation];
}
-(IBAction)arrowAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
