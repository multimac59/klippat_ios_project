//
//  KPCommonViewController.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface KPCommonViewController ()

@end

@implementation KPCommonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.view setBackgroundColor:[UIColor colorWithRed:197/255.0 green:171/255.0 blue:135/255.0 alpha:1.0]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:(deviceFactor==1)?@"bg":@"bg_i4"]]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
void showAlertView(NSString *title,NSString *message)
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

-(void)addFileToPlayListWith:(NSString *)fileId categoryId:(NSString *)cat_id playlistId:(NSString *)playlistId callBack:(CustomBlock)block
{
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"files_add_playlist" forKey:@"task"];
    [infoDict setValue:fileId forKey:@"file_id"];
    [infoDict setValue:cat_id forKey:@"category_id"];
    [infoDict setValue:playlistId forKey:@"user_playlist_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPAddFileToPlaylistType responseBlock:^(id response, NSError *error) {
        NSLog(@"response %@",response);
        block(response,error);
    }];
}

-(void)addFileToFavouritesWithFileId:(NSString *)fileId categoryId:(NSString *)cat_id  callBack:(CustomBlock)block
{
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"my_favourite_save" forKey:@"task"];
    [infoDict setValue:fileId forKey:@"file_id"];
    [infoDict setValue:cat_id forKey:@"files_category_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPAddFileToPFavouriteType responseBlock:^(id response, NSError *error) {
        NSLog(@"response %@",response);
        block(response,error);
    }];

    
}
@end
