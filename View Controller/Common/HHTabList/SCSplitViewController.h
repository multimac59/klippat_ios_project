//
//  SCSplitViewController.h
//  Sociyo
//
//  Created by Komal Verma on 21/10/13.
//  Copyright (c) 2013 Narendra kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCSplitViewController : UIViewController


- (id)init;

@property (nonatomic, strong) IBOutlet UILabel *label;

@end
