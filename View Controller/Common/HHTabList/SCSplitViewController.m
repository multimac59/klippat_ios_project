//
//  SCSplitViewController.m
//  Sociyo
//
//  Created by Komal Verma on 21/10/13.
//  Copyright (c) 2013 Narendra kumar. All rights reserved.
//
#import "HHTabListController.h"

#import "SCSplitViewController.h"

@interface SCSplitViewController ()

@end

@implementation SCSplitViewController

#pragma mark -
#pragma mark Initialization

- (id)init
{
	self = [super init];
	
	if (self != nil) {
		
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.label.text = self.title;
}


#pragma mark -
#pragma mark Accessors

@synthesize label = _label;


#pragma mark -
#pragma mark Lifecycle

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if ([self isMovingToParentViewController]) {
		HHTabListController *tabListController = [self tabListController];
		UIBarButtonItem *leftBarButtonItem = tabListController.revealTabListBarButtonItem;
		self.navigationItem.leftBarButtonItem = leftBarButtonItem;
	}
}


#pragma mark -
#pragma mark Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
