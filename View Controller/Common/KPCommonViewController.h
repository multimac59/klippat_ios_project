//
//  KPCommonViewController.h
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPParser.h"
#import "KPAppDelegate.h"
#import "KPSingletonClass.h"
#import "DSActivityIndicator.h"
#import  "UIImageView+WebCache.h"
typedef void (^CustomBlock) (id result,NSError *error);
@interface KPCommonViewController : UIViewController
@property(nonatomic,strong)CustomBlock customBlock;
void showAlertView(NSString *title,NSString *message);
-(void)addFileToPlayListWith:(NSString*)fileId categoryId:(NSString*)cat_id playlistId:(NSString*)playlistId callBack:(CustomBlock)block;
-(void)addFileToFavouritesWithFileId:(NSString *)fileId categoryId:(NSString *)cat_id callBack:(CustomBlock)block;

@end
