//
//  KPSettingViewController.m
//  Klippat
//
//  Created by Zahid on 15/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSettingViewController.h"
#import "HHTabListController.h"
#import "KPHomeViewController.h"
@interface KPSettingViewController ()
{
    HHTabListController *tabListController;
}
@end

@implementation KPSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES];
    [self addInitialViewComponent];
    [self.view addSubview:[self addButtonWithFrame:CGRectMake(24, self.view.frame.size.height/2-94, 267, 38) title:@"   Privacy Policy" tag:1236 titleColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0] roundedCorner:YES]];//TextColor
    [self.view addSubview:[self addButtonWithFrame:CGRectMake(24, self.view.frame.size.height/2-24, 267, 38) title:@"   Terms and Conditions" tag:1237 titleColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0] roundedCorner:YES]];
    [self addLabel];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addInitialViewComponent
{
    
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addTopView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self isMovingToParentViewController]) {
        tabListController = [self tabListController];
        tabListController.delegate=(id)self;
        
    }
    
    [self.navigationController setNavigationBarHidden:YES];

}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg.png"];
    UIImage *homeImage=[UIImage imageNamed:@"menu-icon"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(10, 5, homeImage.size.width, homeImage.size.height)];//x=30
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:tabListController action:@selector(revealTabList:) forControlEvents:UIControlEventTouchDown];
    
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:@"SETTINGS"];
    [titleLbl setFont:[UIFont fontWithName:@"HelveticaNeueInterface-Regular" size:16]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
    
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"+" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:35]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
}

-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
   // UIImage *arrow = [UIImage imageNamed:@"arow.png"];
       // [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 16, arrow.size.width , arrow.size.height ) tag:778 navigationBar:viewNavigation BGImage:arrow];
        [self.view addSubview:viewNavigation];
}
-(void)homeButtonAction:(id)sender
{
    NSLog(@"home cv");
    [self navigateToHomeScreen];
}
-(void)navigateToHomeScreen {
    
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];

    
    [self.navigationController popViewControllerAnimated:YES];
//    HHTabListController *vc;
//    for(id controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[HHTabListController class]]) {
//            vc=(HHTabListController*)controller;
//            [self.navigationController popToViewController:controller animated:YES];
//            break;
//        }
//    }
//    if (!vc) {
//        KPAppDelegate *app=(KPAppDelegate *)[UIApplication sharedApplication].delegate;
//        [self.navigationController pushViewController:app.tabListController animated:YES];
//        
//    }

}
-(UIButton*)addButtonWithFrame:(CGRect)frame title:(NSString*)title tag:(int)tagValue titleColor:(UIColor*)titleColor roundedCorner:(BOOL)cornerRounded
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setTag:tagValue];
    [button setFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeueInterface-Regular" size:16]];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    [button setBackgroundImage:[UIImage imageNamed:@"txt.png"] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    //[button.layer setBorderWidth:1.0];
    [button addTarget:self action:@selector(ButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}
-(void)addLabel
{
    UILabel *aLabel = [[UILabel alloc]initWithFrame:
                       CGRectMake(24,self.view.frame.size.height/2+46 , 267, 38)];
    aLabel.numberOfLines = 0;
    aLabel.textColor = [UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    aLabel.textAlignment = NSTextAlignmentLeft;
    NSLog(@"lable fornntt %@",aLabel.font);
    [aLabel setFont:[UIFont fontWithName:@"HelveticaNeueInterface-Regular" size:16]];
    aLabel.text = @"   Notification";
    aLabel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"txt.png"]];
    aLabel.userInteractionEnabled=YES;
    //uiswitch button
   UISwitch *mySwitch = [[UISwitch alloc] init];
    mySwitch.center = CGPointMake(230, 19);
    //mySwitch.backgroundColor = [UIColor colorWithRed:70.0/255 green:102.0/255 blue:202.0/255 alpha:1.0];
    mySwitch.tintColor=[UIColor whiteColor];
    mySwitch.onTintColor=[UIColor colorWithRed:70.0/255 green:102.0/255 blue:202.0/255 alpha:1.0];
 [mySwitch addTarget:self action:@selector(switched:)
       forControlEvents:UIControlEventValueChanged];
    mySwitch.layer.cornerRadius=14.0;
    [aLabel addSubview:mySwitch];
    if (([[KPSingletonClass sharedObject].userID isEqualToString:@"0"])) {
        [mySwitch setUserInteractionEnabled:NO];
        [aLabel setHidden:YES];
    }
    [self.view addSubview:aLabel];
}
-(IBAction)switched:(UISwitch*)sender{
    if(sender.on){
        NSLog(@"Switch is ON");
       // NSLog(@"senderValue %@:",(id)sender);
        [self requestForChangeNotification:1];
    } else {
        [self requestForChangeNotification:0];
        NSLog(@"Switch is OFF");
        // NSLog(@"senderValue %@:",(id)sender);
    }
}

-(IBAction)ButtonClickAction:(UIButton*)sender
{
    NSLog(@"ButtonClickAction %ld and title is %@",(long)sender.tag,sender.titleLabel.text);
    if(sender.tag==1236)
    {
        [self requestForGetPrivacyPolicyWithFlag:NO];
        
    }
    else
    {
        [self requestForGetTermsConditionsWithFlag:NO];
    }
    
}

-(void)arrowAction:(id)sender
{
    NSLog(@"arrowAction");
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)requestForGetPrivacyPolicyWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"get_privacy_policy" forKey:@"task"];
    
    NSLog(@"%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetPrivacyPolicyType responseBlock:^(id response, NSError *error) {
        
        NSLog(@"get_privacy_policy %@", response);
        id resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                if (!flag) {
                    [self requestForGetPrivacyPolicyWithFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            }
            else {
                id responseIdAgain = [resDict objectForKey:@"privacy_policy"];

                NSMutableDictionary *message = [responseIdAgain
                                                objectAtIndex:0];
                [self messagePopUpView:[message objectForKey:@"privacy_policy"]];
            }
        });
        
    }];
}

-(void)messagePopUpView:(NSString *)string_message {
    
    UIView *popUp=[UIView new];
    [popUp setFrame:CGRectMake(40, self.view.frame.origin.x +80, [UIScreen mainScreen].bounds.size.width - 80, [UIScreen mainScreen].bounds.size.height - 180)];
    [popUp setBackgroundColor:[UIColor whiteColor]];
    [popUp setTag:1110];
   
    [popUp.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    //[popUp.layer setBorderWidth:2.0f];
    [popUp.layer setCornerRadius:3.0f];
    UIWebView *policyLb=[[UIWebView alloc] init];
    [policyLb setFrame:CGRectMake(5, 10,popUp.frame.size.width - 10,popUp.frame.size.height-20)];
    [policyLb setBackgroundColor:[UIColor lightGrayColor]];
    
    NSString *embedHTML = [NSString stringWithFormat:@"<html><head></head><body><p>%@</p></body></html>", string_message];
    [policyLb loadHTMLString:embedHTML baseURL:nil];

    [popUp addSubview:policyLb];
    UIButton *close=[[UIButton alloc]initWithFrame:CGRectMake(220,-10,30,30)];
    
    [close setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [close setBackgroundImage:[UIImage imageNamed:@"Button Blue Close.png"] forState:UIControlStateNormal];
    [popUp bringSubviewToFront:close];
    [close.titleLabel setFont:[UIFont systemFontOfSize:15]];
    
    [close addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    [popUp addSubview:close];

    [self.view addSubview:popUp];
}

-(void)closeAction:(UIButton*)sender
{
    [sender.superview removeFromSuperview];
    NSLog(@"close");
}
-(IBAction)cancelAction:(id)sender
{
    NSLog(@"cansel popup view");
   // [self.view viewWithTag:1110 ];
   
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    UIScrollView *touchView = (UIScrollView *)[self.view viewWithTag:1110];
    if (touch.view.tag == 1110) {
        
    }else
    [touchView removeFromSuperview];
    
}


-(void)requestForGetTermsConditionsWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"get_terms_conditions" forKey:@"task"];
    
    NSLog(@"%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetPrivacyPolicyType responseBlock:^(id response, NSError *error) {
        
        NSLog(@"Term and condition %@", response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                if (!flag) {
                    [self requestForGetPrivacyPolicyWithFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            }
            else {
                id responseIdAgain = [resDict objectForKey:@"terms_conditions"];
                
                NSMutableDictionary *message = [responseIdAgain
                                                objectAtIndex:0];
                [self messagePopUpView:[message objectForKey:@"terms_conditions"]];
            }
        });
        
    }];
}

-(void)requestForChangeNotification:(NSInteger)notifyInt
{
   
  
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"change_notification" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [infoDict setValue:[NSString stringWithFormat: @"%ld",(long)notifyInt] forKey:@"notification"];
        NSLog(@"infoDict %@:",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPChangeNotification responseBlock:^(id response, NSError *error) {
        NSLog(@"Notification %@", response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                showAlertView(nil, @"Network error.");
            }
            else {
               
                NSLog(@"notificationresponce %@:",[resDict objectForKey:@"notification"]);
                
            }

        });
        
    }];
}

@end
