//
//  KPSearchCell.h
//  Klippat
//
//  Created by Zahid on 16/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPSearchCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIImageView *thumbnail;
@property(nonatomic,strong)UIImageView *iconPlay;
@end
