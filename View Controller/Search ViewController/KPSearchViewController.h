//
//  KPSearchViewController.h
//  Klippat
//
//  Created by Zahid on 16/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"
#import "KPSearchCell.h"

@interface KPSearchViewController : KPCommonViewController

@property(nonatomic,strong)NSMutableArray *searchListItemArray;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic, retain) NSMutableArray	*filteredListContent;	// The content filtered as a result of a search.
@property(nonatomic, assign) BOOL isSearchActive;
@end
