//
//  KPSearchCell.m
//  Klippat
//
//  Created by Zahid on 16/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSearchCell.h"

@implementation KPSearchCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *container=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 280, 55)];
        [container setBackgroundColor:[UIColor grayColor]];
        [container.layer setBorderColor:[UIColor blackColor].CGColor];
        [container setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBG1.png"]]];
        UIImage *image=[UIImage imageNamed:@"cellBG1"];
        NSLog(@"dynamic image size %@",NSStringFromCGSize(image.size));
        
        self.thumbnail =[[UIImageView alloc]initWithFrame:CGRectMake(5, 10.5, 35, 35)];
        [container addSubview:self.thumbnail];
        
        self.iconPlay =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
        [self.iconPlay setImage:[UIImage imageNamed:@"video-play"]];
        [self.thumbnail addSubview:self.iconPlay];
        [self.thumbnail bringSubviewToFront:self.iconPlay];
        [self.iconPlay setHidden:YES];
        self.titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(45, 14, 200, 30)];
        self.textLabel.textAlignment=NSTextAlignmentLeft;
        [container addSubview:self.titleLabel];
        
        [self addSubview:container];
    }
    return self;

}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
