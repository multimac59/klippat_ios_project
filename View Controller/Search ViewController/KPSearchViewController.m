//
//  KPSearchViewController.m
//  Klippat
//
//  Created by Zahid on 16/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "SearchInfo.h"
#import "KPSonngCategoryInfo.h"
#import "KPVideoViewController.h"
#import "KPCoreDataInitializer.h"
#import "KPSearchViewController.h"
#import "HHTabListController.h"
#import "KPHomeViewController.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>
@interface KPSearchViewController ()
@property(nonatomic,strong)NSMutableDictionary * thumbnailDict;
@end

@implementation KPSearchViewController
@synthesize searchListItemArray=_searchListItemArray;
@synthesize tableView=_tableView;
@synthesize  filteredListContent = _filteredListContent;
-(NSMutableDictionary*)thumbnailDict
{
    if (!_thumbnailDict) {
        _thumbnailDict=[NSMutableDictionary new];
    }
    return _thumbnailDict;
}

-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    return _tableView;
}
-(NSMutableArray*)searchListItemArray
{
    if (!_searchListItemArray) {
        _searchListItemArray=[[NSMutableArray alloc]init];
    }
    
    return _searchListItemArray;
    
}
-(NSMutableArray*)filteredListContent
{
    if (!_filteredListContent) {
        _filteredListContent=[NSMutableArray new];
    }
    return _filteredListContent;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)callSearchService{
    
    KPCoreDataInitializer *coreDataIn = [KPCoreDataInitializer sharedMySingleton];
    [coreDataIn initializeTheCoreDataModelClasses];
    
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"search_files" forKeyPath:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].language forKeyPath:@"language_type"];
    [infoDict setValue:@"AllData" forKeyPath:@"file_name"];//user_id
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKeyPath:@"user_id"];

    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPSearchServiceType responseBlock:^(id response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
        });
        id responseDict = [[response objectForKey:@"response"] objectForKey:@"search_result"];
        if ([responseDict isKindOfClass:[NSMutableDictionary class]]) {
            NSMutableDictionary *infoDict = response;
            [coreDataIn deleteAllRows];
            NSLog(@"category detail dictt %@",response);
            KPSearchDataINfo *searchInfo = [[KPSearchDataINfo alloc] init];
            [searchInfo setCategory_id:[infoDict objectForKey:@"category_id"]];
            [searchInfo setFile_duration:[infoDict objectForKey:@"file_duration"]];
            [searchInfo setFile_id:[infoDict objectForKey:@"file_id"]];
            [searchInfo setFile_name:[infoDict objectForKey:@"file_name"]];
            NSString *fileURL=[infoDict valueForKey:@"file_url"];
            [searchInfo setFile_url:[fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
            [searchInfo setPerson_name:[infoDict objectForKey:@"person_name"]];
            [searchInfo setIs_favourite:[infoDict objectForKey:@"favourite_status"]];
            [searchInfo setType:[infoDict objectForKey:@"type"]];

            [dataArray addObject:searchInfo];
        }else {
            NSMutableArray *infoArray = responseDict;
            if (responseDict) {
                [coreDataIn deleteAllRows];
                NSLog(@"category detail array %@",response);
                [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NSMutableDictionary *infoDict = obj;
                    KPSearchDataINfo *searchInfo = [[KPSearchDataINfo alloc] init];
                    [searchInfo setCategory_id:[infoDict objectForKey:@"category_id"]];
                    [searchInfo setFile_duration:[infoDict objectForKey:@"file_duration"]];
                    [searchInfo setFile_id:[infoDict objectForKey:@"file_id"]];
                    [searchInfo setFile_name:[infoDict objectForKey:@"file_name"]];
                    NSString *fileURL=[infoDict valueForKey:@"file_url"];
                    [searchInfo setFile_url:[fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
                    [searchInfo setPerson_name:[infoDict objectForKey:@"person_name"]];
                    [searchInfo setIs_favourite:[infoDict objectForKey:@"favourite_status"]];
                    [searchInfo setType:[infoDict objectForKey:@"type"]];
                    [dataArray addObject:searchInfo];
                }];
            }
        }
        [dataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            KPSearchDataINfo *searchInfo = obj;
            BOOL isAdded = [coreDataIn addSearchInformation:searchInfo];
            NSLog(@"isAdded %d", isAdded);
        }];
        [self.searchListItemArray removeAllObjects];
        self.searchListItemArray = [coreDataIn fetchSearchInfo];
        if (self.isSearchActive) {
            UITextField *searchTf=(UITextField*)[self.view viewWithTag:3749];
            [self filterContentForSearchText:searchTf.text];
        }
        [self.tableView reloadData];
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addInitialViewComponent];
    KPCoreDataInitializer *dataInitializer = [KPCoreDataInitializer sharedMySingleton];
    [dataInitializer initializeTheCoreDataModelClasses];
    self.searchListItemArray = [dataInitializer fetchSearchInfo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addInitialViewComponent
{
    [self.tableView setFrame:CGRectMake(25, 125, self.view.frame.size.width-50, (deviceFactor==1)?380:318)];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    if ([[UIDevice currentDevice]systemVersion].floatValue>=7 ) {
        self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    [self.tableView setShowsVerticalScrollIndicator:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    [self addTopView];
    [self addCustomSearchBar];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self refreshData:nil];
}
-(void)addLocalNavigationBar:(CGRect)frame
{
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow.png"];
   
    [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 16, arrow.size.width , arrow.size.height) tag:778 navigationBar:viewNavigation BGImage:arrow];
    
    [self.view addSubview:viewNavigation];
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"+" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:35]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
    
    UIButton* btnRefresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRefresh setFrame:CGRectMake(320 - 60, navi_bar.frame.size.height/2 - [UIImage imageNamed:@"refresh"].size.height/2, [UIImage imageNamed:@"refresh"].size.width, [UIImage imageNamed:@"refresh"].size.height)];

    [btnRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    [btnRefresh setTitle:@"" forState:UIControlStateNormal];
    [btnRefresh addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventTouchUpInside];
    [navi_bar addSubview:btnRefresh];

}

-(IBAction)refreshData:(id)sender {
    showActivity(self.navigationController.view);
    [self callSearchService];
}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg.png"];
    UIImage *homeImage=[UIImage imageNamed:@"home"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 40)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(30, 8, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:@"SEARCH"];
    [titleLbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    NSLog(@"HelveticaNeueInterface %@",titleLbl.font);
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
        
    
}
-(void)addCustomSearchBar
{
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:CGRectMake(25, 85, 270, 30)];
    [search_txtF setDelegate:(id)self];
    [search_txtF setTag:3749];
[search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
    search_txtF.textColor=[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    UIImage *searchImage=[UIImage imageNamed:@"search"];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: TextColor,NSFontAttributeName:[UIFont italicSystemFontOfSize:12.0]}];
    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 6, searchImage.size.width , searchImage.size.height)];
    [searchIcon setImage:searchImage];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:search_txtF];
}

-(void)homeButtonAction:(id)sender {
    
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    [self.navigationController popViewControllerAnimated:YES];
//    HHTabListController *vc;
//    for(id controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[HHTabListController class]]) {
//            vc=(HHTabListController*)controller;
//            [self.navigationController popToViewController:controller animated:YES];
//            break;
//        }
//    }
//    if (!vc) {
//        KPAppDelegate *app=(KPAppDelegate*)[UIApplication sharedApplication].delegate;
//        [self.navigationController pushViewController:app.tabListController animated:YES];
//  
//    }
}

-(void)generateThumnailFromURL:(NSString*)url forCell:(KPSearchCell*)cell indext:(int)index
{
    NSString *indexKey=[NSString stringWithFormat:@"%d",index];
    NSLog(@"iamgeview detail %@",cell.thumbnail.image);
    if ([self.thumbnailDict valueForKey:indexKey]) {
        NSLog(@"iamgeview detail not NULL");
        [cell.iconPlay setHidden:NO];
        [cell.thumbnail setImage:[self.thumbnailDict valueForKey:indexKey]];
                  return;
    }
    else
    {
   __block UIImageView *tempImageView=cell.thumbnail;
    AVURLAsset *asset=[[AVURLAsset alloc] initWithURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform=TRUE;
    CMTime thumbTime = CMTimeMakeWithSeconds(3,30);
    
    AVAssetImageGeneratorCompletionHandler handler = ^(CMTime requestedTime, CGImageRef im, CMTime actualTime, AVAssetImageGeneratorResult result, NSError *error){
        if (result != AVAssetImageGeneratorSucceeded) {
            NSLog(@"couldn't generate thumbnail, error:%@", error);
        }
        UIImage *tempImg=[UIImage imageWithCGImage:im];
        tempImageView.image=tempImg;
        [[tempImageView layer]setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"video-play"]].CGColor];
        if (tempImg) {
            [cell.iconPlay setHidden:NO];
            [self.thumbnailDict setObject:tempImageView.image forKey:indexKey];
        }
        NSLog(@"thumbnailimage %@", tempImageView.image);
    };
    
    CGSize maxSize = CGSizeMake(320, 180);
    generator.maximumSize = maxSize;
    [generator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:thumbTime]] completionHandler:handler];
    }
}

-(void)arrowAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView Delegate And DataSorce

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (deviceFactor==1)? 65 : 63;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchInfo *search =[(!self.isSearchActive)? self.searchListItemArray : self.filteredListContent objectAtIndex:indexPath.row];
    KPSonngCategoryInfo *categoryInfo = [[KPSonngCategoryInfo alloc] init];
    categoryInfo.file_name = search.file_name;
    categoryInfo.file_duration = search.file_duration;
    categoryInfo.file_id = search.file_id;
    categoryInfo.file_url = search.file_url;
    categoryInfo.person_name = search.person_name;
    categoryInfo.category_id=search.category_id;
    categoryInfo.type=search.type;
    categoryInfo.favourite_status=search.is_favourite;
    NSMutableArray *dataArray = [NSMutableArray new];
    [dataArray addObject:categoryInfo];
    if ([search.type isEqualToString:@"movies"] ||[search.type isEqualToString:@"clips"]) {
        KPVideoViewController *vc=[KPVideoViewController new];
        [vc setCategoryInfo:categoryInfo];
        [self.navigationController pushViewController:vc animated:YES];

    }else {
        KPImagePlayerViewController *obj=[KPImagePlayerViewController new];
        obj.imageArray = dataArray;
        obj.selectedIndex=0;
        [self.navigationController pushViewController:obj animated:YES];
    }
}
-(void)addNoDataFoundLabel
{
    UILabel *label=(UILabel*)[self.view viewWithTag:8493];
    if (label) {
        [label removeFromSuperview];
        label=nil;
    }
   label=[[UILabel alloc]initWithFrame:CGRectMake(20, 160, 280, 20)];
    [label setText:@"No Data Found!"];
    [label setTag:8493];
    [label setTextColor:[UIColor blueColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [self.view addSubview:label];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{   NSLog(@"%@",self.searchListItemArray);
   
    int rowCount=(!self.isSearchActive) ? self.searchListItemArray.count : self.filteredListContent.count;
    if (rowCount==0) {
        [self addNoDataFoundLabel];
    }
    else
    {
        UILabel *label=(UILabel*)[self.view viewWithTag:8493];
        if (label) {
            [label removeFromSuperview];
            label=nil;
        }
    }
    return rowCount;
    
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPSearchCell *cell=(KPSearchCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[KPSearchCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    [cell.iconPlay setHidden:YES];
    SearchInfo *search =[(!self.isSearchActive)? self.searchListItemArray : self.filteredListContent objectAtIndex:indexPath.row];
    if ([search.type isEqualToString:@"photos"]||[search.type isEqualToString:@"notes"]) {
        [cell.thumbnail setImageWithURL:[NSURL URLWithString:search.file_url] placeholderImage:[UIImage imageNamed:@"gallery2"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType){
        }];
    }
    else
    {
    [cell.thumbnail setImage:[UIImage imageNamed:@"media"]];
    [self generateThumnailFromURL:search.file_url forCell:cell indext:indexPath.row];
    }
    [cell.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16 ]];
    [cell.titleLabel setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [cell.titleLabel setText:[NSString stringWithFormat:@" %@",search.file_name]];
    
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *textSearch=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textSearch.length>0) {
        self.isSearchActive=YES;
        [self filterContentForSearchText:textSearch];
    } else {
        self.isSearchActive=NO;
        [self.tableView reloadData];
    }
    return YES;
}
#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText {
	/*
	 Update the filtered array based on the search text and scope.
	 */
	[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    if([searchText length] > 0) {
        
        for (SearchInfo *product in self.searchListItemArray)
        {
#define kOptions1 (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
            NSComparisonResult result = [product.file_name compare:searchText options:kOptions1 range:[product.file_name rangeOfString:searchText options:kOptions1]];
            
            if (result == NSOrderedSame)
            {
                [self.filteredListContent addObject:product];
            }
            else {
            }
        }
    }else {
        self.isSearchActive = NO;
        
    }
    [self.tableView reloadData];
}


@end
