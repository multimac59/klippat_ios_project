//
//  KPSignUpViewController.m
//  Klippat
//
//  Created by Ravi kumar on 10/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSignUpViewController.h"
#import "KPUserNameViewController.h"
@interface KPSignUpViewController ()
{
    BOOL isKeyBord;
}
@end

@implementation KPSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(handleKeyboardWillShow:)
     name:UIKeyboardWillShowNotification
     object:nil];
     
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(handleKeyboardWillHide:)
     name:UIKeyboardWillHideNotification
     object:nil];
    
     //[self.view setBackgroundColor:[UIColor colorWithRed:197/255.0 green:171/255.0 blue:135/255.0 alpha:1.0]];
     [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addTextFieldWithFrame:CGRectMake(25, 210*deviceFactor, self.view.frame.size.width-50, 33) placeholder:NSLocalizedString(@"User Name", nil) tag:88];
    [self addTextFieldWithFrame:CGRectMake(25, 255*deviceFactor, self.view.frame.size.width-50, 33) placeholder:NSLocalizedString(@"Email", nil) tag:89];
    [self addTextFieldWithFrame:CGRectMake(25, 300*deviceFactor, self.view.frame.size.width-50, 33) placeholder:NSLocalizedString(@"Password", nil) tag:90];
    [self addButtonWithFrame:CGRectMake(116, 367*deviceFactor, 170/2, 33) titel:NSLocalizedString(@"Next", nil) tag:91 ];
    [self addTermConditionLabelAndIcon];
    // Do any additional setup after loading the view.
}

- (void)handleKeyboardWillShow:(NSNotification *)notification {
    
    if (!isKeyBord) {
        return;
    }
    [UIView animateWithDuration:0.3f animations:^ {
        self.view.frame = CGRectMake(0.0, -80, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (void)handleKeyboardWillHide:(NSNotification *)notification {
    
    if (!isKeyBord) {
        return;
    }
    [UIView animateWithDuration:0.3f animations:^ {
        self.view.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

-(void)addTermConditionLabelAndIcon
{

    UIImage *icon=[UIImage imageNamed:(deviceFactor==1)?@"klp-logo":@"klp-logo_i"];
    UIImageView *iconImage=[[UIImageView alloc]initWithImage:icon];
    [iconImage setFrame:CGRectMake((deviceFactor==1)?100:109, (deviceFactor==1)?35:30, icon.size.width, icon.size.height)];
    [self.view addSubview:iconImage];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(45, 417*deviceFactor, 234, 23)];
    [label setNumberOfLines:2];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"      By creating an account,you agree to our      Terms of use and understand our Privacy Policy"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:2]
 range:[attributeString.string rangeOfString:@"Terms of use"]];
    [attributeString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:2]
                            range:[attributeString.string rangeOfString:@"Privacy Policy"]];

    [label setAttributedText:attributeString];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10]];
    UIButton *termBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [termBtn setFrame:CGRectMake(0, 12, 63, 15)];
    [termBtn setBackgroundColor:[UIColor clearColor]];
    [termBtn setTag:1555];
    [label addSubview:termBtn];
    [label setUserInteractionEnabled:YES];
    [termBtn addTarget:self action:@selector(termAndPolicyAction:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *policyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [policyBtn setFrame:CGRectMake(280-65-55, 12, 70, 15)];
    [policyBtn setBackgroundColor:[UIColor clearColor]];
    [policyBtn addTarget:self action:@selector(termAndPolicyAction:) forControlEvents:UIControlEventTouchUpInside];
    [policyBtn setTag:1556];

    [label addSubview:policyBtn];
    [self.view addSubview:label];
}
-(void)termAndPolicyAction:(UIButton*)sender
{
    if (sender.tag==1555) {
        NSLog(@"term");
    }
    else
    {
        NSLog(@"policy");
    }
    [self showTermAndConditionWebView];
}

-(void)showTermAndConditionWebView
{
    UIImage *crossImg=[UIImage imageNamed:@"cross_icon"];
    UIButton *crossBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [crossBtn setFrame:CGRectMake(self.view.frame.size.width-crossImg.size.width-5, 12, crossImg.size.width, crossImg.size.height)];
    [crossBtn setImage:crossImg forState:UIControlStateNormal];
    [crossBtn addTarget:self action:@selector(crossButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    UIWebView *webView=[[UIWebView alloc]initWithFrame:self.view.frame];
    [webView addSubview:crossBtn];
    [webView loadHTMLString:@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." baseURL:nil];
    [self.view addSubview:webView];
}

-(void)crossButtonAction:(UIButton*)sender
{
    [sender.superview removeFromSuperview];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addTextFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder  tag:(int)tag
{
    UITextField *textField=[[UITextField alloc]initWithFrame:frame];
    [textField setTag:tag];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setBackgroundColor:[UIColor colorWithRed:207/255.0 green:208/255.0 blue:212/255.0 alpha:1.0]];
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, frame.size.height)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    NSAttributedString *attPlaceholder=[[NSAttributedString alloc]initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:125/255.0 green:145/255.0 blue:190/255.0 alpha:1.0]}];
    [textField setAttributedPlaceholder:attPlaceholder];
    [textField setLeftView:leftView];
    [textField setFont:[UIFont systemFontOfSize:12]];
    if (tag==90)
    {
        [textField setSecureTextEntry:YES];
    }
    [textField setDelegate:(id)self];
    ;
    [self.view addSubview:textField];
}
-(void)addButtonWithFrame:(CGRect)frame titel:(NSString*)title  tag:(int)tag
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTag:tag];
    [button setBackgroundImage:[UIImage imageNamed:@"button"]forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button];
    
}
-(IBAction)buttonClickAction:(UIButton*)sender
{
    NSLog(@"Next  Clicked");
    UITextField *userName=(UITextField*)[self.view viewWithTag:88];
    UITextField *email=(UITextField*)[self.view viewWithTag:89];
    UITextField *password=(UITextField *)[self.view viewWithTag:90];
    if ([userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 ||[email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 ||[password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 ) {
        showAlertView(nil, @"Please fill all fields");
        return;
    }
    else if (![self validateEmailWithString:[email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]])
    {
        showAlertView(nil, @"Please provide valid email id.");
        return;
    }
    NSMutableDictionary *dataInfo=[NSMutableDictionary new];
    [dataInfo setValue:[email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
    [dataInfo setValue:@"user_signup" forKey:@"task"];
    [dataInfo setValue:[userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]forKey:@"user_name"];
    [dataInfo setValue:[password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"password"];
    KPParser *parser=[KPParser new];
    showActivity(self.navigationController.view);
    [parser serviceRequestWithInfo:dataInfo serviceType:KPSignUpServiceType responseBlock:^(id response, NSError *error) {
        NSLog(@"response sign up %@",response);
        NSDictionary *responseDict=(NSDictionary*)[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (!response)
            {
                showAlertView(nil, @"Network error.");
                NSLog(@"Failed to register.");
            }
           else if ([[responseDict valueForKey:@"success"]isEqualToString:@"true"]) {
                [KPSingletonClass sharedObject].userID =[[responseDict valueForKey:@"user_details"]valueForKey:@"user_id"];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"Registered successfully." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                alert.tag=34343;
                [alert show];
            }
        else if([[responseDict valueForKey:@"success"]isEqualToString:@"false"]){
            showAlertView(nil, [(NSString*)[responseDict valueForKey:@"error"] stringByAppendingString:@"."]);
        }
        else
        {
            showAlertView(nil, @"Failed to register.");
            NSLog(@"Failed to register.");
        }
        });
        NSLog(@"SignUp response%@",response);
        
        
    }]; 
  
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==34343) {
        KPAppDelegate *app=(KPAppDelegate *)[UIApplication sharedApplication].delegate;
        [self.navigationController pushViewController:app.tabListController animated:YES];
    }
}
-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow"];
    NSLog(@"siididididiif %@",NSStringFromCGSize(arrow.size));
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2-15, arrow.size.width , arrow.size.height );
    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    [self.view addSubview:viewNavigation];
}
-(IBAction)arrowAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    isKeyBord=YES;
}
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
