//
//  KPUserNameViewController.m
//  Klippat
//
//  Created by Ravi kumar on 10/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPUserNameViewController.h"
@interface KPUserNameViewController ()

@end

@implementation KPUserNameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:197/255.0 green:171/255.0 blue:135/255.0 alpha:1.0]];
    
    [self addTextFieldWithFrame:CGRectMake(20, 150, self.view.frame.size.width-40, 40) placeholder:NSLocalizedString(@"Password", nil) tag:50];
    [self addButtonWithFrame:CGRectMake(80, 210, self.view.frame.size.width-160, 40) titel:NSLocalizedString(@"Next", nil) tag:11 ];
 [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{ 
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addTextFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder  tag:(int)tag
{
    UITextField *textField=[[UITextField alloc]initWithFrame:frame];
    [textField setPlaceholder:placeholder];
    [textField setTag:tag];
    [textField setBackgroundColor:[UIColor whiteColor]];
    [textField setDelegate:(id)self];
    [self.view addSubview:textField];
}
-(void)addButtonWithFrame:(CGRect)frame titel:(NSString*)title  tag:(int)tag
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTag:tag];
    [button setBackgroundColor:[UIColor orangeColor]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:button];
    
}
-(IBAction)buttonClickAction:(id)sender
{
    NSLog(@"Next  Clicked");
//    KPCategoryListViewController *vc=[[KPCategoryListViewController alloc]init];
//    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"footer.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"airo_1.png"];
    UIImage *arrow1 = [UIImage imageNamed:@""];
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:arrow1 forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 20, arrow.size.width , arrow.size.height );
    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    
    [self.view addSubview:viewNavigation];
}
-(IBAction)arrowAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
