//
//  KPCategoryListViewController.m
//  Klippat
//
//  Created by Ravi kumar on 11/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPSlideImageNoteViewController.h"
#import "KPCategoryListViewController.h"
#import "KPSongListViewController.h"
@interface KPCategoryListViewController ()

@end

@implementation KPCategoryListViewController
@synthesize tableView=_tableView;
@synthesize listItemeArray=_listItemeArray;

-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]init];
    }
    return _tableView;
}
-(NSMutableArray*)listItemeArray
{
    if (!_listItemeArray) {
        _listItemeArray=[[NSMutableArray alloc]initWithObjects:@"Old Movies",@"80's Movies",@"90'Movies",@"New Movies",@"Latest upcoming Movies",@"Random picks", nil];
    }
    return _listItemeArray;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:197/255.0 green:171/255.0 blue:135/255.0 alpha:1.0]];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addInitialViewComponent];

    // Do any additional setup after loading the view.
}
-(void)addInitialViewComponent
{
    [self.tableView setFrame:CGRectMake(20, 105,self.view.frame.size.width-40 , 311)];
    [self.tableView setDelegate:(id)self];
    [self.tableView setDataSource:(id)self];
    [self.tableView setScrollEnabled:NO];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    UIImage *image=[UIImage imageNamed:(self.listType==KPMusicListType)?@"movieIcon":@"mp3Icon"];
    [self addImageViewWithFrame:CGRectMake(130, 20, 60, 65) image:image tag:777];
}

-(void)addImageViewWithFrame:(CGRect)frame image:(UIImage*)image tag:(int)tagValue
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    [imageView setImage:image];
    [imageView setTag:tagValue];
    [self.view addSubview:imageView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
}
-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"bt-bg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"bot-arw"];
    UIImage *heart=[UIImage imageNamed:@"fav_heart"];
    UIImage *home=[UIImage imageNamed:@"homeImage"];
    UIImage *music=[UIImage imageNamed:@"musicImage"];
    UIImage *share=[UIImage imageNamed:@"shareImage"];
    [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 10, arrow.size.width , arrow.size.height) tag:778 navigationBar:viewNavigation BGImage:arrow];
    [self addButtonOnNavigationBarFrame:CGRectMake(45,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:779 navigationBar:viewNavigation BGImage:heart];
     [self addButtonOnNavigationBarFrame:CGRectMake(95,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:780 navigationBar:viewNavigation BGImage:music];
     [self addButtonOnNavigationBarFrame:CGRectMake(145,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:781 navigationBar:viewNavigation BGImage:home];
     [self addButtonOnNavigationBarFrame:CGRectMake(195,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:782 navigationBar:viewNavigation BGImage:share];
    [self addButtonOnNavigationBarFrame:CGRectMake(245, viewNavigation.frame.size.height/2 - 15,arrow.size.width + 40, arrow.size.height + 10) tag:783 navigationBar:viewNavigation BGImage:[UIImage imageNamed:@""]];
//    UIButton *arrowButton = [[UIButton alloc] init];
//    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
//    [arrowButton setBackgroundImage:arrow1 forState:UIControlStateHighlighted];
//    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 15, arrow.size.width + 10, arrow.size.height + 10);
//    [arrowButton setTag:778];
//    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
//    [viewNavigation addSubview:arrowButton];185 X260
    
    [self.view addSubview:viewNavigation];
}
-(IBAction)arrowAction:(UIButton*)sender {
    if (sender.tag==778) {
        [self.navigationController popViewControllerAnimated:YES];

    }
    else if(sender.tag==782)
    {
        KPSlideImageNoteViewController *vc=[[KPSlideImageNoteViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        NSLog(@"Other then back arrow on navigation bar %ld",(long)sender.tag);
    }
    
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld row selected",(long)indexPath.row+1);
    
    KPSongListViewController *vc=[[KPSongListViewController alloc]init];
    vc.categoryTitleText=[self.listItemeArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listItemeArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *label;
    
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        label =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
        [label setTag:indexPath.row];
        [label.layer setBorderColor:[UIColor blackColor].CGColor];
        [label.layer setBorderWidth:1.5];
        [label setBackgroundColor:[UIColor grayColor]];
        [cell addSubview:label];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    else
        label =(UILabel*)[cell viewWithTag:indexPath.row];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16 ]];
    [label setTextColor:[UIColor redColor]];
    [label setText:[NSString stringWithFormat:@" %@",[self.listItemeArray objectAtIndex:indexPath.row]]];
    
    return cell;
}


@end
