//
//  KPCategoryListViewController.h
//  Klippat
//
//  Created by Ravi kumar on 11/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
typedef enum
{
    KPMovieListType=0,
    KPMusicListType=1,
    
}ItemListType;
#import <UIKit/UIKit.h>

@interface KPCategoryListViewController : KPCommonViewController
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *listItemeArray;
@property(nonatomic,assign)ItemListType listType;

@end
