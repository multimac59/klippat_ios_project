//
//  KPPlayListCell.m
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPPlayListCell.h"

@implementation KPPlayListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *container=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 280, (deviceFactor==1)?53:52)];
        [container setBackgroundColor:[UIColor grayColor]];
        [container.layer setBorderColor:[UIColor blackColor].CGColor];
        [container setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBG1"]]];
        
        self.thumbnail =[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 35, 40)];
        [container addSubview:self.thumbnail];
        
        self.titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(43, 8, 200, 30)];
        self.textLabel.textAlignment=NSTextAlignmentLeft;
        [container addSubview:self.titleLabel];
        
        [self addSubview:container];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
