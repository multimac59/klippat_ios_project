//
//  KPPlayListViewController.h
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

typedef enum
{
    KPNormalPlayListType=0,
    KPFavouriteType=1,
}PlayListType;

#import "KPCommonViewController.h"
#import "KPPlayListCell.h"
#import "KPHomeViewController.h"
#import "KPGridGalleryViewController.h"
#import "KPVideoViewController.h"
@interface KPPlayListViewController : KPCommonViewController
@property(nonatomic)PlayListType playListType;
@property(nonatomic,strong)NSString *file_id;
@property(nonatomic,strong)NSString *category_id;
@property(nonatomic,strong)NSMutableArray *playListItemArray;
@property(nonatomic,strong)UITableView *tableView;
@end
