//
//  KPPlayListViewController.m
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPSonngCategoryInfo.h"
#import "KPPlayListViewController.h"
#import "HHTabListController.h"

@interface KPPlayListViewController ()

@end

@implementation KPPlayListViewController
@synthesize playListItemArray=_playListItemArray;
@synthesize tableView=_tableView;
-(NSMutableArray*)playListItemArray
{
    if (!_playListItemArray) {
        _playListItemArray=[[NSMutableArray alloc]init];
    }
    
    return _playListItemArray;

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"dfjdlflas %d",self.playListType);
    if(self.playListType==KPNormalPlayListType)
    {
     [self requestForGetPlayListItemWithFlag:NO];
    }
    else
    {
    [self requestForFavouriteItemWithFlag:NO];
    }
    //[self requestForFavouriteFile];
    [self.navigationController setNavigationBarHidden:YES];
    [self addInitialViewComponent];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addInitialViewComponent
{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(23, (deviceFactor==1)?90:80, self.view.frame.size.width-50, (deviceFactor==1)?440:361)];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    if ([[UIDevice currentDevice] systemVersion].floatValue>=7) {
        [self.tableView setContentInset:UIEdgeInsetsMake(-20, 0, 0, 0)];
    }
    [self.tableView setShowsVerticalScrollIndicator:NO];
    
    [self.view addSubview:self.tableView];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addTopView];
    
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"+" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:35]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
   
}
-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow"];
    [self addButtonOnNavigationBarFrame:CGRectMake( 10, viewNavigation.frame.size.height/2-16 , arrow.size.width , arrow.size.height ) tag:778 navigationBar:viewNavigation BGImage:arrow];
        [self.view addSubview:viewNavigation];
}

-(IBAction)arrowAction:(UIButton*)sender
{
    if (sender.tag==778) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else if(sender.tag==783)
    {
        [self createNewPlaylistPopUpWithFrame:CGRectMake(30, 100, 260, 180)];
    }
    
    NSLog(@"Other then back arrow on navigation bar %ld",(long)sender.tag);
    
}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg.png"];
    UIImage *homeImage=[UIImage imageNamed:@"home"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(30, 8, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    if (self.playListType==KPNormalPlayListType)
    [titleLbl setText:@"PLAYLIST"];
    else
        [titleLbl setText:@"FAVOURITE"];

    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
    if (self.playListType==KPNormalPlayListType) {
    UIButton *customButton = [UIButton buttonWithType: UIButtonTypeContactAdd];
    [customButton setBackgroundColor: [UIColor clearColor]];
    [customButton setTitleColor:[UIColor blackColor] forState:
     UIControlStateHighlighted];
    
    [customButton addTarget:self action:@selector(addViewOnAddScreen:) forControlEvents:UIControlEventTouchUpInside];
    [customButton setFrame:CGRectMake(275, 8, 30, 30)];
    [customButton setTintColor:TextColor];
    [ topStripImgView addSubview:customButton];
    }
    [self.view addSubview:topStripImgView];
    
    
}
-(IBAction)addViewOnAddScreen:(id)sender
{
    [self createNewPlaylistPopUpWithFrame:CGRectMake(30, 100, 260, 180)];
}

-(IBAction)searchButtonAction:(id)sender
{
    NSLog(@"You Clicked on search button");
}
-(IBAction)homeButtonAction:(id)sender {
    
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    [self.navigationController popViewControllerAnimated:YES];
//    KPAppDelegate *app=(KPAppDelegate*)[UIApplication sharedApplication].delegate;
//    HHTabListController *vc;
//    NSLog(@"topViewController topViewController %@",self.navigationController.topViewController);
//    for(id controller in self.navigationController.viewControllers) {
//        NSLog(@"topViewController topViewController %@",controller);
//
//        if ([controller isKindOfClass:[HHTabListController class]]) {
//            vc=(HHTabListController*)controller;
//            [self.navigationController popToViewController:controller animated:YES];
//            break;
//        }
//    }
//    if (!vc) {
//        [self.navigationController pushViewController:app.tabListController animated:YES];
//        
//    }

}

-(void)createNewPlaylistPopUpWithFrame:(CGRect)frame
{
    NSLog(@"frame value is %@", NSStringFromCGRect(frame));
    UIView *popUpView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - frame.size.width/2-2, self.view.frame.size.height/2 - frame.size.height/2-41, frame.size.width, frame.size.height-37)];
    [popUpView setBackgroundColor:[UIColor colorWithRed:20.0/255 green:20.0/255 blue:22.0/255 alpha:0.95]];
    [popUpView setTag:3433];
    popUpView.layer.borderWidth = 2.f;
    popUpView.layer.borderColor = [UIColor colorWithRed:177.0/255 green:118.0/255 blue:117.0/255 alpha:1.0].CGColor;
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 12, frame.size.width+4, 18)];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont fontWithName:@"Helvetica-bold" size:15]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    
       UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(20, 45, 210, 30)];
    [textField setTag:3434];
    [textField setTextColor:[UIColor colorWithRed:51.0/255 green:68.0/255 blue:0.0/255 alpha:1.0]];
    textField.textAlignment=NSTextAlignmentCenter;
    NSString *placeholderStr=@"Enter Playlist Name";
    NSMutableAttributedString *attributedPlaceholder=[[NSMutableAttributedString alloc]initWithString:placeholderStr];
    [attributedPlaceholder addAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:51.0/255 green:68.0/255 blue:0.0/255 alpha:1.0]} range:NSMakeRange(0, placeholderStr.length) ];
    [textField setAttributedPlaceholder:attributedPlaceholder];
    
    
    textField.font=[UIFont fontWithName:@"Helvetica" size:12.0];
    [textField setDelegate:(id)self];
    [textField.layer setBorderColor:[UIColor blackColor].CGColor];
    textField.backgroundColor=[UIColor whiteColor];
    [textField.layer setBorderWidth:1.5];
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 6, textField.frame.size.height)];
    [textField setLeftView:leftView];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [popUpView addSubview:textField];
    [popUpView addSubview:titleLabel];
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(38, 90, 60, 24)];
    [button setTitle:@"Cancel" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor colorWithRed:51.0/255 green:68.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
    [button setTag:3435];
    button.layer.cornerRadius=5.5f;
    [button.layer setBorderWidth:1.5];
    [[button titleLabel] setFont:[UIFont systemFontOfSize:10.5]];
    [button.layer setBorderColor:[UIColor blackColor].CGColor];
    [button addTarget:self action:@selector(buttonOnPopUpAction:) forControlEvents:UIControlEventTouchDown];
    [popUpView addSubview:button];
    button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(150,90, 60, 24)];
    [button setTitle:@"Done" forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor colorWithRed:51.0/255 green:68.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
    [button setTag:3436];
    [button.layer setBorderWidth:1.5];
    button.layer.cornerRadius=5.5f;
    [[button titleLabel] setFont:[UIFont systemFontOfSize:10.5]];
    [button.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [button addTarget:self action:@selector(buttonOnPopUpAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [popUpView addSubview:button];
    [self.view addSubview:popUpView];
    [textField becomeFirstResponder];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    CGSize size =[textField.text sizeWithFont:[UIFont systemFontOfSize:20.0] constrainedToSize:CGSizeMake(60, 220) ];
//    NSLog(@"ssssssss %@",NSStringFromCGSize(size));

    return YES;
}

-(IBAction)buttonOnPopUpAction:(UIButton*)sender
{
    NSLog(@"add button activeted");
    UIView *popUp=(UIView*)[self.view viewWithTag:3433];
    if (sender.tag==3436)
    {
        
        UITextField  *textField=(UITextField*)[popUp viewWithTag:3434];
        if([textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0)
        {
            NSLog(@"textfield is empty");
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Enter playlist name." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
        }
        else
        {
            
            NSMutableDictionary *infoDict=[NSMutableDictionary new];
            [infoDict setValue:@"create_user_playlist" forKey:@"task"];
            [infoDict setValue:[[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"playlist_name"];
            [infoDict setValue:([KPSingletonClass sharedObject].userID)?[KPSingletonClass sharedObject].userID:@"29" forKey:@"user_id"];
            [self webSerivceToCreatePlaylistWithInfo:infoDict wihtFlag:NO];
        }
    }
    [popUp removeFromSuperview];
    
    
}

-(void)webSerivceToCreatePlaylistWithInfo:(NSMutableDictionary*)infoDict wihtFlag:(BOOL)flag
{
    KPParser *parser=[KPParser new];
    showActivity(self.navigationController.view);
    [parser serviceRequestWithInfo:infoDict serviceType:KPCreatePlayListType responseBlock:^(id response, NSError *error) {
        NSLog(@"create playlist %@",response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (!response) {
                NSLog(@"Errror %@creatae playlist ",[error localizedDescription]);
                if (!flag) {
                    [self webSerivceToCreatePlaylistWithInfo:infoDict wihtFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            }
            if ([[resDict valueForKey:@"success"]isEqualToString:@"true"]) {
                showAlertView(nil, @"Successfully created.");
                KPSonngCategoryInfo *info=[KPSonngCategoryInfo new];
                info.category_id=[[resDict valueForKey:@"user_playlist"]valueForKey:@"users_playlist_id"];
                info.file_name=[[resDict valueForKey:@"user_playlist"]valueForKey:@"playlist_name"];
                [self.playListItemArray addObject:info];
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.playListItemArray.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                [self.tableView endUpdates];
                
                
            }
        });
    }];

}
-(void)addSongInPlayListWithPlayListID:(NSString*)playListID
{
    __weak typeof(self) weakSelf = self;
    showActivity(weakSelf.navigationController.view);
    [self addFileToPlayListWith:self.file_id categoryId:self.category_id playlistId:playListID callBack:^(id result,NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(weakSelf.navigationController.view);
            if (result==nil)
            {
                showAlertView(nil, @"Network error.");
            }
            else
            {   NSDictionary *response =[[result valueForKey:@"response"]valueForKey:@"playlist"];
                //showAlertView(nil,[NSString stringWithFormat:@"%@.",[response valueForKey:@"message"]]);
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@.",[response valueForKey:@"message"]] delegate:weakSelf cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                alert.tag=3435;
                [alert show];
            }
        });
    }];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==3435) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (deviceFactor==1)?63:60.5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld row selected",(long)indexPath.row+1);
    KPSonngCategoryInfo *dataDict=[self.playListItemArray objectAtIndex:indexPath.row];

    NSLog(@"navigationcontrollers %@",self.navigationController.viewControllers);
    if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]isKindOfClass:[KPVideoViewController class]]) {
        [self addSongInPlayListWithPlayListID:dataDict.category_id];
        NSLog(@"addd in playlist clicked %@",dataDict);
        return;
    }
    KPSongListViewController *vc=[KPSongListViewController new];
    if (self.playListType==KPNormalPlayListType) {
        vc.categoryTitleText=[dataDict.file_name uppercaseString];
        vc.category_id=dataDict.category_id;
        vc.songListType=KPSongListPlaylist;
        [self.navigationController pushViewController:vc animated:YES];

    }
    else if([dataDict.file_name isEqualToString:@"movies"]||[dataDict.file_name isEqualToString:@"Music Clips"])
    {
        vc.categoryType=dataDict.type;
        vc.categoryTitleText=dataDict.file_name;
        vc.songListType=KPSongListFavourites;
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
    {
        KPGridGalleryViewController *grid=[KPGridGalleryViewController new];
        grid.category_id=dataDict.category_id;
        grid.categoryName=dataDict.file_name;
        [self.navigationController pushViewController:grid animated:YES];
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.playListItemArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPPlayListCell *cell=(KPPlayListCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[KPPlayListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    KPSonngCategoryInfo *categoryInfo=[self.playListItemArray objectAtIndex:indexPath.row];
    [cell.thumbnail setImage:[UIImage imageNamed:@"ic-img"]];
    
    [cell.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16 ]];
    [cell.titleLabel setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    NSLog(@"file name %@",categoryInfo.file_name);
    NSString *str1=[categoryInfo.file_name substringToIndex:1];
    NSLog(@"set %@",str1);
    NSString *result=[categoryInfo.file_name capitalizedString];
    NSLog(@"result %@",result);
    if ([result isEqualToString:@"Notes"]&&self.playListType==KPFavouriteType) {
        result=@"Quotes";
    }
    NSString *substr=[categoryInfo.file_name substringFromIndex:1];

    NSLog(@"subString %@:",substr);
    NSLog(@"converted result %@",[result stringByAppendingString:substr]);
    //[cell.titleLabel setText:[NSString stringWithFormat:@" %@",categoryInfo.file_name]];
    [cell.titleLabel setText:result];
    
    return cell;
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)requestForGetPlayListItemWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"user_playlist_show" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    NSLog(@"requestForGetPlayListItem :-- %@",infoDict);
    KPParser *parser=[KPParser new];
    
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetPlayListType responseBlock:^(id response, NSError *error) {
        NSLog(@"user_playlist_show %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                if (!flag) {
                    [self requestForGetPlayListItemWithFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            } else {
                    NSDictionary *resDict=[response valueForKey:@"response"];
                    id response = [resDict valueForKey:@"user_playlist"];
                    NSLog(@"%@",resDict);
                    if ([response isKindOfClass:[NSMutableDictionary class]]) {
                        NSMutableDictionary *infoDict = response;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"users_playlist_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        [songList setFile_name:[infoDict objectForKey:@"playlist_name"]];
                        [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];
                        [self.playListItemArray addObject:songList];
                    }else {
                        NSMutableArray *infoArray = response;
                        [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                            NSMutableDictionary *infoDict = obj;
                            KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                            [songList setCategory_id:[infoDict objectForKey:@"users_playlist_id"]];
                            [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                            [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                            [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                            [songList setFile_name:[infoDict objectForKey:@"playlist_name"]];
                            [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                            [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                            [songList setType:[infoDict objectForKey:@"type"]];
                            [self.playListItemArray addObject:songList];
                        }];
                    }
                    [self.tableView reloadData];
                NSLog(@"%@",resDict);
            }
        });

    }];
}
-(void)requestForFavouriteItemWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"return_user_favourite_type" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    NSLog(@" requestForFavouriteItem --%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetFavoriteType responseBlock:^(id response, NSError *error) {
        NSLog(@"favourites in 104573778969578767496 %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                if (!flag) {
                    [self requestForFavouriteItemWithFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            }
            else
            {
                NSDictionary *resDict=[response valueForKey:@"response"];
              
                if ([resDict isKindOfClass:[NSMutableDictionary class]]) {
                    NSLog(@"fav__1");
                    for (NSDictionary *infoDict in [resDict valueForKey:@"users_favourite_type"]) {
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[resDict objectForKey:@"users_playlist_id"]];
                        [songList setFavourite_status:[resDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[resDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict valueForKey:@"type"]];
                        if ([songList.file_id isEqualToString:@"clips"]) {
                            [songList setFile_name:@"Music Clips"];
                        }
                        else
                        [songList setFile_name:[infoDict valueForKey:@"type"]];
                        [songList setFile_url:[resDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[resDict objectForKey:@"person_name"]];
                        [songList setType:[resDict objectForKey:@"type"]];
                        [self.playListItemArray addObject:songList];
                        NSLog(@"fav@@@ %@",songList.file_name);

                    }
                }
                else
                {
                    NSMutableArray *infoArray = response;
                    NSLog(@"fav__2 %@",response);
                    [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *infoDict = obj;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"users_playlist_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        if ([songList.file_id isEqualToString:@"clips"]) {
                            [songList setFile_name:@"Music Clips"];
                        }
                        else
                        [songList setFile_name:[infoDict objectForKey:@"playlist_name"]];
                        [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];
                        [self.playListItemArray addObject:songList];
                         NSLog(@"fav@@@ %@",songList.file_name);
                    }];
                }
                [self.tableView reloadData];
                NSLog(@"Fav__3%@",resDict);
            }

        });
    }];
}

-(void)requestForFavouriteDetailWithType:(NSString*)type WithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"return_user_favourite_files" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [infoDict setValue:type forKey:@"type"];
    NSLog(@"hello fav detail%@",infoDict);
     KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetFavoriteDetailType responseBlock:^(id response, NSError *error) {
        
        NSLog(@"return_user_favourite_files %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if(response==nil)
            {
                if (!flag) {
                    [self requestForFavouriteDetailWithType:type WithFlag:YES];
                }
                else
                showAlertView(nil,@"Please check your internet connection");
            }
            else
            {
                NSDictionary *resDict=[response valueForKey:@"response"];
                id response = [resDict valueForKey:@"user_playlist"];
                NSLog(@"%@",resDict);
                if ([response isKindOfClass:[NSMutableDictionary class]]) {
                    NSMutableDictionary *infoDict = response;
                    KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                    [songList setCategory_id:[infoDict objectForKey:@"users_playlist_id"]];
                    [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                    [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                    [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                    [songList setFile_name:[infoDict objectForKey:@"playlist_name"]];
                    [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                    [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                    [songList setType:[infoDict objectForKey:@"type"]];
                    
                    [self.playListItemArray addObject:songList];
                }else {
                    NSMutableArray *infoArray = response;
                    [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *infoDict = obj;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"users_playlist_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        [songList setFile_name:[infoDict objectForKey:@"playlist_name"]];
                        [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];
                        
                        [self.playListItemArray addObject:songList];
                    }];
                }
                [self.tableView reloadData];
                NSLog(@"%@",resDict);
            }
            
        });
        
    }];
    
}

@end
