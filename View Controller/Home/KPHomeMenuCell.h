//
//  KPHomeMenuCell.h
//  Klippat
//
//  Created by R@v! Kumar on 28/02/15.
//  Copyright (c) 2015 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPHomeMenuCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLable;
@end
