//
//  KPHomeMenuCell.m
//  Klippat
//
//  Created by R@v! Kumar on 28/02/15.
//  Copyright (c) 2015 Komal kumar. All rights reserved.
//

#import "KPHomeMenuCell.h"

@implementation KPHomeMenuCell

- (void)awakeFromNib {
    // Initialization code
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.titleLable=[[UILabel alloc]init];
        [self addSubview:self.titleLable];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    

    // Configure the view for the selected state
}

@end
