//
//  KPHomeViewController.h
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPSearchViewController.h"
#import "KPCategoryViewController.h"
#import "KPHomeMenuCell.h"

@interface KPHomeViewController : KPCommonViewController
@property (nonatomic, strong)UIButton *myButton;
@property(nonatomic,strong)UIView *chooseLangView;
@property (nonatomic, strong) UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *sideManuItem;

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIPageControl * pageControl;
//@property(nonatomic, retain)NSMutableArray *myImageArray;
@property(nonatomic, retain)NSArray *myArray;
@property(nonatomic,strong)UILabel *titleLbl;


@end
