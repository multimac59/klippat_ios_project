//
//  KPGridGalleryCell.m
//  Klippat
//
//  Created by Zahid on 17/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPImageCategoryCell.h"

@implementation KPImageCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *containerView=[[UIView alloc]initWithFrame:CGRectMake(25, 0, 270, 55)];
        [containerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBG1"]]];
        self.titleLable =[[UILabel alloc]initWithFrame:CGRectMake(20, 11, 200, 30)];
        //self.titleLable =[[UILabel alloc]init];
        [self.titleLable setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [self.titleLable setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
       
        
        [containerView addSubview:self.titleLable];
        [self addSubview:containerView];

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
