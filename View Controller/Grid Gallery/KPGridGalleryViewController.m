//
//  KPGridGalleryViewController.m
//  Klippat
//
//  Created by Ravi kumar on 11/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSonngCategoryInfo.h"
#import "KPGridGalleryViewController.h"
#import "HHTabListController.h"
#import "KPLocalViewController.h"
#import "KPHomeViewController.h"
#import "KPPlayListViewController.h"
@interface KPGridGalleryViewController ()
{
    BOOL isSearchActive,isDownloads;
}
@property(nonatomic,strong)NSMutableArray *filteredArray;
@end

@implementation KPGridGalleryViewController
@synthesize imageArray=_imageArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(NSMutableArray *)filteredArray
{
    if (!_filteredArray) {
        _filteredArray=[NSMutableArray new];
    }
    return _filteredArray;
}
-(NSMutableArray*)imageArray
{
    if (!_imageArray)
    {
        _imageArray=[[NSMutableArray alloc]init];
    }
    return _imageArray;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *viewControllersArray=self.navigationController.viewControllers;
    [self addTopView];
    [self addBottomView];

    if ([[viewControllersArray objectAtIndex:viewControllersArray.count-2]isKindOfClass:[KPPlayListViewController class]]) {
        [self requestForFavouriteDetailWithFlag:NO];
    }
    else if([[viewControllersArray objectAtIndex:viewControllersArray.count-2]isKindOfClass:[KPLocalViewController class]])
    {
        NSLog(@"navigation from downoad screen,Thanks with data %@",self.imageArray);
        isDownloads=YES;
         [self addGridViewWithImages];
    }
    else
    [self getCategoryDetailServiceWithFlag:NO];
}

-(void)addBottomView
{
    UIImageView * bottomImage = [[UIImageView alloc]init];
    [bottomImage setFrame:CGRectMake(0.0, self.view.frame.size.height-120, 320.0, 120.0)];
    [bottomImage setImage:[UIImage imageNamed:@"blur-footer"]];
    [self.view addSubview:bottomImage];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(klipsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-80, 60.0, 60.0)];
    [button setImage:[UIImage imageNamed:@"klip"] forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(IBAction)klipsButtonAction:(id)sender
{
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
    //[[self getPlayerView] removeFromSuperview];
    KPHomeViewController *vc;
    for(id controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[KPHomeViewController class]])
        {
            vc=(KPHomeViewController*)controller;
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    if (!vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark Content Filtering
- (void)filterContentForSearchText:(NSString*)searchText {
	/*
	 Update the filtered array based on the search text and scope.
	 */
	//[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    [self.filteredArray removeAllObjects];
    if([searchText length] > 0) {
        for (KPSonngCategoryInfo *tempDict in self.imageArray)
        {
            NSString *songName = tempDict.file_name;
            NSString *singerName=tempDict.person_name;
#define kOptions1 (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
            NSComparisonResult result = [songName compare:searchText options:kOptions1 range:[songName rangeOfString:searchText options:kOptions1]];
            NSComparisonResult result2 = [singerName compare:searchText options:kOptions1 range:[singerName rangeOfString:searchText options:kOptions1]];
            if (result == NSOrderedSame ||result2==NSOrderedSame)
            {
                [self.filteredArray addObject:tempDict];
            }
            
        }
    }
    
    [self addGridViewWithImages];
    
}

-(void)getCategoryDetailServiceWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"show_category_details" forKeyPath:@"task"];
    [infoDict setValue:self.category_id forKeyPath:@"category_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [self.imageArray removeAllObjects];
    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPGetCategoryDetailType responseBlock:^(id response, NSError *error) {
        NSLog(@"category detail %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (error) {
                if (!flag)
                {
                    [self getCategoryDetailServiceWithFlag:YES];
                }
                else
                showAlertView(nil, @"Please check your internet connection");
            }
            else
            {
                NSDictionary *resDict=[response valueForKey:@"response"];
                id response = [resDict valueForKey:@"all_list"];
                if ([response isKindOfClass:[NSMutableDictionary class]]) {
                    NSMutableDictionary *infoDict = response;
                    KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                    [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                    [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                    [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                    [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                    [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                    NSString *fileURL=[infoDict objectForKey:@"file_url"];
                    [songList setFile_url:[fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
                    [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                    [songList setType:[infoDict objectForKey:@"type"]];
                    songList.serverURL =songList.file_url;
                    [self.imageArray addObject:songList];
                }else {
                    NSMutableArray *infoArray = response;
                    [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *infoDict = obj;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                        NSString *fileURL=[infoDict objectForKey:@"file_url"];
                        [songList setFile_url:[fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];
                        songList.serverURL =songList.file_url;

                        [self.imageArray addObject:songList];
                    }];
                }
                [self addGridViewWithImages];

            }
        });
        
    }];
}
-(void)requestForFavouriteDetailWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"return_user_favourite_files" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [infoDict setValue:self.categoryName forKey:@"type"];
    [infoDict setValue:[KPSingletonClass sharedObject].language forKey:@"language_type"];
    NSLog(@"hello fav detail%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetFavoriteDetailType responseBlock:^(id response, NSError *error) {
        
        NSLog(@"return_user_favourite_files %@",response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if(response==nil)
            {
                if (!flag) {
                    [self requestForFavouriteDetailWithFlag:YES];
                }
                else
                showAlertView(nil,@"Network error.");
            }
            else if([[resDict valueForKey:@"success"]isEqualToString:@"true"])
            {
                NSLog(@"%@",resDict);
               // self.imageArray =[resDict valueForKey:@"users_favourite_files"];
                for (NSDictionary *infoDict in [resDict valueForKey:@"users_favourite_files"]) {
                    KPSonngCategoryInfo *info=[KPSonngCategoryInfo new];
                    info.file_name=[infoDict valueForKey:@"file_name"];
                    info.category_id=[infoDict valueForKey:@"category_id"];
                    NSString *fileURL=[infoDict valueForKey:@"file_url"];
                    info.file_url=[fileURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    info.person_name=[infoDict valueForKey:@"person_name"];
                    info.type=[infoDict valueForKey:@"type"];
                    info.file_duration=[infoDict valueForKey:@"file_duration"];
                    info.file_id=[infoDict valueForKey:@"file_id"];
                    info.serverURL =info.file_url;
                    info.favourite_status=[infoDict valueForKey:@"favourite_status"];
                    [self.imageArray addObject:info];
                   }
                NSLog(@"users_favourite_files %@",self.imageArray);
                [self addGridViewWithImages];

            }
            
        });
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)addLocalNavigationBar:(CGRect)frame
{
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    UIImage *arrow = [UIImage imageNamed:@"arow"];
    UIImage *arrow1 = [UIImage imageNamed:@""];
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:arrow1 forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2-16, arrow.size.width , arrow.size.height );
    [arrowButton addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    [self.view addSubview:viewNavigation];
}

-(IBAction)homeButtonAction:(id)sender
{
        [[KPSingletonClass sharedObject] setViaDownloads:@""];
        [[KPSingletonClass sharedObject] setViaFavourites:@""];
        [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
        HHTabListController *vc;
        for(id controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[HHTabListController class]]) {
                vc=(HHTabListController*)controller;
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        if (!vc) {
            [self.navigationController pushViewController:vc animated:YES];
            
        }
}

-(void)addCategoryTitleWithNavigationButton
{
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, 73, 120,25)];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setBackgroundColor:[UIColor colorWithRed:7/255.0 green:12/255.0 blue:19/255.0 alpha:1.0]];
    [titleLbl setTextColor:[UIColor colorWithRed:165/255.0 green:166/255.0 blue:211/255.0 alpha:1.0]];
    [titleLbl setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLbl setTag:8349];
    [titleLbl setText:[self.categoryName uppercaseString]];
    
    UIImage *left_icon=[UIImage imageNamed:@"arrow-left"];
    UIImage*right_icon=[UIImage imageNamed:@"arrow-right"];
    UIButton *left_naviBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [left_naviBtn setFrame:CGRectMake(titleLbl.frame.origin.x-left_icon.size.width-10, 75, left_icon.size.width, left_icon.size.height)];
    [left_naviBtn setImage:left_icon forState:UIControlStateNormal];
    [left_naviBtn addTarget:self action:@selector(leftArrowNavAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:left_naviBtn];
    
    UIButton *right_naviBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [right_naviBtn setFrame:CGRectMake(titleLbl.frame.origin.x+titleLbl.frame.size.width+10, 75, right_icon.size.width, right_icon.size.height)];
    [right_naviBtn setImage:right_icon forState:UIControlStateNormal];
    [right_naviBtn addTarget:self action:@selector(rightArrowNavAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right_naviBtn];
    
    [self.view addSubview:titleLbl];
}

-(void)leftArrowNavAction:(UIButton*)sender
{
    if (--self.selectedIndex>=0) {
        NSDictionary *tempDict=[self.allCategoryArray objectAtIndex:self.selectedIndex];
        NSLog(@"temppppp Data %@",tempDict);
        self.category_id=[tempDict valueForKey:@"category_id"];
        UILabel *titleLbl=(UILabel*)[self.view viewWithTag:8349];
        [titleLbl setText:[[tempDict valueForKey:@"category_name"] uppercaseString]];

        [self getCategoryDetailServiceWithFlag:NO];
    }
    NSLog(@"leftArrowNavAction");
}

-(void)rightArrowNavAction:(UIButton*)sender
{
    if (++self.selectedIndex<self.allCategoryArray.count) {
        NSDictionary *tempDict=[self.allCategoryArray objectAtIndex:self.selectedIndex];
        NSLog(@"temppppp Right Data %@",tempDict);
        self.category_id=[tempDict valueForKey:@"category_id"];
        UILabel *titleLbl=(UILabel*)[self.view viewWithTag:8349];
        [titleLbl setText:[[tempDict valueForKey:@"category_name"] uppercaseString]];
        [self getCategoryDetailServiceWithFlag:NO];
    }
    NSLog(@"rightArrowNavAction");
  
}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"back_blue"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 41)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 11, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(53, 13, 230, 20)];
    //[titleLbl setText:[NSString stringWithFormat:@"%@",[([[self.categoryName lowercaseString]isEqualToString:@"notes"]?@"QUOTES":self.categoryName) uppercaseString]]] ;
    
    [titleLbl setText:[NSString stringWithFormat:@"%@",[(self.gridViewType==KPGrideViewImagesType)?@"pictures":@"quotes" uppercaseString]]];
    
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:14.0/255 green:26.0/255 blue:93.0/255 alpha:1.0]];
    [titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [topStripImgView addSubview:titleLbl];
    
    UIImage * searchImage = [UIImage imageNamed:@"search_1"];
    UIButton * searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton addTarget:self action:@selector(searchButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [searchButton setBackgroundImage:searchImage forState:UIControlStateNormal];
    [searchButton setFrame:CGRectMake(280.0, 13.0, searchImage.size.width, searchImage.size.height)];
    [topStripImgView addSubview:searchButton];
    [self addCategoryTitleWithNavigationButton];
    [self.view addSubview:topStripImgView];
}

-(IBAction)searchButton:(UIButton*)sender
{
    NSLog(@"Search Icon");
    sender.selected=!sender.selected;
    UIScrollView *scrollView=(UIScrollView*)[self.view viewWithTag:8938];
    if (sender.selected)
    {
        [self addCustomSearchBar];
        [scrollView setFrame:CGRectMake(9, (deviceFactor==1)?137:137, 310, (deviceFactor==1)?376:311)];
    }
    else
    {
        UITextField *searchTF=(UITextField*)[self.view viewWithTag:8934];
        [searchTF removeFromSuperview];
        [scrollView setFrame:CGRectMake(9, (deviceFactor==1)?102:108, 310, (deviceFactor==1)?411:346)];//9, (deviceFactor==1)?102:124, 310, (deviceFactor==1)?411:346
    }
//    NSLog(@"Search Icon");
//    sender.selected=!sender.selected;
//    int factor=0;
//    if (self.songListType!=KPNormalPlayListType) {
//        factor=30;
//    }
//    if (sender.selected)
//    {
//        [self addCustomSearchBar];
//        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?(137-factor):(137-factor), self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?(137-factor):(137-factor)))];
//    }
//    else
//    {
//        UITextField *searchTF=(UITextField*)[self.view viewWithTag:8934];
//        [searchTF removeFromSuperview];
//        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?(102-factor):(102-factor), self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?(102-factor):(102-factor)))];
//    }
}

-(void)addCustomSearchBar
{
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:IS_IPHONE_5?CGRectMake(9, 102,self.view.frame.size.width-18, 30):CGRectMake(9, 102,self.view.frame.size.width-18, 30)];
    [search_txtF setDelegate:(id)self];
    UIImage *search=[UIImage imageNamed:@"search"];
    [search_txtF setTag:8934];
    UIColor *color =[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color,NSFontAttributeName:[UIFont italicSystemFontOfSize:12.0]}];
    [search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
    search_txtF.textColor=[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 25)];
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 6, search.size.width, search.size.height)];
    [searchIcon setImage:[UIImage imageNamed:@"search"]];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:search_txtF];
}
-(void)addNoDataFoundLabel
{
    UILabel *label=(UILabel*)[self.view viewWithTag:8493];
    if (label) {
        [label removeFromSuperview];
        label=nil;
    }
    label=[[UILabel alloc]initWithFrame:CGRectMake(20, 160, 280, 20)];
    [label setText:@"No Data Found!"];
    [label setTag:8493];
    [label setTextColor:[UIColor blueColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [self.view addSubview:label];
}
-(void)addGridViewWithImages
{
    UIScrollView *scrollView=(UIScrollView*)[self.view viewWithTag:8938];
    if (scrollView) {
        [scrollView removeFromSuperview];
        scrollView=nil;
    }
    if (isSearchActive && self.filteredArray.count==0  )
    {
        [self addNoDataFoundLabel];
        return;
    }
    else
    {
        UILabel *label=(UILabel*)[self.view viewWithTag:8493];
        if (label) {
            [label removeFromSuperview];
            label=nil;
        }
    }
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(9, (deviceFactor==1)?102:102, 310, (deviceFactor==1)?411:346)];
    [scrollView setBackgroundColor:[UIColor clearColor]];
    [scrollView setTag:8938];
    [self.view addSubview:scrollView];
    int dx=-1,dy=0;
    for (int i=0;i<((!isSearchActive)?self.imageArray.count:self.filteredArray.count);i++)
    {
        KPSonngCategoryInfo *tempDict=[(!isSearchActive)?self.imageArray:self.filteredArray objectAtIndex:i];
        NSLog(@"Image No. %d",i);
        if (i%4==0 && i!=0)
        {
            dx=0;
            dy++;

        }
        else
        {
            dx++;
        }
        UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(77*dx+0.5, 0+72*dy, 70, 61)];
        UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(1.5, 0, 67, 60)];
       [imageView setTag:1717+i];
        UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureAction:)];
        [imageView setUserInteractionEnabled:YES];
        [imageView addGestureRecognizer:tapGesture];
        if (!isDownloads) {
            [imageView setImageWithURL:[NSURL URLWithString:tempDict.file_url] placeholderImage:[UIImage imageNamed:@"placeholder_img"]];
        }
        else
        {
         UIImage *img = [UIImage imageWithContentsOfFile:tempDict.file_url];
        [imageView setImage:img];
        }
        [tempView addSubview:imageView];
        [tempView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"thumnail_grid_bg"]]];
        [scrollView addSubview:tempView];
    }
    [scrollView setContentSize:CGSizeMake(293, ((dy+1)*69)-24)];
}
-(IBAction)tapGestureAction:(UIGestureRecognizer*)gestureRecognizer
{
    NSLog(@"tapGestureAction Tapped Image No. %d",gestureRecognizer.view.tag-1716);
    KPImagePlayerViewController *obj=[KPImagePlayerViewController new];
    obj.imageArray=self.imageArray;
    if (self.gridViewType==KPGrideViewQuotesType)
    {
        obj.viewerType=KPViewerQoutesType;
    }else
        obj.viewerType=KpViewerImageType;
    obj.selectedIndex=gestureRecognizer.view.tag-1717;
    [self.navigationController pushViewController:obj animated:YES];
}

-(IBAction)backButtonAction :(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (text.length>0) {
        isSearchActive=YES;
        [self filterContentForSearchText:text];
    }
    else
        
    {
        isSearchActive=NO;
        [self addGridViewWithImages];
    }
    
    return YES;
}
@end
