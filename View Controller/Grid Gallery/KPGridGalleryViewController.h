//
//  KPImageCategoriesController.h
//  Klippat
//
//  Created by Ravi kumar on 11/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"
#import "KPImagePlayerViewController.h"
#import "KPCategoryCell.h"
typedef enum
{
    KPGrideViewQuotesType=2,
    KPGrideViewImagesType=3,
}KPGridViewType;
@interface KPGridGalleryViewController : KPCommonViewController
@property(nonatomic,strong)NSMutableArray *imageArray;
@property(nonatomic)KPGridViewType gridViewType;
@property(nonatomic,strong)NSString *categoryName;
@property(nonatomic,strong)NSString *category_id;
@property(nonatomic)int selectedIndex;
@property(nonatomic,strong)NSMutableArray *allCategoryArray;

@end
