//
//  KPSongListCell.m
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSongListCell.h"

@implementation KPSongListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *containerView=[[UIView alloc]initWithFrame:CGRectMake(25, 0, 270, 55)];
        //[containerView.layer setBorderColor:[UIColor blackColor].CGColor];
        //[containerView.layer setBorderWidth:1.5];
        [containerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBG1"]]];
        self.thumbnailImage=[[UIImageView alloc]init];
        [containerView addSubview:self.thumbnailImage];
        
        self.favouriteBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *imageRed=[UIImage imageNamed:@"red-heart"];
        [self.favouriteBtn setImage:[UIImage imageNamed:@"blk-heart"] forState:UIControlStateNormal];
        [self.favouriteBtn setImage:imageRed forState:UIControlStateSelected];
        [self.favouriteBtn setFrame:CGRectMake(205, 14, imageRed.size.width, imageRed.size.height)];
        [containerView addSubview:self.favouriteBtn];
        
        
//        self.playBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        UIImage * imagePlay = [UIImage imageNamed:@"playBtn"];
//        [self.playBtn setImage:imagePlay forState:UIControlStateNormal];
//        //[self.playBtn setFrame:CGRectMake(242, 14, imagePlay.size.width, imagePlay.size.height)];
//        [self.playBtn setFrame:CGRectMake(242, 16, 16, 16)];
//        [containerView addSubview:self.playBtn];
        
        self.playBtn = [[UIImageView alloc]init];
        [self.playBtn setFrame:CGRectMake(242.0, 16.0, 16.0, 16.0)];
        [self.playBtn setImage:[UIImage imageNamed:@"playBtn"]];
        [containerView addSubview:self.playBtn];
        
        
        
        
        self.songTitle =[[UILabel alloc]init];
        [self.songTitle setFont:[UIFont fontWithName:@"Helvetica" size:13]];
        [self.songTitle setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
        [containerView addSubview:self.songTitle];
        
        self.singerName=[[UILabel alloc]init];
        [self.singerName setFont:[UIFont fontWithName:@"Helvetica" size:12]];
        [self.singerName setTextColor:[UIColor colorWithRed:37.0/255 green:0.0/255 blue:81.0/255 alpha:1.0]];
        [containerView addSubview:self.singerName];
        [self addSubview:containerView];
        self.timeLbl=[[UILabel alloc]initWithFrame:CGRectMake(203,39, 50, 10)];
        [self.timeLbl setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
        [self.timeLbl setTextColor:[UIColor colorWithRed:37.0/255 green:0.0/255 blue:81.0/255 alpha:1.0]];
        [containerView addSubview:self.timeLbl];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
