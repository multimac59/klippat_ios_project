//
//  KPSongListViewController.h
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KPSonngCategoryInfo.h"
#import "KPImagePlayerViewController.h"
typedef enum
{
    KPSongListNormal,
    KPSongListFavourites,
    KPSongListPlaylist,
    KPSongListDownloads,
    
}SongListType;

@interface KPSongListViewController : KPCommonViewController
@property (nonatomic,strong)NSString *categoryTitleText;
@property (nonatomic,strong)NSString *rootCategoryTitle;

@property(nonatomic)SongListType songListType;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSString *category_id;
@property(nonatomic)int selectedIndex;
@property(nonatomic,strong)NSMutableArray *songListArray;
@property(nonatomic,strong)NSMutableArray *allCategoryArray;

@property(nonatomic,strong)NSString *categoryType;
@property(nonatomic)DownLoadCategoryType downLoadcategoryType;

@end
