//
//  KPSongListCell.h
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPSongListCell : UITableViewCell
@property(nonatomic,strong)UIImageView *thumbnailImage;
@property(nonatomic,strong)UILabel *songTitle;
@property(nonatomic,strong)UILabel *singerName;
@property(nonatomic,strong)UILabel *timeLbl;
@property(nonatomic,strong)UIImageView *heatImgView;
@property(nonatomic,strong)UIButton *favouriteBtn;
//@property(nonatomic,strong)UIButton *playBtn;
@property(nonatomic,strong)UIImageView *playBtn;

@end
