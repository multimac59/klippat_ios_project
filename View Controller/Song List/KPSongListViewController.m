//
//  KPSongListViewController.m
//  Klippat
//
//  Created by Ravi kumar on 12/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "DownloadInfo.h"
#import "KPDownLoadInfo.h"
#import "KPCoreDataInitializer.h"
#import "KPSongListViewController.h"
#import "KPSongListCell.h"
#import "KPPlayListViewController.h"
#import "KPVideoViewController.h"
#import "KPHomeViewController.h"
#import "KPCategoryCell.h"
@interface KPSongListViewController ()
@property(nonatomic)BOOL isSearchActive;
@property(nonatomic,strong)NSMutableArray *filteredArray;
@end

@implementation KPSongListViewController
@synthesize tableView=_tableView;
@synthesize songListArray=_songListArray;
-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    return _tableView;
}
-(NSMutableArray*)songListArray
{
    if (!_songListArray) {
        _songListArray=[[NSMutableArray alloc]init];
    }
    return _songListArray;
}
-(NSMutableArray *)filteredArray
{
    if (!_filteredArray) {
        _filteredArray=[NSMutableArray new];
    }
    return _filteredArray;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     //[self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 49, 320, 49)];
    [self adInitialViewComponent];
//    if (self.songListType==KPSongListFavourites) {
//        [self requestForFavouriteDetail];
//    } else if(self.songListType==KPSongListNormal) {
//        [self getCategoryDetailService];
//    } else if (self.songListType==KPSongListPlaylist) {
//        [self requestForUserPlayListFile];
//    }else if (self.songListType==KPSongListDownloads){
//        NSMutableArray *dataArrary  = [[self getCoreDataInitializer] fetchDataAccordingToFilter:self.downLoadcategoryType];
//        [self.songListArray removeAllObjects];
//        [dataArrary enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//            DownloadInfo *downLoad = obj;
//            KPDownLoadInfo *downlOadInfoLocal = [[KPDownLoadInfo alloc] init];
//            [downlOadInfoLocal setFile_url:downLoad.localPath];
//            [downlOadInfoLocal setFile_name:downLoad.file_name];
//            [downlOadInfoLocal setFile_duration:downLoad.file_duration];
//            [downlOadInfoLocal setFile_id:downLoad.file_id];
//            [downlOadInfoLocal setCategory_id:downLoad.category_id];
//            [downlOadInfoLocal setPerson_name:downLoad.person_name];
//            [self.songListArray addObject:downlOadInfoLocal];
//
//        }];
//    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.songListType==KPSongListFavourites) {
        [self requestForFavouriteDetail];
    } else if(self.songListType==KPSongListNormal) {
        [self getCategoryDetailServiceWithFlag:YES];
    } else if (self.songListType==KPSongListPlaylist) {
        [self requestForUserPlayListFile];
    }else if (self.songListType==KPSongListDownloads){
        NSMutableArray *dataArrary  = [[self getCoreDataInitializer] fetchDataAccordingToFilter:self.downLoadcategoryType];
        [self.songListArray removeAllObjects];
        [dataArrary enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            DownloadInfo *downLoad = obj;
            KPDownLoadInfo *downlOadInfoLocal = [[KPDownLoadInfo alloc] init];
            [downlOadInfoLocal setFile_url:downLoad.localPath];
            [downlOadInfoLocal setFile_name:downLoad.file_name];
            [downlOadInfoLocal setFile_duration:downLoad.file_duration];
            [downlOadInfoLocal setFile_id:downLoad.file_id];
            [downlOadInfoLocal setServerURL:downLoad.file_url];
            [downlOadInfoLocal setCategory_id:downLoad.category_id];
            [downlOadInfoLocal setPerson_name:downLoad.person_name];
            [downlOadInfoLocal setType:downLoad.type];
            [self.songListArray addObject:downlOadInfoLocal];
            
        }];
    }
}
-(KPCoreDataInitializer *)getCoreDataInitializer
{
    
    KPCoreDataInitializer *dataInitilizer = [KPCoreDataInitializer sharedMySingleton];
    [dataInitilizer initializeTheCoreDataModelClasses];
    return dataInitilizer;
}
-(void)requestForFavouriteDetail
{
    showActivity(self.navigationController.view);
    [self.songListArray removeAllObjects];

    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"return_user_favourite_files" forKey:@"task"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [infoDict setValue:([self.categoryTitleText isEqualToString:@"Music Clips" ]?@"clips":self.categoryTitleText) forKey:@"type"];
    [infoDict setValue:[KPSingletonClass sharedObject].language forKey:@"language_type"];
    [self.songListArray removeAllObjects];
    NSLog(@"hello fav detail%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetFavoriteDetailType responseBlock:^(id response, NSError *error) {
        
        NSLog(@"return_user_favourite_files %@",response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if(response==nil)
            {
                showAlertView(nil,@"Network error.");
                [self.tableView reloadData];
            }
            else if([[resDict valueForKey:@"success"]isEqualToString:@"true"])
            {
                NSLog(@"users_favourite_files%@",resDict);
                for (NSDictionary *dict in [resDict valueForKey:@"users_favourite_files"]) {
                    KPSonngCategoryInfo *info=[KPSonngCategoryInfo new];
                    info.favourite_status=[dict valueForKey:@"favourite_status"];
                    info.file_duration=[dict valueForKey:@"file_duration"];
                    info.person_name=[dict valueForKey:@"person_name"];
                    info.file_url=[dict valueForKey:@"file_url"];
                    info.file_name=[[dict valueForKey:@"file_name"]capitalizedString];
                    info.category_id=[dict valueForKey:@"category_id"];
                    info.type=[dict valueForKey:@"type"];
                    info.file_id=[dict valueForKey:@"file_id"];
                    [self.songListArray addObject:info];
                }
               // self.songListArray =[resDict valueForKey:@"users_favourite_files"];
                [self.tableView reloadData];
            }
            
        });
        
    }];
    
}



-(void)addBottomBarWithHeadfoneIcon
{
    UIImage *bottomBG=[UIImage imageNamed:@"blur-footer"];
    UIImageView *bottomImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-bottomBG.size.height ,self.view.frame.size.width , bottomBG.size.height)];
    [bottomImage setImage:bottomBG];
    UIButton *headfoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *headFone=[UIImage imageNamed:@"klipatt_headPhone"];
    [headfoneBtn setFrame:CGRectMake(self.view.frame.size.width/2-headFone.size.width/2, bottomBG.size.height/2-headFone.size.height/2+24, headFone.size.width, headFone.size.height)];
    [headfoneBtn setImage:headFone forState:UIControlStateNormal];
    [headfoneBtn addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomImage addSubview:headfoneBtn];
    [self.view bringSubviewToFront:bottomImage];
    [self.view addSubview:bottomImage];
}
-(void)requestForUserPlayListFile
{
    showActivity(self.navigationController.view);
    [self.songListArray removeAllObjects];

    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"return_users_playlist_file" forKey:@"task"];
    [infoDict setValue:self.category_id forKey:@"user_playlist_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].language forKey:@"language_type"];

    
    NSLog(@"%@",infoDict);
    KPParser *parser=[KPParser new];
    [parser serviceRequestWithInfo:infoDict serviceType:KPGetUserPlaylistFile responseBlock:^(id response, NSError *error) {
        
        NSLog(@"play list files detail %@", response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil)
            {
                showAlertView(nil, @"Network error.");
                [self.tableView reloadData];
            }
            else if([[resDict valueForKey:@"success"]isEqualToString:@"true"])
            {
                NSDictionary *resDict=[response valueForKey:@"response"];
                id response = [resDict valueForKey:@"users_playlist_files"];
                NSLog(@"%@",resDict);
                if ([response isKindOfClass:[NSMutableDictionary class]]) {
                    NSMutableDictionary *infoDict = response;
                    KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                    [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                    [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                    [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                    [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                    [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                    [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                    [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                    [songList setType:[infoDict objectForKey:@"type"]];

                    [self.songListArray addObject:songList];
                }else {
                    NSMutableArray *infoArray = response;
                    [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *infoDict = obj;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                        [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];

                        [self.songListArray addObject:songList];
                    }];
                }
                [self.tableView reloadData];
            }
        });
        
    }];
}
-(void)getCategoryDetailServiceWithFlag:(BOOL)flag
{
    showActivity(self.navigationController.view);
    [self.songListArray removeAllObjects];

    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"show_category_details" forKeyPath:@"task"];
    [infoDict setValue:self.category_id forKeyPath:@"category_id"];
    [infoDict setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPGetCategoryDetailType responseBlock:^(id response, NSError *error) {
        NSLog(@"category detail %@",response);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (!response) {
                if (flag) {
                    [self getCategoryDetailServiceWithFlag:NO];
                }
                else
                {
                showAlertView(nil, @"Network error.");
                [self.tableView reloadData];
                }
            }
            else
            {
            NSDictionary *resDict=[response valueForKey:@"response"];
                id response = [resDict valueForKey:@"all_list"];
                if ([response isKindOfClass:[NSMutableDictionary class]]) {
                    NSMutableDictionary *infoDict = response;
                    KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                    [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                    [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                    [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                    [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                    [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                    [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                    [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                    [songList setType:[infoDict objectForKey:@"type"]];
                    [self.songListArray addObject:songList];
                }else {
                    NSMutableArray *infoArray = response;
                    [infoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSMutableDictionary *infoDict = obj;
                        KPSonngCategoryInfo *songList = [[KPSonngCategoryInfo alloc] init];
                        [songList setCategory_id:[infoDict objectForKey:@"category_id"]];
                        [songList setFavourite_status:[infoDict objectForKey:@"favourite_status"]];
                        [songList setFile_duration:[infoDict objectForKey:@"file_duration"]];
                        [songList setFile_id:[infoDict objectForKey:@"file_id"]];
                        [songList setFile_name:[infoDict objectForKey:@"file_name"]];
                        [songList setFile_url:[infoDict objectForKey:@"file_url"]];
                        [songList setPerson_name:[infoDict objectForKey:@"person_name"]];
                        [songList setType:[infoDict objectForKey:@"type"]];

                        [self.songListArray addObject:songList];
                    }];
                }
            [self.tableView reloadData];
            }
        });
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Content Filtering
- (void)filterContentForSearchText:(NSString*)searchText {
	/*
	 Update the filtered array based on the search text and scope.
	 */
	//[self.filteredListContent removeAllObjects]; // First clear the filtered array.
	/*
	 Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
	 */
    [self.filteredArray removeAllObjects];
    if([searchText length] > 0) {
        for (KPSonngCategoryInfo *tempDict in self.songListArray)
        {
            NSString *songName = tempDict.file_name;
            NSString *singerName=tempDict.person_name;
#define kOptions1 (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
            NSComparisonResult result = [songName compare:searchText options:kOptions1 range:[songName rangeOfString:searchText options:kOptions1]];
            NSComparisonResult result2 = [singerName compare:searchText options:kOptions1 range:[singerName rangeOfString:searchText options:kOptions1]];
            if (result == NSOrderedSame ||result2==NSOrderedSame)
            {
                [self.filteredArray addObject:tempDict];
            }
            
    }
    }
        [self.tableView reloadData];
}


-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg.png"];
    UIImage *homeImage=[UIImage imageNamed:@"back_blue"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 11, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchDown];
   
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:self.rootCategoryTitle?self.rootCategoryTitle:self.categoryTitleText];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:14.0/255 green:26.0/255 blue:93.0/255 alpha:1.0]];
    [titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [topStripImgView addSubview:titleLbl];
    
    UIButton * searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchButton addTarget:self action:@selector(searchButton:) forControlEvents:UIControlEventTouchUpInside];
    UIImage * searchImage = [UIImage imageNamed:@"search_1"];

    [searchButton setBackgroundImage:searchImage forState:UIControlStateNormal];
    [searchButton setFrame:CGRectMake(280.0, 13.0, searchImage.size.width, searchImage.size.height)];
    [topStripImgView addSubview:searchButton];
    
    if (self.songListType==KPNormalPlayListType) {
        [self addCategoryTitleWithNavigationButton];
    }
    [self.view addSubview:topStripImgView];
}


-(IBAction)searchButton:(UIButton*)sender
{
    NSLog(@"Search Icon");
    sender.selected=!sender.selected;
    int factor=0;
    if (self.songListType!=KPNormalPlayListType)
    {
        factor=30;
    }
    
    if (sender.selected)
    {
        [self addCustomSearchBar];
        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?(137-factor):(137-factor), self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?(137-factor):(137-factor)))];
    }
    else
    {
        UITextField *searchTF=(UITextField*)[self.view viewWithTag:8934];
        [searchTF removeFromSuperview];
        [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?(102-factor):(102-factor), self.view.frame.size.width, self.view.frame.size.height-((deviceFactor==1)?(102-factor):(102-factor)))];
    }
    
}
-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)homeButtonAction:(id)sender
{
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
    //[[self getPlayerView] removeFromSuperview];
    KPHomeViewController *vc;
    for(id controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[KPHomeViewController class]])
        {
            vc=(KPHomeViewController*)controller;
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    if (!vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
    }
}



-(void)adInitialViewComponent
{
    int factor=0;
    if (self.songListType!=KPNormalPlayListType)
    {
        factor=30;
    }
    [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?(102-factor):(102-factor), self.view.frame.size.width, (deviceFactor==1)?self.view.frame.size.height-(102-factor):self.view.frame.size.height-(102-factor))];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    if ([UIDevice currentDevice].systemVersion.floatValue>=7) {
        self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    //[self addBottomBarWithHeadfoneIcon];
    [self addBottomView];
    [self addTopView];
    
}


-(void)addBottomView
{
    UIImageView * bottomImage = [[UIImageView alloc]init];
    [bottomImage setFrame:CGRectMake(0.0, self.view.frame.size.height-120, 320.0, 120.0)];
    [bottomImage setImage:[UIImage imageNamed:@"blur-footer"]];
    [self.view addSubview:bottomImage];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(klipsButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-80, 60.0, 60.0)];
    [button setImage:[UIImage imageNamed:@"klip"] forState:UIControlStateNormal];
    [self.view addSubview:button];
}

-(IBAction)klipsButtonAction:(id)sender
{
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    
    //[[self getPlayerView] removeFromSuperview];
    KPHomeViewController *vc;
    for(id controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[KPHomeViewController class]])
        {
            vc=(KPHomeViewController*)controller;
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }
    if (!vc)
    {
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)addLabelWithFrame:(CGRect)frame title:(NSString*)title tag:(int)tag
{
    UILabel *label=[[UILabel alloc]initWithFrame:frame];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:19]];
    [label setText:title];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTag:tag];
    [self.view addSubview:label];
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
}

-(void)addLocalNavigationBar:(CGRect)frame
{
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow"];
    [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 16, arrow.size.width , arrow.size.height ) tag:778 navigationBar:viewNavigation BGImage:arrow];
    [self.view addSubview:viewNavigation];
}

-(void)addCategoryTitleWithNavigationButton
{
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, 73, 120,25)];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setBackgroundColor:[UIColor colorWithRed:7/255.0 green:12/255.0 blue:19/255.0 alpha:1.0]];
    [titleLbl setTextColor:[UIColor colorWithRed:165/255.0 green:166/255.0 blue:211/255.0 alpha:1.0]];
    [titleLbl setFont:[UIFont boldSystemFontOfSize:18]];
    [titleLbl setTag:8349];
    [titleLbl setText:[self.categoryTitleText uppercaseString]];
    
    UIImage *left_icon=[UIImage imageNamed:@"arrow-left"];
    UIImage*right_icon=[UIImage imageNamed:@"arrow-right"];
    UIButton *left_naviBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [left_naviBtn setFrame:CGRectMake(titleLbl.frame.origin.x-left_icon.size.width-10, 75, left_icon.size.width, left_icon.size.height)];
    [left_naviBtn setImage:left_icon forState:UIControlStateNormal];
    [left_naviBtn addTarget:self action:@selector(leftArrowNavAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:left_naviBtn];
    
    UIButton *right_naviBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [right_naviBtn setFrame:CGRectMake(titleLbl.frame.origin.x+titleLbl.frame.size.width+10, 75, right_icon.size.width, right_icon.size.height)];
    [right_naviBtn setImage:right_icon forState:UIControlStateNormal];
    [right_naviBtn addTarget:self action:@selector(rightArrowNavAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:right_naviBtn];
    
    [self.view addSubview:titleLbl];
}

-(void)leftArrowNavAction:(UIButton*)sender
{
        if ((self.selectedIndex-1)>=0 && (self.selectedIndex-1)<self.allCategoryArray.count) {
            NSDictionary *tempDict=[self.allCategoryArray objectAtIndex:--self.selectedIndex];
            NSLog(@"temppppp Data %@",tempDict);
            self.category_id=[tempDict valueForKey:@"category_id"];
            UILabel *titleLbl=(UILabel*)[self.view viewWithTag:8349];
            [titleLbl setText:[[tempDict valueForKey:@"category_name"] uppercaseString]];
            [self getCategoryDetailServiceWithFlag:YES];
        }
    NSLog(@"leftArrowNavAction");
}

-(void)rightArrowNavAction:(UIButton*)sender
{
        if ((self.selectedIndex+1)<self.allCategoryArray.count && (self.selectedIndex+1)>=0) {
            NSDictionary *tempDict=[self.allCategoryArray objectAtIndex:++self.selectedIndex];
            NSLog(@"temppppp Right Data %@",tempDict);
            self.category_id=[tempDict valueForKey:@"category_id"];
            UILabel *titleLbl=(UILabel*)[self.view viewWithTag:8349];
            [titleLbl setText:[[tempDict valueForKey:@"category_name"] uppercaseString]];
            [self getCategoryDetailServiceWithFlag:YES];
        }
    NSLog(@"rightArrowNavAction");
    
}

-(void)addCustomSearchBar
{
    int factor=0;
    if (self.songListType!=KPNormalPlayListType) {
        factor=30;
    }
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:CGRectMake(0, 102-factor, self.view.frame.size.width, 30)];
    [search_txtF setDelegate:(id)self];
    [search_txtF setTag:8934];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: TextColor,NSFontAttributeName:[UIFont italicSystemFontOfSize:15.0]}];
    [search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:15.0]];
    [search_txtF setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIImage *search=[UIImage imageNamed:@"search"];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 4, search.size.width    , search.size.height)];
    [searchIcon setImage:search];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:search_txtF];
}
-(void)addImageViewWithFrame:(CGRect)frame image:(UIImage*)image tag:(int )tag
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    [imageView setImage:image];
    [imageView setTag:tag];
    [self.view addSubview:imageView];
}
-(IBAction)arrowAction:(UIButton*)sender
{
    if (sender.tag==778)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(sender.tag==779)
    {
        //fav
        KPPlayListViewController *vc=[[KPPlayListViewController alloc]init];
        vc.playListType=KPFavouriteType;
        // [self.navigationController pushViewController:vc animated:YES];
    } else if(sender.tag==780) {
       // KPPlayListViewController *vc=[[KPPlayListViewController alloc]init];
       // vc.playListType=KPNormalPlayListType;
      //  KPImagePlayerViewController *vc12=[[KPImagePlayerViewController alloc]init];
        //[self.navigationController pushViewController:vc12 animated:YES];
    }
    NSLog(@"Other then back arrow on navigation bar %ld",(long)sender.tag);
    
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPHONE_5?66:54.14;//70.5
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld row selected",(long)indexPath.row+1);
    KPSonngCategoryInfo *search=[(self.isSearchActive)?self.filteredArray:self.songListArray objectAtIndex:indexPath.row];
   
    NSMutableArray *dataArray = [NSMutableArray new];
    [dataArray addObject:search];
    KPVideoViewController *vc=[KPVideoViewController new];
    [vc setCategoryInfo:search];
    vc.videoArray=(self.isSearchActive)?self.filteredArray:self.songListArray ;
    vc.selectedIndex=indexPath.row;
    if (self.songListType==KPSongListDownloads) {
        vc.isDownloads=YES;
    }
    else
        vc.isDownloads=NO;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)addNoDataFoundLabel
{
    UILabel *label=(UILabel*)[self.view viewWithTag:8493];
    if (label) {
        [label removeFromSuperview];
        label=nil;
    }
    label=[[UILabel alloc]initWithFrame:CGRectMake(20, 160, 280, 20)];
    [label setText:@"No Data Found!"];
    [label setTag:8493];
    [label setTextColor:[UIColor blueColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [self.view addSubview:label];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount=(self.isSearchActive)?self.filteredArray.count:self.songListArray.count;
    if (rowCount==0&& self.isSearchActive) {
        [self addNoDataFoundLabel];
    }
    else
    {
        UILabel *label=(UILabel*)[self.view viewWithTag:8493];
        if (label) {
            [label removeFromSuperview];
            label=nil;
        }
    }
    return rowCount;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPCategoryCell *cell=(KPCategoryCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[KPCategoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    KPSonngCategoryInfo *tempDict=[(self.isSearchActive)?self.filteredArray:self.songListArray objectAtIndex:indexPath.row];
    
    NSString *thumbnailURLString=@"http://www.examiner.com/images/blog/EXID9626/images/TheLastSongPoster.jpg";
    [cell.thumbnailImage setFrame:CGRectMake(cell.thumbnailImage.frame.origin.x, cell.thumbnailImage.frame.origin.y, cell.thumbnailImage.frame.size.width, IS_IPHONE_5?65:53.14)];
    [cell.thumbnailImage.superview setFrame:CGRectMake(cell.thumbnailImage.superview.frame.origin.x, cell.thumbnailImage.superview.frame.origin.y, cell.thumbnailImage.superview.frame.size.width, IS_IPHONE_5?65:53.14)];

    [cell.thumbnailImage setImageWithURL:[NSURL URLWithString:thumbnailURLString] placeholderImage:nil];
    [cell.accessoryButton setTag:indexPath.row];
    [cell.accessoryButton addTarget:self action:@selector(favouriteBtnClicked:) forControlEvents:UIControlEventTouchDown];
    
    [cell.descLable setText:@"1 Songs | 0 hours 4 minutes"];

    if (self.songListType==KPSongListDownloads) {
        [cell.accessoryButton setHidden:YES];
    }
    @try {
       
        if ([tempDict.favourite_status isEqualToString:@"1"]) {
            cell.accessoryButton.selected=YES;
        }
        else
            cell.accessoryButton.selected=NO;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    [cell.titleLable setText:tempDict.file_name];
    return cell;
}

-(void)updatingFavouriteStatusDBWithInfo:(KPSonngCategoryInfo*)categoryInfo
{
    NSLog(@"is favvv %@",self.navigationController.viewControllers);
    //    if (![[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
    //    {
    
    KPSearchDataINfo *info=[KPSearchDataINfo new];
    info.file_duration=categoryInfo.file_duration;
    info.file_id=categoryInfo.file_id;
    info.file_name=categoryInfo.file_name;
    info.file_url=categoryInfo.file_url;
    info.type=categoryInfo.type;
    info.category_id=categoryInfo.category_id;
    info.person_name=categoryInfo.person_name;
    info.is_favourite=categoryInfo.favourite_status;
    BOOL yes=[[self getCoreDataInitializer] updateFavStatusForSearch:info];
    NSLog(@"is_favourite in video player %hhd",yes);
    //  }
    
}


-(void)addToFavouritesForIndex:(UIButton*)sender
{
    KPSonngCategoryInfo *info=[self.songListArray objectAtIndex:sender.tag];
    showActivity(self.navigationController.view);
    __weak typeof(self) weakSelf = self;
    [self addFileToFavouritesWithFileId:info.file_id categoryId:info.category_id callBack:^(id result,NSError *error) {
        NSLog(@"fabbbbbvvv result %@",result);
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(weakSelf.navigationController.view);
            if (result==nil)
            {
                sender.selected=!sender.selected;
                showAlertView(nil, @"Network error.");
            }
            else
            {   NSDictionary *response =[result valueForKey:@"response"];
                NSString *msg;
                if ([[response valueForKey:@"success"]isEqualToString:@"true"]) {
                    
                    msg=@"Successfully added";
                    if (sender.selected) {
                        info.favourite_status=@"1";
                        [weakSelf.songListArray replaceObjectAtIndex:sender.tag withObject:info];

                    }
                    else
                    {
                        info.favourite_status=@"0";
                        if (weakSelf.songListType==KPSongListFavourites) {
                            [weakSelf.songListArray removeObject:info];
                            [weakSelf.tableView reloadData];
                        }
                        else
                            [weakSelf.songListArray replaceObjectAtIndex:sender.tag withObject:info];

                    }
                    [weakSelf updatingFavouriteStatusDBWithInfo:info];
                   // [weakSelf.songListArray replaceObjectAtIndex:sender.tag withObject:info];
                }
                else
                showAlertView(nil,[NSString stringWithFormat:@"%@.",[response valueForKey:@"error"]]);
            }
        });
        
        NSLog(@"addfileto favvvv %@",result);
    }];
}
-(void)displayAlertView
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Not Login!" message:@"Please login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login"       , nil];
    alertView.tag=34343;
    [alertView show];
}

-(IBAction)favouriteBtnClicked:(UIButton*)sender
{
    if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"])
    {
        [self displayAlertView];
        return;
    }
    sender.selected=!sender.selected;
    [self addToFavouritesForIndex:sender];
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"textlength %d",text.length);
    if (text.length>0) {
        self.isSearchActive=YES;
        [self filterContentForSearchText:text];
    }
    else
        
    {
        self.isSearchActive=NO;
        [self.filteredArray removeAllObjects];
        [self.tableView reloadData];
    }

    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==34343 && buttonIndex==1) {
        KPAppDelegate *app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
        [app.tabListController logOutButtonClicked];
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
}
@end
