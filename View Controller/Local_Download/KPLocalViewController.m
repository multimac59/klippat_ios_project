//
//  KPLocalViewController.m
//  Klippat
//
//  Created by Amit Gupta on 23/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPLocalViewController.h"
#import "KPHomeViewController.h"

@interface KPLocalViewController ()
@property(nonatomic,strong)NSMutableArray *filteredArray;
@property(nonatomic)BOOL isSearchActive;
@end

@implementation KPLocalViewController

@synthesize downloadItemArray=_downloadItemArray;
@synthesize tableView=_tableView;

-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    return _tableView;
}
-(NSMutableArray*)downloadItemArray
{
    if (!_downloadItemArray) {
        _downloadItemArray=[[NSMutableArray alloc]init];
    }
    return _downloadItemArray;
    
}
-(NSMutableArray*)filteredArray
{
    if (!_filteredArray) {
        _filteredArray=[NSMutableArray new];
    }
    return _filteredArray;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addInitialViewComponent];
    
    NSMutableArray *dataArray  = [[self getCoreDataInitializer] getDistinctDataForDownLoad];
    [dataArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.downloadItemArray addObject:[obj objectForKey:@"downloadtype"]];
        NSLog(@"download object %@ and full array %@",obj,self.downloadItemArray);
    }];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addInitialViewComponent {
    
    [self.tableView setFrame:CGRectMake(25, 125, self.view.frame.size.width-50, 380)];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    if ([[UIDevice currentDevice]systemVersion].floatValue>=7 ) {
        self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    [self.tableView setShowsVerticalScrollIndicator:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    [self addTopView];
    [self addCustomSearchBar];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
}

-(KPCoreDataInitializer *)getCoreDataInitializer {
    
    KPCoreDataInitializer *dataInitilizer = [KPCoreDataInitializer sharedMySingleton];
    [dataInitilizer initializeTheCoreDataModelClasses];
    return dataInitilizer;
}

-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"btg.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow.png"];
    
    [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 16, arrow.size.width , arrow.size.height) tag:778 navigationBar:viewNavigation BGImage:arrow];
    
    [self.view addSubview:viewNavigation];
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"+" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:35]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg.png"];
    UIImage *homeImage=[UIImage imageNamed:@"home"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 40)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(30, 8, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:@"DOWNLOADS"];
    [titleLbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    NSLog(@"HelveticaNeueInterface %@",titleLbl.font);
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
    
    
}
-(void)addCustomSearchBar
{
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:CGRectMake(25, 85, 270, 30)];
    [search_txtF setDelegate:(id)self];
    [search_txtF setFont:[UIFont fontWithName:@"Helvetica" size:14.0]];
    [search_txtF setTextColor:TextColor];
    UIImage *searchImage=[UIImage imageNamed:@"search"];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: TextColor,NSFontAttributeName:[UIFont italicSystemFontOfSize:12.0]}];
    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 6, searchImage.size.width , searchImage.size.height)];
    [searchIcon setImage:searchImage];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:search_txtF];
}

-(void)homeButtonAction:(id)sender {
    
    [[KPSingletonClass sharedObject] setViaDownloads:@""];
    [[KPSingletonClass sharedObject] setViaFavourites:@""];
    [[KPSingletonClass sharedObject] setViaPlayList:@""];
    [self.navigationController popViewControllerAnimated:YES];
//    KPAppDelegate *app=(KPAppDelegate*)[UIApplication sharedApplication].delegate;
//    HHTabListController *vc;
//    for(id controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[HHTabListController class]]) {
//            vc=(HHTabListController*)controller;
//            [self.navigationController popToViewController:controller animated:YES];
//            break;
//        }
//    }
//    if (!vc) {
//        [self.navigationController pushViewController:app.tabListController animated:YES];
//        
//    }
}

-(void)arrowAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView Delegate And DataSorce

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *tempDict=[(!self.isSearchActive) ? self.downloadItemArray:self.filteredArray objectAtIndex:indexPath.row];
    if (getDownLoadedCategoryType(tempDict) == KPVideo) {
        KPSongListViewController *vc=[[KPSongListViewController alloc]init];
        vc.categoryTitleText = [NSString stringWithFormat:@"%@", tempDict];
        vc.songListType = KPSongListDownloads;
        vc.downLoadcategoryType = KPVideo;
        [self.navigationController pushViewController:vc animated:YES];

    }else if(getDownLoadedCategoryType(tempDict) == KPImage) {
        NSMutableArray *dataArrary  = [[self getCoreDataInitializer] fetchDataAccordingToFilter:KPImage];
    
        __block NSMutableArray *downloadArray=[NSMutableArray new];
        [dataArrary enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            DownloadInfo *downLoad = obj;
            KPSonngCategoryInfo *downlOadInfoLocal = [[KPSonngCategoryInfo alloc] init];
            [downlOadInfoLocal setFile_url:downLoad.localPath];
            [downlOadInfoLocal setFile_name:downLoad.file_name];
            [downlOadInfoLocal setFile_duration:downLoad.file_duration];
            [downlOadInfoLocal setServerURL:downLoad.file_url];
            [downlOadInfoLocal setFile_id:downLoad.file_id];
            [downlOadInfoLocal setCategory_id:downLoad.category_id];
            [downlOadInfoLocal setPerson_name:downLoad.person_name];
            [downlOadInfoLocal setType:downLoad.type];
            NSLog(@"downloded info %@ %@ ",downlOadInfoLocal.file_url,downlOadInfoLocal.person_name);
            [downloadArray addObject:downlOadInfoLocal];
            
        }];
        NSLog(@"image oR qouts %@ and %@",dataArrary,downloadArray);

        KPGridGalleryViewController *vc=[KPGridGalleryViewController new];
        vc.categoryName=@"Downloads";
        vc.imageArray=downloadArray;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)addNoDataFoundLabel
{
    UILabel *label=(UILabel*)[self.view viewWithTag:8493];
    if (label) {
        [label removeFromSuperview];
        label=nil;
    }
    label=[[UILabel alloc]initWithFrame:CGRectMake(20, 160, 280, 20)];
    [label setText:@"No Data Found!"];
    [label setTag:8493];
    [label setTextColor:[UIColor blueColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [self.view addSubview:label];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%@",self.downloadItemArray);
    int rowCount=(!self.isSearchActive)? self.downloadItemArray.count:self.filteredArray.count;
    if (rowCount==0 && self.isSearchActive) {
        [self addNoDataFoundLabel];
    }
    else
    {
        UILabel *label=(UILabel*)[self.view viewWithTag:8493];
        if (label) {
            [label removeFromSuperview];
            label=nil;
        }
    }
    return rowCount;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPDownloadCell *cell=(KPDownloadCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[KPDownloadCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    NSString *tempDict=[(!self.isSearchActive)?self.downloadItemArray:self.filteredArray objectAtIndex:indexPath.row];
    
    [cell.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16 ]];
    [cell.titleLabel setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [cell.titleLabel setText:[NSString stringWithFormat:@" %@",tempDict]];
    
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *textSearch=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textSearch.length>0) {
        self.isSearchActive=YES;
        [self filterListForText:textSearch];
    }
    else
    {    self.isSearchActive=NO;
        [self.tableView reloadData];
    }
    return YES;
}
-(void)filterListForText:(NSString*)textSearch
{
    [self.filteredArray removeAllObjects];
    for (NSString *tempDict in self.downloadItemArray) {
        NSLog(@"tempDict tempDict %@", tempDict);
#define kOptions1 (NSCaseInsensitiveSearch|NSRegularExpressionSearch)
        NSComparisonResult result=[tempDict compare:textSearch options:kOptions1 range:[tempDict rangeOfString:textSearch options:kOptions1]];
        
        if (result==NSOrderedSame) {
            [self.filteredArray addObject:tempDict];
        }
    }
    [self.tableView reloadData];
}

@end
