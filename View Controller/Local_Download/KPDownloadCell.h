//
//  KPDownloadCell.h
//  Klippat
//
//  Created by Amit Gupta on 23/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPDownloadCell : UITableViewCell
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIImageView *thumbnail1;
@property(nonatomic)BOOL hasImage;
@end
