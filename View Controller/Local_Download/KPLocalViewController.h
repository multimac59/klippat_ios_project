//
//  KPLocalViewController.h
//  Klippat
//
//  Created by Amit Gupta on 23/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCommonViewController.h"
#import "KPDownloadCell.h"
#import "KPCoreDataInitializer.h"
#import "DownloadInfo.h"
#import "KPDownLoadInfo.h"
#import "KPGridGalleryViewController.h"
@interface KPLocalViewController : KPCommonViewController
@property(nonatomic,strong)NSMutableArray *downloadItemArray;
@property(nonatomic,strong)UITableView *tableView;
@end
