//
//  KPDownloadCell.m
//  Klippat
//
//  Created by Amit Gupta on 23/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPDownloadCell.h"

@implementation KPDownloadCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIView *container=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 280, 55)];
        [container setBackgroundColor:[UIColor grayColor]];
        [container.layer setBorderColor:[UIColor blackColor].CGColor];
        [container setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"cellBG1.png"]]];
        UIImage *image=[UIImage imageNamed:@"cellBG1"];
        NSLog(@"dynamic image size %@",NSStringFromCGSize(image.size));
        
        self.thumbnail1 =[[UIImageView alloc]initWithFrame:CGRectMake(5, 10.5, 29, 29)];
        [container addSubview:self.thumbnail1];
        
        self.titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(45, 14, 200, 30)];
        self.textLabel.textAlignment=NSTextAlignmentLeft;
        [container addSubview:self.titleLabel];
        
        [self addSubview:container];
    }
    return self;

}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
