//
//  KPSlideImageNoteViewController.h
//  Klippat
//
//  Created by Ravi kumar on 11/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

typedef enum {
    KPSlideTypeNotes=0,
    KPSlideTypeImage=1,
}KPSlideType;
#import <UIKit/UIKit.h>

@interface KPSlideImageNoteViewController : KPCommonViewController
@property(nonatomic)KPSlideType slideType;
@property(nonatomic,strong)UIScrollView *scrollView;
@end
