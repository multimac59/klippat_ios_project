//
//  KPSlideImageNoteViewController.m
//  Klippat
//
//  Created by Ravi kumar on 11/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSlideImageNoteViewController.h"

@interface KPSlideImageNoteViewController ()

@end

@implementation KPSlideImageNoteViewController
@synthesize scrollView=_scrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(UIScrollView*)scrollView
{
    if (!_scrollView) {
        _scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(65, self.view.frame.size.height/4, 185, 260)];
        [_scrollView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:self.scrollView];
    }
    return _scrollView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:197/255.0 green:171/255.0 blue:135/255.0 alpha:1.0]];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addInitialViewComponent];
    // Do any additional setup after loading the view.
}

-(void)addInitialViewComponent
{
    UIImage *noteImage=[UIImage imageNamed:@"noteImage"];
    UIImage *noteIcon=[UIImage imageNamed:@"noteIcon"];
    UIImage *leftArrow=[UIImage imageNamed:@"airo_1"];
    UIImage *rightArrow=[UIImage imageNamed:@"airo_right"];

    [self addImageViewWithFrame:CGRectMake(122, 20, 65, 60) image:noteIcon tag:6565];
    [self addImageViewWithFrame:CGRectMake(0, 0, 185,260) image:noteImage tag:6566];
    [self addButtonWithFrame:CGRectMake(5, self.view.frame.size.height/2-30, 35, 35) BGImage:leftArrow tag:6567];

    [self addButtonWithFrame:CGRectMake(self.view.frame.size.width-50, self.view.frame.size.height/2-30, 35, 35) BGImage:rightArrow tag:6568];
}
-(void)addButtonWithFrame:(CGRect)frame BGImage:(UIImage*)image tag:(int)tagValue
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    [button setFrame:frame];
    [self.view addSubview:button];
}

-(IBAction)buttonClickAction:(UIButton*)sender
{
    if (sender.tag==6567)
    {
        NSLog(@"left button clcik");
    }
    else
    NSLog(@"right button clcik");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addImageViewWithFrame:(CGRect)frame image:(UIImage*)image tag:(int)tagValue
{
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:frame];
    [imageView setTag:tagValue];
    [imageView setImage:image];
    if (tagValue==6566) {
        [self.scrollView addSubview:imageView];
    }
    else
    [self.view addSubview:imageView];
    
    
}

-(void)addButtonOnNavigationBarFrame:(CGRect)frame tag:(int)tagValue navigationBar:(UINavigationBar*)navi_bar BGImage:(UIImage*)image
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTag:tagValue];
    if (!image) {
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    }
    [button addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchDown];
    [navi_bar addSubview:button];
}
-(void)addLocalNavigationBar:(CGRect)frame {
    
    UIImage *navi_Image = [UIImage imageNamed:@"footer.png"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"airo_1.png"];
    UIImage *heart=[UIImage imageNamed:@"fav_heart"];
    UIImage *home=[UIImage imageNamed:@"homeImage"];
    UIImage *music=[UIImage imageNamed:@"musicImage"];
    UIImage *share=[UIImage imageNamed:@"shareImage"];
    [self addButtonOnNavigationBarFrame:CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2 - 15, arrow.size.width + 10, arrow.size.height + 10) tag:778 navigationBar:viewNavigation BGImage:arrow];
    [self addButtonOnNavigationBarFrame:CGRectMake(45,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:779 navigationBar:viewNavigation BGImage:heart];
    [self addButtonOnNavigationBarFrame:CGRectMake(95,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:780 navigationBar:viewNavigation BGImage:music];
    [self addButtonOnNavigationBarFrame:CGRectMake(145,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:781 navigationBar:viewNavigation BGImage:home];
    [self addButtonOnNavigationBarFrame:CGRectMake(195,viewNavigation.frame.size.height/2 - 15, arrow.size.width + 15, arrow.size.height + 10) tag:782 navigationBar:viewNavigation BGImage:share];
    [self addButtonOnNavigationBarFrame:CGRectMake(245, viewNavigation.frame.size.height/2 - 15,arrow.size.width + 40, arrow.size.height + 10) tag:783 navigationBar:viewNavigation BGImage:[UIImage imageNamed:@""]];
    [self.view addSubview:viewNavigation];
}
-(IBAction)arrowAction:(UIButton*)sender
{
    if (sender.tag==778) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
        NSLog(@"Other then back arrow on navigation bar %ld",(long)sender.tag);
}

@end
