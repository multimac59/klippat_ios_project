//
//  KPLoginViewController.m
//  Klippat
//
//  Created by Komal Kumar on 02/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//
#import "KPDrawingView.h"
#import "KPLoginViewController.h"
#import "KPSignUpViewController.h"
#import "KPSettingViewController.h"
#import "KPSearchViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import "KPAppDelegate.h"
@interface KPLoginViewController ()
{
    bool IsGPluse_once;
    NSTimer *timer;
    KPAppDelegate *app;
}
@end

@implementation KPLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:@"userName"])
    {
        UITextField *userName=(UITextField*)[self.transParenentView viewWithTag:1001];
        UITextField *password=(UITextField*)[self.transParenentView viewWithTag:1002];
        [userName setText:[userDefaults valueForKey:@"userName"]];
        [password setText:[userDefaults valueForKey:@"password"]];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
  
    [self addInitialViewComponent];

    [self setNeedsStatusBarAppearanceUpdate];
    
  app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
    if ([app.player isPlaying])
    {
        [self showSplash];
        timer=[NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(removeSplashScreen:) userInfo:nil repeats:YES];
    }
}


-(void)showSplash
{
    UIImageView *splashScreen=[[UIImageView alloc]initWithFrame:self.view.frame];
    //[splashScreen setImage:[UIImage imageNamed:IS_IPHONE_5?@"splash_without_logo":@"Default"]];
    [splashScreen setImage:[UIImage imageNamed:IS_IPHONE_5?@"splash_without_logo":@"splash_without_logo"]];
    
    
    UIImage * klips = [UIImage imageNamed:IS_IPHONE_5?@"klipatt-without-circle":@"klipatt-without-circle"];
   // UIImageView * klipsImageView = [[UIImageView alloc]initWithFrame:CGRectMake(104.0, 210.0, 110.0, 111.0)];
    UIImageView * klipsImageView = [[UIImageView alloc]initWithFrame:CGRectMake(104.0, 210.0, klips.size.width, klips.size.height)];
    [klipsImageView setImage:klips];
    //klipsImageView.center = self.view.center;
    //    circularImageView.center=klipsImageView.center;
    //    [klipsImageView addSubview:circularImageView];
    [splashScreen addSubview:klipsImageView];
    
    
    
    UIImage *circularImage=[UIImage imageNamed:IS_IPHONE_5?@"spin-ring":@"spin-ring"];
    UIImageView *circularImageView=[[UIImageView alloc]initWithFrame:CGRectMake(104.0, 210.0, circularImage.size.width-2, circularImage.size.height-2)];

    [circularImageView setImage:circularImage];
    [splashScreen addSubview:circularImageView];
    
    [self runSpinAnimationOnView:circularImageView duration:4.0 rotations:1.0 repeat:0];
    

    UIView *splashView=[[UIView alloc]initWithFrame:self.view.frame];
    [splashView setUserInteractionEnabled:NO];
    [self.view setUserInteractionEnabled:NO];
    [self.view addSubview:splashView];
    [splashView setTag:98724];
    [splashView addSubview:splashScreen];
    [self.view bringSubviewToFront:splashView];
}

- (void) runSpinAnimationOnView:(UIImageView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = NO;
    rotationAnimation.repeatCount = repeat;
    view.layer.speed=1.0*0.34;
    view.layer.timeOffset=[view.layer convertTime:CACurrentMediaTime() fromLayer:nil];
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void)removeSplashScreen:(NSTimer*)timer2
{
    
    KPAppDelegate *app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
    UIView *splashScreen=[self.view viewWithTag:98724];
    [self.view setUserInteractionEnabled:YES];
    if ([KPSingletonClass sharedObject].userID)
    {
        [self navigateToHomeScreenWithBlock:^(id response)
        {
            
 
        }];
        
    }
    [splashScreen removeFromSuperview];
    if ([app.player isPlaying])
    {
        [app.player stop];
    }
    [timer2 invalidate];
    
}
- (void)viewWillLayoutSubviews
{
    [_transParenentView.layer setBorderWidth:0.28];
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(UIView*)transParenentView
{
    if (!_transParenentView)
    {
        _transParenentView=[[UIView alloc]initWithFrame:CGRectMake(15, IS_IPHONE_5?76.5:32.5, 290, 415)];// y=50
        [_transParenentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]]];
        [_transParenentView.layer setBorderColor:[UIColor darkGrayColor].CGColor];
        [_transParenentView.layer setBorderWidth:0.30];
        [self.view addSubview:_transParenentView];
    }
    return _transParenentView;
}

-(IBAction)myCustomFunction:(id)sender
{
    
}
-(void)addInitialViewComponent
{
    if (self.transParenentView.superview==self.view)
    {
        [self.transParenentView removeFromSuperview];
        self.transParenentView=nil;
    }
    UIImage *loginImage=[UIImage imageNamed:@"txt-bg"];
    UIImage *noAccountImage=[UIImage imageNamed:@"noAccount"];
    UIImage *fbImage=[UIImage imageNamed:@"facebook"];
    UIImage *twtImage=[UIImage imageNamed:@"twitter"];
    UIImage *gpImage=[UIImage imageNamed:@"google"];
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    
    
    [self addTextFieldWithFrame:CGRectMake(10, 15, 270, 34) placeholder:ANLocalizedString(@"User Name", nil) rightIcon:nil tag:1001 text:([userDefaults valueForKey:@"userName"])?[userDefaults valueForKey:@"userName"]:@""];
    [self addTextFieldWithFrame:CGRectMake(10, 55, 270, 34) placeholder:ANLocalizedString(@"Password", nil) rightIcon:nil tag:1002 text:([userDefaults valueForKey:@"password"])?[userDefaults valueForKey:@"password"]:@""];
    [self addButtonWithFrame:CGRectMake(10, 100, 270, 34) titel:@"Sign In" BGImage:loginImage tag:1003 selectedBGImage:nil];
    [self addLabelWithFrame:CGRectMake(10, 140, self.view.frame.size.width-50, 34) titel:@"" tag:1004];
    [self addButtonWithFrame:CGRectMake(10, 180, 180, 34) titel:ANLocalizedString(@"Don't have an account?", nil) BGImage:nil tag:1005 selectedBGImage:noAccountImage];
    [self addButtonWithFrame:CGRectMake(10, 224, self.view.frame.size.width-50, 40) titel:@"" BGImage:fbImage tag:1006 selectedBGImage:fbImage];//facebook
    [self addButtonWithFrame:CGRectMake(10, 280, self.view.frame.size.width-50, 40) titel:@"" BGImage:twtImage tag:1007 selectedBGImage:twtImage];//twitter
    [self addButtonWithFrame:CGRectMake(10, 335, self.view.frame.size.width-50, 40) titel:@"" BGImage:gpImage tag:1008 selectedBGImage:gpImage];//google
    [self addButtonWithFrame:CGRectMake(232, 371, 60, 45) titel:ANLocalizedString(@"Skip", nil) BGImage:nil tag:1009 selectedBGImage:nil];
}


-(void)addTextFieldWithFrame:(CGRect)frame placeholder:(NSString*)placeholder rightIcon:(UIImage*)image tag:(int)tag text:(NSString*)text
{
    UITextField *textField=[[UITextField alloc]initWithFrame:frame];
    [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}]];
    [textField setText:text];
    [textField setTag:tag];
    
    NSLog(@"iamge size %@",NSStringFromCGSize(image.size));
    UIView *leftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,10, frame.size.height)];
    [textField setLeftViewMode:UITextFieldViewModeAlways];
    [textField setLeftView:leftView];
    [textField setDelegate:(id)self];
    if (tag==1002)
    {
        [textField setSecureTextEntry:YES];
    }
    [textField setBackground:[UIImage imageNamed:@"txt-bg"]];
    [textField setFont:[UIFont systemFontOfSize:14.0]];
    
    [self.transParenentView addSubview:textField];
    
}

-(void)addButtonWithFrame:(CGRect)frame titel:(NSString*)title BGImage:(UIImage*)image tag:(int)tag selectedBGImage:(UIImage*)imageSelected
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:imageSelected forState:UIControlStateSelected];
    [button setTag:tag];
    if ([title isEqualToString:@"Skip"])
    {
        [button.titleLabel setFont:[UIFont systemFontOfSize:16.0]];
    }
    else
        [button.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    [self.transParenentView addSubview:button];
    
    if (tag ==1005)
    {
        [button.titleLabel setFont:[UIFont systemFontOfSize:16.0]];
    }
    
}
-(void)addLabelWithFrame:(CGRect)frame titel:(NSString*)title tag:(int)tag
{
    UILabel *label=[[UILabel alloc]initWithFrame:frame];
    [label setText:title];
    [label setTextColor:[UIColor colorWithRed:131/255.0 green:17/255.0 blue:103/255.0 alpha:1.0]];
    [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"txt-bg"]]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:23.0f]];
    [label setTag:tag];
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(10, 13, 10, 10)];
    [button setTag:tag+748];
    [button setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    if ([[NSUserDefaults standardUserDefaults]valueForKey:@"userName"])
    {
        button.selected =YES;
    }
    [label addSubview:button];
    [button addTarget:self action:@selector(checkButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *rememberLbl=[[UILabel alloc]initWithFrame:CGRectMake(22, 13, 100, 10)];
    UITapGestureRecognizer *tapRemember=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(rememberMeAction)];
    [rememberLbl setUserInteractionEnabled:YES];
    [rememberLbl addGestureRecognizer:tapRemember];
    [rememberLbl setText:@"Remember me"];
    [rememberLbl setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [rememberLbl setBackgroundColor:[UIColor clearColor]];
    [rememberLbl setTextColor:[UIColor whiteColor]];
    [label addSubview:rememberLbl];
    button =[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(150, 9, 120, 20)];
    [button setTitle:@"Forgot Password" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [button addTarget:self action:@selector(forgotPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    [label addSubview:button];
    [label setUserInteractionEnabled:YES];
    [self.transParenentView addSubview:label];
    
}
-(void)rememberMeAction
{
    UIButton *rememberBtn=(UIButton*)[self.transParenentView viewWithTag:1004+748];
    [self checkButtonAction:rememberBtn];
}
-(IBAction)forgotPasswordAction:(id)sender
{
    KPForgotPasswordViewController *vc=[[KPForgotPasswordViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"forgotPassword");
}
-(IBAction)checkButtonAction:(UIButton*)sender
{
    sender.selected=!sender.selected;
    if (!sender.selected) {
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userName"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    NSLog(@"checkbuttonAction");
}
-(void)facebookLoginButtonAction
{
    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error)
    {
         
         [self sessionStateChanged:session state:state error:error];
     }];
}


-(void)fbInfoDidRecievedWithData:(NSDictionary*)response
{
    KPAppDelegate *app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    
    [infoDict setValue:[response valueForKey:@"id"] forKey:@"social_user_id"];
    [infoDict setValue:[[response valueForKey:@"name"]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ] forKey:@"user_name"];
    [infoDict setValue:[response valueForKey:@"email"] forKey:@"email"];
    [infoDict setValue:[response valueForKey:@"first_name"] forKey:@"fname"];
    [infoDict setValue:[response valueForKey:@"last_name"] forKey:@"lname"];
    [infoDict setValue:([[response valueForKey:@"gender"]isEqualToString:@"male"])?@"0":@"1" forKey:@"gender"];
    [infoDict setValue:@"Facebook" forKey:@"login_type"];
    [infoDict setValue:@"myimage.png" forKey:@"profile_image_url"];
    [infoDict setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:@"device_id"];
    [infoDict setValue:@"user_signin" forKey:@"task"];
    [infoDict setValue:[app.mDeviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"device_token"];
    [self serviceRequestforSocialLoginInfo:infoDict withFlag:NO];
    
}
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    if (!error && state == FBSessionStateOpen)
    {
        NSLog(@"Session opened");
        [FBRequestConnection startWithGraphPath:@"/me" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection,id result,NSError *error)
        {
            NSLog(@"user info on login %@",result);
            [self fbInfoDidRecievedWithData:result];
        }];
        
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        NSLog(@"Session closed");
    }
    
    // Handle errors
    if (error)
    {
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                // [self showMessage:alertText withTitle:alertTitle];
                
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
            }
        }
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}


-(IBAction)buttonClickAction:(UIButton*)sender
{
    [[KPSingletonClass sharedObject] setIsFromFacebook:NO];
    
    if (sender.tag==1003)//Login
    {
        
        UITextField *userName =(UITextField*)[self.transParenentView viewWithTag:1001];
        UITextField *password=(UITextField *)[self.transParenentView viewWithTag:1002];
        UILabel *lable=(UILabel*)[self.transParenentView viewWithTag:1004];
        UIButton *rememberBtn=(UIButton*)[lable viewWithTag:1004+748];
        
        if ([userName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0 || [password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length==0)
        {
            showAlertView(nil, @"Please fill all fields.");
            return;
        }
        if (rememberBtn.selected)
        {
            NSLog(@"raviloign remember me");
        }
        NSMutableDictionary *infoDict=[NSMutableDictionary new];
        [infoDict setValue:userName.text forKey:@"user_name"];
        [infoDict setValue:password.text forKey:@"password"];
        [infoDict setValue:@"user_signin" forKey:@"task"];
        [infoDict setValue:@"Normal" forKey:@"login_type"];
     
        [self serverRequestServiceWithInfo:infoDict withFlag:NO userName:userName.text password:password.text andSender:rememberBtn];
    }
    //
    else if (sender.tag==1005) {
        KPSignUpViewController *signUp=[[KPSignUpViewController alloc]init];
        [self.navigationController pushViewController:signUp animated:YES];
    }
    else if (sender.tag==1006) //FB
    {
        //[[KPSingletonClass sharedObject] setIsFromFacebook:YES];
        [self facebookLoginButtonAction];
    }
    else if (sender.tag==1007) //twitter
    {
        NSLog(@"Twitter Login");
        [self getTwitterData];
        
    } else if (sender.tag==1008) //G+
    {
        [self loginWithGooglePlus];
        
    }
    else if(sender.tag==1009)//1009 skippppppp
    {
        [KPSingletonClass sharedObject].userID=@"0";
        [self navigateToHomeScreenWithBlock:^(id response)
        {
            
        }];
    }
}


-(void)serverRequestServiceWithInfo:(NSMutableDictionary*)infoDict withFlag:(BOOL)flag userName:(NSString *)userName password:(NSString*)password andSender:(UIButton*)rememberBtn
{
    KPParser *parser=[KPParser new];
    showActivity(self.navigationController.view);
    [parser serviceRequestWithInfo:infoDict serviceType:KPSignInNormalType responseBlock:^(id response, NSError *error)
     {
         NSLog(@"user_signin sharda Prasad %@",response);
         if (response) {
             NSDictionary *responseDict = (NSDictionary*)[response valueForKey:@"response"];
             dispatch_async(dispatch_get_main_queue(), ^{
                 dismissHUD(self.navigationController.view);
                 if([[responseDict valueForKey:@"success"] isEqualToString:@"true"]){
                     [KPSingletonClass sharedObject].userID = [[responseDict valueForKey:@"user_details"] valueForKey:@"user_id"];
                     
                     NSLog(@"[KPSingletonClass sharedObject].userID :- %@", [KPSingletonClass sharedObject].userID);
                     
                     if (rememberBtn.selected) {
                         NSLog(@"raviloign remember me");
                         NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
                         [userDefaults setObject:userName forKey:@"userName"];
                         [userDefaults setObject:password forKey:@"password"];
                         [userDefaults synchronize];
                     }
                     [self navigateToHomeScreenWithBlock:^(id response)
                      {
                          
                      }];
                 }
                 else{
                     showAlertView(nil, [(NSString*)[responseDict valueForKey:@"error"] stringByAppendingString:@"."]);
                 }
                 
                 
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 dismissHUD(self.navigationController.view);
                 if (!flag) {
                     [self serverRequestServiceWithInfo:infoDict withFlag:YES userName:userName password:password andSender:rememberBtn];
                 }
                 else
                 showAlertView(nil, @"Network error.");
             });
         }
     }];
}

-(void) getTwitterData
{
    
    [SCTwitter loginViewControler:self callback:^(BOOL success)
    {
        //loadingView.hidden = YES;
        if (success)
        {
            NSLog(@"Login is Success -  %i", success);
            
            [SCTwitter getUserInformationCallback:^(BOOL success, id result)
            {
                if ([result isKindOfClass:[NSArray class]])
                {
                    NSMutableArray *twitterArray = result;
                    
                    [twitterArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop)
                    {
                        NSDictionary *twitterDict = [twitterArray objectAtIndex:idx];
                        NSLog(@"resultresultresult : %@ twitterDict -- %@", result, [twitterDict valueForKey:@"id"]);
                        
                        NSLog(@"Twitter profile_image_url :- %@ :-screen_name :%@ :-status : %@ : location : %@ : name : %@", [twitterDict valueForKey:@"profile_image_url"], [twitterDict valueForKey:@"screen_name"], [[twitterDict objectForKey:@"status"] valueForKey:@"id"], [twitterDict valueForKey:@"location"], [twitterDict valueForKey:@"name"]);
                        
                        KPAppDelegate *app=[UIApplication sharedApplication].delegate;
                        NSMutableDictionary *dataInfo=[NSMutableDictionary new];
                        [dataInfo setValue:[twitterDict valueForKey:@"id"] forKey:@"social_user_id"];
                        [dataInfo setValue:@"user_signin" forKey:@"task"];
                        [dataInfo setValue:[[twitterDict valueForKey:@"screen_name"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"user_name"];
                        [dataInfo setValue:@"" forKey:@"lname"];
                        [dataInfo setValue:@"" forKey:@"fname"];
                        [dataInfo setValue:@"0" forKey:@"gender"];
                        [dataInfo setValue:[twitterDict valueForKey:@"profile_image_url"] forKey:@"profile_image_url"];
                        [dataInfo setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:@"device_id"];
                        [dataInfo setValue:@"Twitter" forKey:@"login_type"];
                        [dataInfo setValue:@"" forKey:@"email"];
                        [dataInfo setValue:[app.mDeviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"device_token"];
                        NSLog(@"twitter daattatat  %@",dataInfo);
                        [self serviceRequestforSocialLoginInfo:dataInfo withFlag:NO];
                        
                    }];
                }
            }];
        } else
        {
            NSLog(@"Login is Success -  %i", success);
        }
    }];
}

-(void)loginWithGooglePlus
{
    
    GPPSignIn *signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
    signIn.clientID = kClientId;
    signIn.scopes = @[kGTLAuthScopePlusLogin, kGTLAuthScopePlusMe ];
    signIn.delegate = self;
    [[GPPSignIn sharedInstance] trySilentAuthentication];
    [signIn authenticate];
}

- (void)authentication:(GTMOAuth2Authentication *)auth
               request:(NSMutableURLRequest *)request
     finishedWithError:(NSError *)error {
    if (error != nil) {
        // Authorization failed
        NSLog(@"errorerrorerror %@", error);
    } else {
        //
        
        NSLog(@"Authorization succeeded %@", @"Authorization succeeded");
    }
}

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth  error:(NSError *)error {
    if (IsGPluse_once) {
        return;
    }
    IsGPluse_once=YES;
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    [plusService setAuthorizer:auth];
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error) {
                if (error) {
                    GTMLoggerError(@"Error: %@", error);
                } else {
                    GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
                    NSMutableDictionary *dataInfo=[NSMutableDictionary new];
                    [dataInfo setValue:person.identifier forKey:@"social_user_id"];
                    [dataInfo setValue:@"user_signin" forKey:@"task"];
                    [dataInfo setValue:[person.name.givenName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"user_name"];
                    [dataInfo setValue:person.name.familyName forKey:@"lname"];
                    [dataInfo setValue:person.name.givenName forKey:@"fname"];
                    [dataInfo setValue:([person.gender isEqualToString:@"male"])?@"0":@"1" forKey:@"gender"];
                    [dataInfo setValue:person.image.url forKey:@"profile_image_url"];
                    [dataInfo setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey:@"device_id"];
                    [dataInfo setValue:@"Google%20Plus" forKey:@"login_type"];
                    [dataInfo setValue:auth.userEmail forKey:@"email"];
                    [dataInfo setValue:[app.mDeviceToken stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"device_token"];
                    [self serviceRequestforSocialLoginInfo:dataInfo withFlag:NO];
                    NSLog(@"NSDictionary :- %@ & %@ and %@ and email %@", person.identifier,dataInfo,person.displayName ,auth.userEmail);
                    
                    
                }
            }];
}

-(void)navigateToHomeScreenWithBlock:(Completionblock)block
{
    dismissHUD(self.navigationController.view);
    IsGPluse_once=NO;
    NSLog(@"navigate to home screen %@",self.navigationController.viewControllers);
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.navigationController pushViewController:app.tabListController animated:YES];
    block(@"success");
}


-(void)serviceRequestforSocialLoginInfo:(NSMutableDictionary*)infoDict withFlag:(BOOL)flag//facebook or twitter
{
    
    NSLog(@"dataDict %@",infoDict);
    showActivity(self.navigationController.view);
    [[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPSocialLoginType responseBlock:^(id response, NSError *error) {
        NSLog(@"socail login %@",response);
        NSDictionary *resDict=[response valueForKey:@"response"];
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(self.navigationController.view);
            if (response==nil) {
                if (!flag) {
                    [self serviceRequestforSocialLoginInfo:infoDict withFlag:YES];
                }
                else
                showAlertView(nil, @"Network error.");
            }
            else if ([[resDict valueForKey:@"success"]isEqualToString:@"true"]) {
                [KPSingletonClass sharedObject].userID = [[resDict valueForKey:@"user_details"] valueForKey:@"user_id"];
                [[NSUserDefaults standardUserDefaults]setValue:[KPSingletonClass sharedObject].userID forKey:@"user_id"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [self navigateToHomeScreenWithBlock:^(id response) {
                    
                }];
            }
            else
                showAlertView(nil, @"Failed to login.");
        });
        
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    if (textField.tag==1002 && [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"] ) {
    //        textField.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    //    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //    NSString *text=[textField.text stringByReplacingCharactersInRange:range withString:string];
    //    NSString *password=[[[NSUserDefaults standardUserDefaults]objectForKey:@"password"]lowercaseString];
    //    if (textField.tag==1001 && [[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]isEqualToString:[text lowercaseString] ]) {
    //        UITextField *passwordTxt=(UITextField*)[self.view viewWithTag:1002];
    //        passwordTxt.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    //    }
    return YES;
}
-(void)viewWillDisappear:(BOOL)animated
{
    UITextField *t1=(UITextField*)[self.transParenentView viewWithTag:1001];
    UITextField *t2=(UITextField*)[self.transParenentView viewWithTag:1002];
    t1.text=nil;
    t2.text=nil;
    [t1 becomeFirstResponder];
    [t1 resignFirstResponder];
    [t2 resignFirstResponder];
    
    
    
}

@end
