//
//  KPImagePlayerViewController.m
//  Klippat
//
//  Created by Ravi kumar on 13/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#define DeviceFactor [UIScreen mainScreen].bounds.size.height/480
#define IS_iPHONE_5 ([UIScreen mainScreen].bounds.size.height==568)
//#define IS_iPHONE_6 ([UIScreen mainScreen].bounds.size.height==667)
//#define IS_iPHONE_6P ([UIScreen mainScreen].bounds.size.height==736)



#import "KPDownLoadInfo.h"
#import "KPCoreDataInitializer.h"
#import "AFURLSessionManager.h"
#import "KPImagePlayerViewController.h"
#import "KPSlideShowCell.h"
#import "KPHomeViewController.h"
#import "KPLocalViewController.h"
#import "SearchInfo.h"



@interface KPImagePlayerViewController () <SwipeViewDataSource, SwipeViewDelegate>
{
    KPSonngCategoryInfo *selectedImageInfo;
    UIButton *rightArrowButton,* leftArrowButton;
    int swipedIndex;
}
@property(nonatomic,strong)UIImage *currentImage;
@property(nonatomic,strong)UITableView *tableview;
@property (nonatomic, strong)  SwipeView *swipeView;
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation KPImagePlayerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



-(UIImage*)currentImage
{
    if (!_currentImage) {
        _currentImage=[UIImage new];
    }
    return _currentImage;
}
-(NSMutableArray*)imageArray
{
    if (!_imageArray) {
        _imageArray=[[NSMutableArray alloc]init];
    }
    return _imageArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.items  =[NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    [self addInintialViewComponent];
    [self addLabel];
}

-(void)addLabel
{
    UILabel * label = [[UILabel alloc]init];
    [label setFrame:CGRectMake(0.0, 69.0, 320.0, 32.0)];
    [label setBackgroundColor:[UIColor colorWithRed:7/255.0 green:12/255.0 blue:19/255.0 alpha:1.0]];
    [label setTextColor:[UIColor colorWithRed:116.0/255 green:123.0/255 blue:164.0/255 alpha:1.0]];
    
    [label setTag:5656];
    [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
    label.textAlignment = NSTextAlignmentCenter;
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    [label setText: [categoryInfo.file_name capitalizedString]];
    [self.view addSubview:label];
}

-(void)twitterPost
{
    KPSlideShowCell *customCell=(KPSlideShowCell*)[self.tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet setInitialText:@"Klippat iOS App"];
    [tweetSheet addURL:[NSURL URLWithString:selectedImageInfo.serverURL]];
    [tweetSheet addImage:customCell.pictureView.image];
    [self presentModalViewController:tweetSheet animated:YES];
}

-(void)addInintialViewComponent
{
    selectedImageInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    NSLog(@"self.imageArrayself.imageArray %@ and %d", self.imageArray, self.selectedIndex);
    [self addTitleViewAndBottomView];
    [self addImageViewToPlayWithFrame:IS_iPHONE_5? CGRectMake(0, 106, 320, 420*deviceFactor):CGRectMake(0, 98, 320, 330) tag:2335 image:selectedImageInfo.file_url];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"You have pressed the %@ button", [actionSheet buttonTitleAtIndex:buttonIndex]);
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqual:@"Facebook"])
    {
        [self facebookPost];
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqual:@"Twitter"])
    {
        [self twitterPost];
    }
}

-(void)shareOnFBWithImage:(UIImage*)image
{
    
    SLComposeViewController *controller = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
    SLComposeViewControllerCompletionHandler myBlock =
    ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultCancelled)
        {
            NSLog(@"Cancelled");
        } else
        {
            NSLog(@"Done");
        }
        [controller dismissViewControllerAnimated:YES completion:nil];
    };
    controller.completionHandler =myBlock;
    [controller addImage:image];
    //Adding the Text to the facebook post value from iOS
    [controller setInitialText:@"Klippat iOS app"];
    //Adding the URL to the facebook post value from iOS
    [controller addURL:[NSURL URLWithString:selectedImageInfo.serverURL]];
    //Adding the Text to the facebook post value from iOS
    [self presentViewController:controller animated:YES completion:nil];
}

-(IBAction)facebookPost
{
    KPSlideShowCell *customCell=(KPSlideShowCell*)[self.tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
    
    NSLog(@"[FBSession activeSession] :- %hhd", FBSession.activeSession.isOpen);
    
    if (!FBSession.activeSession.isOpen) {
        NSLog(@"NOT fb login");
        [self shareOnFBWithImage:customCell.pictureView.image];
        return;
        
    } else
    {
        NSLog(@"selectedImageInfo.serverURL %@", (selectedImageInfo.serverURL) ? selectedImageInfo.serverURL : selectedImageInfo.file_url);
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@"Klippat iOS App" forKey:@"message"];
        [params setObject:(selectedImageInfo.serverURL) ? selectedImageInfo.serverURL : selectedImageInfo.file_url forKey:@"link"];
        [FBRequestConnection startForPostWithGraphPath:@"me/feed"
                                           graphObject:(FBGraphObject*)[NSDictionary dictionaryWithDictionary:params]
                                     completionHandler:
         ^(FBRequestConnection *connection, id result, NSError *error) {
             NSLog(@"FBRequestConnection :- %@", error);
             if (!error) {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"Successfully shared on Facebook."
                                            delegate:self
                                   cancelButtonTitle:@"Ok!"
                                   otherButtonTitles:nil] show];
             } else {
                 [[[UIAlertView alloc] initWithTitle:nil
                                             message:@"Failed to share on Facebook."
                                            delegate:self
                                   cancelButtonTitle:@"Ok!"
                                   otherButtonTitles:nil] show];
                 //[self shareOnFBWithImage:customCell.pictureView.image];
             }
         }
         ];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)shareButtonClicked {
    
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter", nil];
    [actionSheet showInView:self.view];
    //actionSheet.tag=1;
}

-(void)addTitleViewAndBottomView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"back_blue"];
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 41)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setTag:7348];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 11, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    KPSonngCategoryInfo *info=[self.imageArray objectAtIndex:self.selectedIndex];
    if (![[info.type lowercaseString] isEqualToString:@"notes" ])
    {
        [titleLbl setText:@"PICTURES"];
        self.viewerType=KpViewerImageType;
    }
    else
    {
        [titleLbl setText:@"QUOTES"];
        self.viewerType=KPViewerQoutesType;
    }
    [titleLbl setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:TextColor];
    [topStripImgView addSubview:titleLbl];
    [titleLbl setTag:7349];
    [self.view addSubview:topStripImgView];
    
    //Bottom view
    UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, 320, 49)];
    [footerView setBackgroundColor:[UIColor colorWithRed:64/255.0 green:70/255.0 blue:67/255.0 alpha:1.0]];
    //[footerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"btg"]]];
    [footerView setBackgroundColor:[UIColor colorWithRed:0.0/255 green:-0.0/255 blue:25.0/255 alpha:1.0]];
    
    UIImage *block_icon=[UIImage imageNamed:@"lineThree"];
    UIImage *fave_icon=[UIImage imageNamed:@"grayFav"];
    UIImage *download_icon=[UIImage imageNamed:@"download1"];
    UIImage *share_icon=[UIImage imageNamed:@"shareIcon"];
    UIImage * klips = [UIImage imageNamed:@"klip5"];
    UIImage * fullScreen = [UIImage imageNamed:@"zoomIn"];
    UIImage * playlist =[UIImage imageNamed:@"playlistIcon"];

    
    
    float favWidth;
    float downloadWidth;
    float shareListWidth;
    float x=0,y=0;
    if ([[KPSingletonClass sharedObject].viaFavourites isEqualToString:@"Favourites"]) {
        
        favWidth = self.view.frame.size.width + 80;
        downloadWidth = 148;
        shareListWidth = 198;
        x=13;
    } else if ([[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"]) {
        
        favWidth = self.view.frame.size.width +180;;
        downloadWidth = self.view.frame.size.width +180;
        shareListWidth = 175;
        x=45;
        y=2;
    }else if ([[KPSingletonClass sharedObject].viaPlayList isEqualToString:@"PlayList"]) {
        
        favWidth = 106;
        downloadWidth = 154;
        shareListWidth = 194;
        x=13;
    }else {
        
        favWidth = 128;
        downloadWidth = 173;
        shareListWidth = 213;
    }
    [footerView setTag:9876];
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(10, 12, block_icon.size.width, block_icon.size.height) title:@"" BGImage:block_icon tag:8889]];//07+x
    
     [footerView addSubview:[self addbuttonWithFrame:CGRectMake(53, 17, fave_icon.size.width, fave_icon.size.height) title:@"" BGImage:fave_icon tag:8888]];//46
    
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(65+fave_icon.size.width, 8, playlist.size.width, playlist.size.height) title:@"" BGImage:playlist tag:9000]];//Added
    UIButton *playlistBtn=(UIButton*)[footerView viewWithTag:9000];
    [playlistBtn setEnabled:NO];
    
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(145, 7, klips.size.width, klips.size.height) title:@"" BGImage:klips tag:9003]];//favWidth-46

    
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(209, 14, download_icon.size.width, download_icon.size.height) title:@"" BGImage:download_icon tag:8891]];//213
   
   // [footerView addSubview:[self addbuttonWithFrame:CGRectMake(downloadWidth-58, 13+y, fave_icon.size.width, fave_icon.size.height) title:@"" BGImage:klips tag:9001]];
    
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(53+fave_icon.size.width+130+klips.size.width+download_icon.size.width, 15, share_icon.size.width, share_icon.size.height) title:@"" BGImage:share_icon tag:8892]];//247

    
    
    [footerView addSubview:[self addbuttonWithFrame:CGRectMake(53+fave_icon.size.width+150+klips.size.width+share_icon.size.width+download_icon.size.width, 13, fullScreen.size.width, fullScreen.size.height) title:@"" BGImage:fullScreen tag:9002]];//shareListWidth+72
    
    [self.view addSubview:footerView];
}


-(UIButton*)addbuttonWithFrame:(CGRect)frame title:(NSString*)title BGImage:(UIImage*)image tag:(int)tagValue
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTag:tagValue];
    [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:25]];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    if (tagValue==8888)
    {
        KPSonngCategoryInfo *imageInfo=[self.imageArray objectAtIndex:self.selectedIndex];
        
        [button setBackgroundImage:[UIImage imageNamed:@"redFav"] forState:UIControlStateSelected];
        [button setSelected:[imageInfo.favourite_status isEqualToString:@"1"]];
    }
    [button addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchDown];
    return button;
}

-(void)addImageViewToPlayWithFrame:(CGRect)frame tag:(int)tag image:(NSString*)image
{
    UIView *playerView=[[UIView alloc]initWithFrame:frame];
    [playerView setBackgroundColor:[UIColor colorWithRed:44.0/255 green:47.0/255 blue:48.0/255 alpha:1.0]];
    [playerView setAlpha:0.9];

    [playerView.layer setBorderColor:[UIColor blackColor].CGColor];
//    self.tableview=[[UITableView alloc]initWithFrame:CGRectMake((deviceFactor==1)?-12.5:14, (deviceFactor==1)?49.5:6, 345*deviceFactor, 320)];
//    [self.tableview setScrollEnabled:NO];
//    [self.tableview setTransform:CGAffineTransformMakeRotation(-M_PI*0.5 )];
//    [self.tableview setDelegate:(id)self];
//    [self.tableview setDataSource:(id)self];
//    [self.tableview setBackgroundColor:[UIColor clearColor]];
//    [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    [self.tableview setUserInteractionEnabled:YES];
//    [self.tableview setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    [playerView addSubview:self.tableview];
    
    self.swipeView =[[SwipeView alloc]init];
//    [self.swipeView setFrame:CGRectMake((deviceFactor==1)?-12.5:14, (deviceFactor==1)?49.5:6, 345*deviceFactor, 320)];
    [self.swipeView setFrame:CGRectMake(0, IS_IPHONE_5?45:40, self.view.frame.size.width, IS_IPHONE_5?330:250)];
    [playerView addSubview:self.swipeView];
    [self.swipeView setDelegate:self];
    [self.swipeView setDataSource:self];
    [self.swipeView setUserInteractionEnabled:YES];
    [self.swipeView scrollToItemAtIndex:self.selectedIndex duration:0.1];

    UIImage *nextImage=[UIImage imageNamed:@"arw_next"];
    UIView *rightArrowSuperView=[[UIView alloc]initWithFrame:CGRectMake(258, IS_IPHONE_5?162:132, nextImage.size.width+13, nextImage.size.width+15)];
    [rightArrowSuperView setBackgroundColor:[UIColor clearColor
                                             ]];
    [rightArrowSuperView setUserInteractionEnabled:YES];
    [rightArrowSuperView setTag:98];
//    UISwipeGestureRecognizer * rightTap = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(tapButtonAction:)];
//    [rightTap setDirection:UISwipeGestureRecognizerDirectionRight];
//    
//    //UITapGestureRecognizer *rightTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapButtonAction:)];
//
//    [self.tableview addGestureRecognizer:rightTap];
    
    rightArrowButton=[UIButton buttonWithType:UIButtonTypeCustom];
//    [rightArrowButton setFrame:CGRectMake(262, IS_IPHONE_5?165:135, nextImage.size.width+5 , nextImage.size.height+5)];
    [rightArrowButton setFrame:CGRectMake(4, 3, nextImage.size.width+5 , nextImage.size.height+5)];

    [rightArrowButton setTag:98];
    [rightArrowButton setBackgroundImage:nextImage forState:UIControlStateNormal];
    [rightArrowButton addTarget:self action:@selector(leftOrRightButtonClicked:) forControlEvents:UIControlEventTouchDown];
    UIImage *prevImage=[UIImage imageNamed:@"arw_prev"];
    UIView *leftArrowSuperView=[[UIView alloc]initWithFrame:CGRectMake(9, IS_IPHONE_5?162:132, nextImage.size.width+13 , nextImage.size.height+11)];
    [leftArrowSuperView setUserInteractionEnabled:YES];
    [leftArrowSuperView setTag:99];
    UITapGestureRecognizer *leftTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapButtonAction:)];
    [leftArrowSuperView addGestureRecognizer:leftTap];
    [leftArrowSuperView setBackgroundColor:[UIColor clearColor]];
    leftArrowButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [leftArrowButton setFrame:CGRectMake(0, 3, nextImage.size.width+5 , nextImage.size.height+5)];

    [leftArrowButton setTag:99];
    [leftArrowButton setBackgroundImage:prevImage forState:UIControlStateNormal];
    [leftArrowButton addTarget:self action:@selector(leftOrRightButtonClicked:) forControlEvents:UIControlEventTouchDown];
    [playerView addSubview:leftArrowSuperView];
    
    [playerView addSubview:rightArrowSuperView];
    
    [playerView bringSubviewToFront:leftArrowSuperView];
    [playerView bringSubviewToFront:rightArrowSuperView];
    if (!self.imageArray || !self.imageArray.count>0 ||self.imageArray.count==1) {
        [leftArrowSuperView setHidden:YES];
        [rightArrowSuperView setHidden:YES];
    }
    else if (self.selectedIndex==0)
    {
        [leftArrowSuperView setHidden:YES];
    }
    else if(self.selectedIndex==self.imageArray.count-1)
    {
        [rightArrowSuperView setHidden:YES];
    }
    [self.view addSubview:playerView];
}

-(void)swipeLeftButtonAction:(UISwipeGestureRecognizer *)gesture
{
    NSLog(@"Swipe Left");
    [self leftOrRightButtonClicked:rightArrowButton];
    UILabel * label = (UILabel *)[self.view viewWithTag:5656];
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    [label setText: categoryInfo.file_name];
}

-(void)swipeRightButtonAction:(UISwipeGestureRecognizer *)gesture
{
    NSLog(@"Swipe Right");
    [self leftOrRightButtonClicked:leftArrowButton];
    UILabel * label = (UILabel *)[self.view viewWithTag:5656];
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    [label setText: categoryInfo.file_name];
}

-(void)tapButtonAction:(UISwipeGestureRecognizer*)gesture
{
    NSLog(@"Swipe Gesture");
    if (gesture.view.tag==98)
    {
        [self leftOrRightButtonClicked:rightArrowButton];
    }
    else
    {
        [self leftOrRightButtonClicked:leftArrowButton];

    }
}
-(IBAction)homeButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//if ([[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
-(IBAction)leftOrRightButtonClicked:(UIButton*)sender
{
    UIImageView *topStrip=(UIImageView*)[self.view viewWithTag:7348];
    UILabel *titleLbl=(UILabel*)[topStrip viewWithTag:7349];
    
    if (sender.tag==99) {
        if (rightArrowButton.superview.hidden) {
            [rightArrowButton.superview setHidden:NO];
        }
        NSLog(@"left left");
        if (self.selectedIndex==0) {

            return;
        }
        else
            self.selectedIndex--;
        if (self.selectedIndex==0) {
            [leftArrowButton.superview setHidden:YES];

        }
        if ([[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
        {
            KPSonngCategoryInfo *info=[self.imageArray objectAtIndex:self.selectedIndex];
            if (![[info.type lowercaseString] isEqualToString:@"notes" ])
            {
                [titleLbl setText:@"PICTURES"];
                self.viewerType=KpViewerImageType;
            }
            else
            {
                [titleLbl setText:@"QUOTES"];
                self.viewerType=KPViewerQoutesType;
                
            }

        }
        [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else
    {
        if (leftArrowButton.superview.hidden)
        {
            [leftArrowButton.superview setHidden:NO];
        }
        if (self.selectedIndex==self.imageArray.count-1)
        {
            return;
        }
        else
            self.selectedIndex++;
        if (self.selectedIndex==self.imageArray.count-1)
        {
            [rightArrowButton.superview setHidden:YES];
            
        }
        if ([[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
        {
            KPSonngCategoryInfo *info=[self.imageArray objectAtIndex:self.selectedIndex];
            if (![[info.type lowercaseString] isEqualToString:@"notes" ]) {
                [titleLbl setText:@"PICTURES"];
                self.viewerType=KpViewerImageType;
            }
            else
            {
                [titleLbl setText:@"QUOTES"];
                self.viewerType=KPViewerQoutesType;
                
            }
            
        }
        [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        NSLog(@"right right ");
    }
}

-(void)tapToZoomOutImage:(UIGestureRecognizer*)gesture
{
    UIView *footerView=(UIView*)[self.view viewWithTag:9876];
    UIButton *zoom_outBtn=(UIButton*)[footerView viewWithTag:9002];
    [self buttonClickAction:zoom_outBtn];
    
}

-(IBAction)buttonClickAction:(UIButton *)sender
{
    if (sender.tag==8888) {
        NSLog(@"favourite  %ld",(long)sender.tag);
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"])
        {
            [self displayAlertView];
            return;
        }
        [self addToFavourites];
    }
    else if (sender.tag==8889) {
        NSLog(@"back  %ld",(long)sender.tag);
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (sender.tag==8890) {
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        [self addToPlayList];
    } else if (sender.tag==8891) {
        NSLog(@"download  %ld",(long)sender.tag);
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
        //[sender setEnabled:NO];
        showActivity( self.navigationController.view);
        [self downLoadIMgesInBackground:categoryInfo.file_url with:sender];
        //showActivity([self.navigationController view]);
    } else if (sender.tag==8892) {
        
        NSLog(@"share  %ld",(long)sender.tag);
        if ([[KPSingletonClass sharedObject].userID isEqualToString:@"0"]) {
            [self displayAlertView];
            return;
        }
        [self shareButtonClicked];
    } else if (sender.tag==8893) {
        
        NSLog(@"previous  %ld",(long)sender.tag);
        if (self.selectedIndex==0) {
            return;
        }
        else
            self.selectedIndex--;
        [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    } else if (sender.tag==8894) {
        NSLog(@"next  %ld",(long)sender.tag);
        if (self.selectedIndex==self.imageArray.count-1) {
            return;
        }
        else
            self.selectedIndex++;
        [self.tableview scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (sender.tag == 9002)
    {
        UIView *footerView=(UIView*)[self.view viewWithTag:9876];
        UILabel * label = (UILabel *)[self.view viewWithTag:5656];
        UIImageView *topStrip=(UIImageView*)[self.view viewWithTag:7348];


        sender.selected=!sender.selected;
        if (sender.selected) {
            
            [footerView setHidden:YES];
            [topStrip setHidden:YES];
            [label setHidden:YES];

            //[self.view bringSubviewToFront:self.swipeView.superview];
            [self.swipeView.superview setFrame:self.view.bounds];
            [self.swipeView setFrame:self.view.bounds];

        }
        else
        {
            [footerView setHidden:NO];
            [label setHidden:NO];
            [topStrip setHidden:NO];

            [self.swipeView.superview setFrame:IS_iPHONE_5? CGRectMake(0, 106, 320, 420*deviceFactor):CGRectMake(0, 98, 320, 330)];
        [self.swipeView setFrame:CGRectMake(0, 45, self.view.frame.size.width, 330)];
        }
    }
    else if (sender.tag == 9003)
    {
        [[KPSingletonClass sharedObject] setViaDownloads:@""];
        [[KPSingletonClass sharedObject] setViaFavourites:@""];
        [[KPSingletonClass sharedObject] setViaPlayList:@""];
        
        //[[self getPlayerView] removeFromSuperview];
        KPHomeViewController *vc;
        for(id controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[KPHomeViewController class]])
            {
                vc=(KPHomeViewController*)controller;
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        if (!vc)
        {
            [self.navigationController pushViewController:vc animated:YES];
        }


    }
    else if (sender.tag == 9000)
    {
        
    }

    NSLog(@"Button clicked with tag %ld",(long)sender.tag);
}



-(void)displayAlertView {
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Not Login!" message:@"Please login first." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Login"       , nil];
    alertView.tag=34343;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==34343 && buttonIndex==1) {
        KPAppDelegate *app =(KPAppDelegate *)[UIApplication sharedApplication].delegate;
        [app.tabListController logOutButtonClicked];
        //[self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)downLoadIMgesInBackground:(NSString *)urlString with:(UIButton*)sender
{    
    NSLog(@"decoder.original_url %@", urlString);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSString *filename = [URL lastPathComponent];
    
    [documentsDirectoryURL URLByAppendingPathComponent:filename];
    //BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectoryURL path]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSString *filename = [URL lastPathComponent];
        NSLog(@"jksdfhjksahfhsda %@ and %@", [response suggestedFilename], filename);
        return [documentsDirectoryURL URLByAppendingPathComponent:filename];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"Image File downloaded to: %@", filePath);
        if (!error) {
            KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
            NSLog(@"sonngcategory info:: %@",categoryInfo);
            KPDownLoadInfo *downLoadInfo = [[KPDownLoadInfo alloc] init];
            [downLoadInfo setFile_duration:categoryInfo.file_duration];
            [downLoadInfo setFile_id:categoryInfo.file_id];
            [downLoadInfo setCategory_id:categoryInfo.category_id];
            [downLoadInfo setFile_name:categoryInfo.file_name];
            [downLoadInfo setFile_url:categoryInfo.file_url];
            [downLoadInfo setPerson_name:categoryInfo.person_name];
            [downLoadInfo setLocalPath:[filePath path]];
            [downLoadInfo setIs_favourite:categoryInfo.favourite_status];
            [downLoadInfo setDownloadtype: getCategoryType(KPImage)];
            [downLoadInfo setType:categoryInfo.type];
            [[self getCoreDataInitializer] addDownLoadInformation:downLoadInfo];
            dispatch_async(dispatch_get_main_queue(), ^{
                dismissHUD(self.navigationController.view);
                NSLog(@"image file downloaded succesfully ");
                showAlertView(@"", @"Downloaded successfully.");
            });
 
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                dismissHUD(self.navigationController.view);
                NSLog(@"image file downloaded succesfully ");
                showAlertView(@"", @"Network error.");
            });

        }
            }];
    [downloadTask resume];
    
}

-(KPCoreDataInitializer *)getCoreDataInitializer {
    
    KPCoreDataInitializer *dataInitilizer = [KPCoreDataInitializer sharedMySingleton];
    [dataInitilizer initializeTheCoreDataModelClasses];
    return dataInitilizer;
}

#pragma mark To add in playlist
-(void)addToPlayList
{
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    showActivity(self.navigationController.view);
    __weak typeof(self) weakSelf = self;
    [self addFileToPlayListWith:categoryInfo.file_id categoryId:categoryInfo.category_id playlistId:@"32" callBack:^(id result,NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(weakSelf.navigationController.view);
            if (result==nil) {
                showAlertView(nil, @"Network error.");
            } else {
                NSDictionary *response =[[result valueForKey:@"response"]valueForKey:@"playlist"];
                showAlertView(nil,[NSString stringWithFormat:@"%@.",[response valueForKey:@"message"]]);
            }
        });
        
        NSLog(@"addfileto playlist %@",result);
    }];
}

-(void)updatingFavouriteStatusDBWithInfo:(KPSonngCategoryInfo*)categoryInfo
{
    NSLog(@"is favvv %@",self.navigationController.viewControllers);
//    if (![[KPSingletonClass sharedObject].viaDownloads isEqualToString:@"Downloads"])
//    {
        KPSearchDataINfo *info=[KPSearchDataINfo new];
        info.file_duration=categoryInfo.file_duration;
        info.file_id=categoryInfo.file_id;
        info.file_name=categoryInfo.file_name;
        info.file_url=categoryInfo.file_url;
        info.type=categoryInfo.type;
        info.category_id=categoryInfo.category_id;
        info.person_name=categoryInfo.person_name;
        info.is_favourite=categoryInfo.favourite_status;
        BOOL yes=[[self getCoreDataInitializer] updateFavStatusForSearch:info];
        NSLog(@"is_favourite %hhd",yes);
  //  }

}
#pragma mark To add in Favourites
-(void)addToFavourites
{
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:self.selectedIndex];
    UIView *bottomView=(UIView *)[self.view viewWithTag:9876];
    UIButton *favBtn=(UIButton*)[bottomView viewWithTag:8888];
    
    if ([categoryInfo.favourite_status isEqualToString:@"1"])
    {
        showAlertView(nil, @"File already added.");
        return;
    }
    showActivity(self.navigationController.view);
    __weak typeof(self) weakSelf = self;
    [self addFileToFavouritesWithFileId:categoryInfo.file_id categoryId:categoryInfo.category_id callBack:^(id result,NSError *error) {
        NSLog(@"fabbbbbvvv result %@",result);
       // dispatch_async(dispatch_get_main_queue(), ^{
            dismissHUD(weakSelf.navigationController.view);
            if (result==nil) {
                showAlertView(nil, @"Network error.");
            } else {
                
                NSDictionary *response =[result valueForKey:@"response"];
                NSString *msg;
                if ([[response valueForKey:@"success"]isEqualToString:@"true"]) {
                    msg=@"Successfully added";
                    if ([[weakSelf.navigationController.viewControllers objectAtIndex:weakSelf.navigationController.viewControllers.count-2]isKindOfClass:[KPSearchViewController class]]) {
                        NSLog(@"Iscomingfromsearch");
                        [weakSelf updatingFavouriteStatusDBWithInfo:categoryInfo];
                    }
                    [categoryInfo setFavourite_status:@"1"];
                    [favBtn setSelected:YES];
                    @try {
                        [weakSelf.imageArray replaceObjectAtIndex:weakSelf.selectedIndex withObject:categoryInfo];
                    }
                    @catch (NSException *exception) {
                        
                    }
                    @finally {
                        
                    }
                    
                    //dispatch_async(dispatch_get_main_queue(), ^{
                   // });
                } else
                    msg=[response valueForKey:@"error"];
                showAlertView(nil,[NSString stringWithFormat:@"%@.",msg]);
            }
       //});
    }];
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 319;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"viewControllers %@",self.navigationController.viewControllers);
    NSLog(@"%ld row selected",(long)indexPath.row+1);
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.imageArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPSlideShowCell *cell=(KPSlideShowCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil)
    {
        cell=[[KPSlideShowCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    self.selectedIndex=indexPath.row;
    selectedImageInfo = [self.imageArray objectAtIndex:indexPath.row];
    [cell.pictureView setTransform:CGAffineTransformMakeRotation(M_PI * 0.5)];
    [cell.pictureView setFrame:CGRectMake(0, 0, 345*deviceFactor, 319)];
    [cell.pictureView setUserInteractionEnabled:YES];
    
    UISwipeGestureRecognizer * leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftButtonAction:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [cell.pictureView addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer * rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightButtonAction:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [cell.pictureView addGestureRecognizer:rightSwipe];
    
    
    

    UIView *bottomView=(UIView *)[self.view viewWithTag:9876];
    UIButton *favBtn=(UIButton*)[bottomView viewWithTag:8888];
    [favBtn setSelected:[selectedImageInfo.favourite_status isEqualToString:@"1"]];
    UIImage *img = [UIImage imageWithContentsOfFile:selectedImageInfo.file_url];
    if (img)
    {
        [cell.pictureView setImage:img];
    }
    else
        [cell.pictureView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",selectedImageInfo.file_url]]];
    return cell;
}



#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return [self.imageArray count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UIImageView *imageView=nil;
    

    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIView alloc] initWithFrame:self.swipeView.bounds];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        
        imageView=[[UIImageView alloc]initWithFrame:view.bounds];
        imageView.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [imageView setBackgroundColor:[UIColor clearColor]];
        [imageView setTag:84];
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:imageView];
        [imageView setUserInteractionEnabled:YES];
        UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapToZoomOutImage:)];
        [singleTap setNumberOfTapsRequired:2];
        [imageView addGestureRecognizer:singleTap];
        [singleTap setDelegate:(id)self];

        //[view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        imageView=(UIImageView*)[view viewWithTag:84];
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set background color
    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    view.backgroundColor = [UIColor colorWithRed:red
                                           green:green
                                            blue:blue
                                           alpha:1.0];
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = _items[index] ;
    selectedImageInfo = [self.imageArray objectAtIndex:index];
        UIImage *img = [UIImage imageWithContentsOfFile:selectedImageInfo.file_url];
        if (img)
        {
            [imageView setImage:img];
        }
        else
            [imageView setImageWithURL:[NSURL URLWithString:[NSString   stringWithFormat:@"%@",selectedImageInfo.file_url]]];
    swipedIndex=index;
    
    return view;
}
-(void)swipeViewCurrentItemIndexDidChange:(SwipeView *)swipeView
{
    NSLog(@"swipeViewCurrentItemIndexDidChange ");
    UILabel * titleLbl = (UILabel *)[self.view viewWithTag:5656];
    KPSonngCategoryInfo *categoryInfo=[self.imageArray objectAtIndex:swipedIndex];
    [titleLbl setText: [categoryInfo.file_name capitalizedString]];

}
- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}

@end
