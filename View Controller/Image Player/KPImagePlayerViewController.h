//
//  KPImagePlayerViewController.h
//  Klippat
//
//  Created by Ravi kumar on 13/06/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSonngCategoryInfo.h"
#import "KPCommonViewController.h"
#import <Social/Social.h>
#import "SwipeView.h"


typedef enum
{
    KPViewerQoutesType=2,
    KpViewerImageType=3,
}KPViewerType;
@interface KPImagePlayerViewController : KPCommonViewController
@property(nonatomic)KPViewerType viewerType;
@property(nonatomic,strong)NSMutableArray *imageArray;
@property(nonatomic)NSInteger selectedIndex;


@property (nonatomic, strong) KPSonngCategoryInfo *categoryInfo;

@end
