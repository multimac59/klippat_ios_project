//
//  KPSlideShowCell.h
//  Klippat
//
//  Created by Ravi kumar on 11/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPSlideShowCell : UITableViewCell
@property(nonatomic,strong)UIImageView *pictureView;
@end
