//
//  KPSlideShowCell.m
//  Klippat
//
//  Created by Ravi kumar on 11/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSlideShowCell.h"

@implementation KPSlideShowCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.pictureView=[[UIImageView alloc]init ];//]WithFrame:CGRectMake(0, 0, <#CGFloat width#>, <#CGFloat height#>)
        [self addSubview:self.pictureView];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
