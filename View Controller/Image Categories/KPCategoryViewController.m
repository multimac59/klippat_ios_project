//
//  KPImageCategoryViewController.m
//  Klippat
//
//  Created by Ravi kumar on 18/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPCategoryViewController.h"
#import "KPGridGalleryViewController.h"
@interface KPCategoryViewController ()

@end

@implementation KPCategoryViewController
@synthesize tableView=_tableView;
@synthesize categoriesArray=_categoriesArray;
-(UITableView*)tableView
{
    if (!_tableView) {
        _tableView=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    }
    return _tableView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(NSMutableArray*)imageArray
{
    if (!_categoriesArray) {
        _categoriesArray=[[NSMutableArray alloc]init];
        for (int i=0; i<14; i++) {
            [_categoriesArray addObject:@"Categaries 1"];
        }
    }
    return _categoriesArray;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getCategoriesFromServer];
    [self.tableView setFrame:CGRectMake(0, (deviceFactor==1)?125:122, self.view.frame.size.width-10, 378*deviceFactor)];
    [self.tableView setDataSource:(id)self];
    [self.tableView setDelegate:(id)self];
    if ([UIDevice currentDevice].systemVersion.floatValue>=7) {
        self.tableView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:self.tableView];
    [self addLocalNavigationBar:CGRectMake(self.view.frame.origin.x, self.view.frame.size.height - 44, 320, 44)];
    [self addCustomSearchBar];
    //[self addGridViewWithImages];
    // Do any additional setup after loading the view.
}
-(void)getCategoriesFromServer
{
    NSMutableDictionary *infoDict=[NSMutableDictionary new];
    [infoDict setValue:@"show_category_list" forKeyPath:@"task"];
    [infoDict setValue:@"english" forKeyPath:@"language_type"];
    [infoDict setValue:@"movies" forKeyPath:@"type"];
[[KPParser new]serviceRequestWithInfo:infoDict serviceType:KPGetCategoryListType responseBlock:^(id response, NSError *error) {
    NSLog(@"getCategoriesFromServer response--%@",response);
}];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)addLocalNavigationBar:(CGRect)frame {
    [self addTopView];
    UIImage *navi_Image = [UIImage imageNamed:@"btg"];
    UINavigationBar *viewNavigation = [[UINavigationBar alloc] init];
    viewNavigation.frame = frame;
    [viewNavigation setBackgroundImage:navi_Image forBarMetrics:UIBarMetricsDefault];
    
    UIImage *arrow = [UIImage imageNamed:@"arow.png"];
    UIImage *arrow1 = [UIImage imageNamed:@""];
    UIButton *arrowButton = [[UIButton alloc] init];
    [arrowButton setBackgroundImage:arrow forState:UIControlStateNormal];
    [arrowButton setBackgroundImage:arrow1 forState:UIControlStateHighlighted];
    arrowButton.frame = CGRectMake(viewNavigation.frame.origin.x + 10, viewNavigation.frame.size.height/2-15, arrow.size.width , arrow.size.height-5 );
    [arrowButton addTarget:self action:@selector(arrowAction:) forControlEvents:UIControlEventTouchUpInside];
    [viewNavigation addSubview:arrowButton];
    [self.view addSubview:viewNavigation];
}

-(IBAction)arrowAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)addTopView
{
    UIImage *stripBg=[UIImage imageNamed:@"tpbg"];
    UIImage *homeImage=[UIImage imageNamed:@"hos.png.png"];
    NSString *titleString;
    if (self.categoryType==KPCategoryTypeImage) {
        titleString=@"PICTURES";
    }
    else  if (self.categoryType==KPCategoryTypeNotes) {
        titleString=@"NOTES";
    }
    else  if (self.categoryType==KPCategoryTypeVideo) {
        titleString=@"MOVIES CLIPS";
    }
    else
    {
        titleString=@"MUSIC CLIPS";

    }
    NSLog(@"home imaage size %f",homeImage.size.height);
    UIImageView *topStripImgView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 28, 320, 42)];
    [topStripImgView setImage:stripBg];
    [topStripImgView setUserInteractionEnabled:YES];
    UIButton * button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(20, 8, homeImage.size.width, homeImage.size.height)];
    [topStripImgView addSubview:button];
    [button setBackgroundImage:homeImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeButtonAction:) forControlEvents:UIControlEventTouchDown];
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(65, 15, 190, 13.5)];
    [titleLbl setText:titleString];
    [titleLbl setTextAlignment:NSTextAlignmentCenter];
    [titleLbl setTextColor:[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0]];
    [topStripImgView addSubview:titleLbl];
    
    [self.view addSubview:topStripImgView];
}
-(void)addCustomSearchBar
{
    UITextField *search_txtF=[[UITextField alloc]initWithFrame:CGRectMake(25, 85,270, 30)];
    [search_txtF setDelegate:(id)self];
    UIColor *color =[UIColor colorWithRed:11.0/255 green:63.0/255 blue:158.0/255 alpha:1.0];
    search_txtF.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color,NSFontAttributeName:[UIFont italicSystemFontOfSize:12.0]}];
    UIImage *searchImage=[UIImage imageNamed:@"search"];
    [search_txtF setFont:[UIFont italicSystemFontOfSize:12.0]];
    UIView *lftView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 25)];
    [search_txtF setLeftViewMode:UITextFieldViewModeAlways];
    [search_txtF setLeftView:lftView];
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 30)];
    UIImageView *searchIcon=[[UIImageView alloc]initWithFrame:CGRectMake(0, 6, searchImage.size.width, searchImage.size.height)];
    [searchIcon setImage:searchImage];
    [rightView addSubview:searchIcon];
    [search_txtF setRightView:rightView];
    [search_txtF setRightViewMode:UITextFieldViewModeAlways];
    [search_txtF setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:search_txtF];
}
-(IBAction)homeButtonAction :(id)sender
{
    NSLog(@"homeButtonAction");
    [self navigateToHomeScreen];
}
-(void)navigateToHomeScreen
{
[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableView Delegate And DataSorce
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld row selected",(long)indexPath.row+1);
    KPCategoryCell *cell=(KPCategoryCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (self.categoryType==KPCategoryTypeImage) {
     KPGridGalleryViewController *obj=[KPGridGalleryViewController new];
    obj.categoryName=cell.titleLable.text;
    [self.navigationController pushViewController:obj animated:YES];
    }
    else if (self.categoryType==KPCategoryTypeMusic) {
        KPSongListViewController *vc=[[KPSongListViewController alloc]init];
        vc.categoryTitleText=cell.titleLable.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.categoryType==KPCategoryTypeVideo) {
        KPSongListViewController *vc=[[KPSongListViewController alloc]init];
        vc.categoryTitleText=cell.titleLable.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else  if (self.categoryType==KPCategoryTypeNotes) {
        KPGridGalleryViewController *obj=[KPGridGalleryViewController new];
        obj.categoryName=cell.titleLable.text;
        [self.navigationController pushViewController:obj animated:YES];    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"CellIdentifier";
    KPCategoryCell *cell=(KPCategoryCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        cell=[[KPCategoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    // NSDictionary *tempDict=[self.categoriesArray objectAtIndex:indexPath.row];
    [cell.textLabel setFrame:CGRectMake(0, 8, 150, 14)];
    // [cell.titleLable setText:[tempDict valueForKey:@"title"]];
    [cell.titleLable setText:[NSString stringWithFormat:@"Categories %d",indexPath.row+1]];
    return cell;
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end