//
//  KPParser.m
//  Klippat
//
//  Created by Ravi kumar on 24/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPParser.h"
#import "SBJsonParser.h"
#import "SBJson.h"
#define baseURL (@"http://www.mobappssolutions.com/klippat/index.php")
// //http://www.funkiorange.com/klippat/index.php
@implementation KPParser
-(void)serviceRequestWithInfo:(NSMutableDictionary*)infoDictionay serviceType:(ServiceType)type responseBlock:(CompletionBlock)responseBlock
{
    NSLog(@"infoDictionayinfoDictionay %@",infoDictionay);
    self.responseBlock=responseBlock;
    switch (type)
    {
        case KPSignUpServiceType:
            [self signUpRequestWithInfo:infoDictionay];
            break;
        case KPSignInNormalType:
            [self normalLoginWithInfo:infoDictionay];
            break;
        case KPForgotPasswordType:
            [self forgotPasswordWithInfo:infoDictionay];
            break;
        case KPCreatePlayListType:
            [self createPlayListWithInfo:infoDictionay];
            break;
        case KPSocialLoginType:
            [self socialLoginWithInfo:infoDictionay];
            break;
        case KPAddFileToPlaylistType:
            [self addFileToPlayListWithInfo:infoDictionay];
            break;

        case KPGetPlayListType:
            [self getPlayListWithInfo:infoDictionay];
            break;
            
        case KPGetCategoryListType:
            [self getCategoryListWithInfo:infoDictionay];
            break;
        case KPGetFavoriteType:
            [self getUserFavouriteCategoriesWithInfo:infoDictionay];
            break;
        case KPGetFavoriteDetailType:
            [self getFavouriteFilesWithInfo:infoDictionay];
            break;
            
        case KPGetCategoryDetailType:
            [self getCategoryDetailWithInfo:infoDictionay];
            break;
            
        case KPAddFileToPFavouriteType:
            [self saveFileToFavoriteWithInfo:infoDictionay];
            break;
        case KPGetUserPlaylistFile:
            [self getFavoriteUserPlayListFileWithInfo:infoDictionay];
            break;
        case KPGetPrivacyPolicyType:
            [self getPrivacyPolicyWithInfo:infoDictionay];
            break;
        case KPGetTermsConditionsType:
            [self GetTermsConditionsWithInfo:infoDictionay];
            break;
            
        case KPSearchServiceType:
            [self searchRequestWithInfo:infoDictionay];
            break;

        case KPChangeNotification:
            [self ChangeNotificationWithInfo:infoDictionay];
            break;

        case KPRandomFilePlayType:
            [self getRandomPlayListWithInfo:infoDictionay];
            break;
            
        case KPWhatsNewType:
            [self getWhatsNewWithInfo:infoDictionay];
            break;
        default:
            break;
    }
}

#pragma mark Normal user sign up
-(void)searchRequestWithInfo:(NSMutableDictionary*)infoDictionary
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?&task=%@&file_name=%@&language_type=%@&user_id=%@",baseURL,[infoDictionary valueForKey:@"task"],[infoDictionary valueForKey:@"file_name"], [infoDictionary valueForKey:@"language_type"],[infoDictionary valueForKey:@"user_id"]]]];
    [request setHTTPMethod:@"Post"];
    NSLog(@"base url %@ and %@ and request url %@",baseURL,infoDictionary,request.URL);
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
        NSLog(@"network errrrrrrr");
    }
    
}

#pragma mark Normal user sign up
-(void)signUpRequestWithInfo:(NSMutableDictionary*)infoDictionary
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?&task=%@&user_name=%@&email=%@&password=%@",baseURL,[infoDictionary valueForKey:@"task"],[infoDictionary valueForKey:@"user_name"],[infoDictionary valueForKey:@"email"],[infoDictionary valueForKey:@"password"]]]];
    [request setHTTPMethod:@"Post"];
    NSLog(@"base url %@ and %@ and request url %@",baseURL,infoDictionary,request.URL);
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
        NSLog(@"network errrrrrrr");
    }
    
}

#pragma mark Normal user Login
-(void)normalLoginWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?user_name=%@&password=%@&task=%@&login_type=%@",baseURL,[infoDict valueForKey:@"user_name"],[infoDict valueForKey:@"password"],[infoDict valueForKey:@"task"],[infoDict valueForKey:@"login_type"]]]];
    [request setHTTPMethod:@"Post"];
    NSLog(@"Normal login url %@",request.URL);
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
    
}

#pragma mark Forgot password 
-(void)forgotPasswordWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?email=%@&task=%@",baseURL,[infoDict valueForKey:@"email"],[infoDict valueForKey:@"task"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

#pragma mark Forgot password
-(void)createPlayListWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@&playlist_name=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"user_id"],[infoDict valueForKey:@"playlist_name"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    NSLog(@"url create playlist %@ and %@",request.URL,[NSString stringWithFormat:@"%@?task=%@&user_id=%@&playlist_name=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"user_id"],[infoDict valueForKey:@"playlist_name"]]);
    if (!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

#pragma mark Socail Login
-(void)socialLoginWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?user_name=%@&task=%@&social_user_id=%@&login_type=%@&fname=%@&lname=%@&email=%@&gender=%@&profile_image_url=%@&device_id=%@&device_token=%@",baseURL,[infoDict valueForKey:@"user_name"],[infoDict valueForKey:@"task"],[infoDict valueForKey:@"social_user_id"],[infoDict valueForKey:@"login_type"],[infoDict valueForKey:@"fname"],[infoDict valueForKey:@"lname"],[infoDict valueForKey:@"email"],[infoDict valueForKey:@"gender"],[infoDict valueForKey:@"profile_image_url"],[infoDict valueForKey:@"device_id"],[infoDict valueForKey:@"device_token"]]]];
    NSLog(@"fb login url %@",[NSString stringWithFormat:@"%@?user_name=%@&task=%@&social_user_id=%@&login_type=%@&fname=%@&lname=%@&email=%@&gender=%@&profile_image_url=%@&device_id=%@&device_token=%@",baseURL,[infoDict valueForKey:@"user_name"],[infoDict valueForKey:@"task"],[infoDict valueForKey:@"social_user_id"],[infoDict valueForKey:@"login_type"],[infoDict valueForKey:@"fname"],[infoDict valueForKey:@"lname"],[infoDict valueForKey:@"email"],[infoDict valueForKey:@"gender"],[infoDict valueForKey:@"profile_image_url"],[infoDict valueForKey:@"device_id"],[infoDict valueForKey:@"device_token"]]);
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
    
}
#pragma mark ADD Files To Playlist
-(void)addFileToPlayListWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@&user_playlist_id=%@&file_id=%@&category_id=%@&type=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"user_id"],[infoDict valueForKey:@"user_playlist_id"],[infoDict valueForKey:@"file_id"],[infoDict valueForKey:@"category_id"],[infoDict valueForKey:@"type"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    NSLog(@"addFileToPlayListWithInfo %@",request.URL);
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
 
}
#pragma mark To get playlist Contents
-(void)getPlayListWithInfo:(NSMutableDictionary*)infoDect
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@",baseURL,[infoDect valueForKey:@"task"],[infoDect valueForKey:@"user_id"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
     NSLog(@"getPlayListWithInfo %@",request.URL);
    if(!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
        
    }
    
}
/*
 * Lists of video Categories
 */
#pragma mark Get category list
-(void)getCategoryListWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&language_type=%@&type=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"language_type"],[infoDict valueForKey:@"type"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    NSLog(@"getCategoryListWithInfo %@",request.URL);
    if (!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
    }
}
/*
 my_favourite_save	task
 file_id
 user_id
 files_category_id*/
#pragma mark Save to favorites
-(void)saveFileToFavoriteWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString  stringWithFormat:@"%@?task=%@&file_id=%@&user_id=%@&category_id=%@",baseURL,[dataInfo valueForKey:@"task"],[dataInfo valueForKey:@"file_id"],[dataInfo valueForKey:@"user_id"],[dataInfo valueForKey:@"files_category_id"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    NSLog(@"getCategoryListWithInfo %@",request.URL);
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
}



#pragma mark To show user's favourites
-(void)getUserFavouriteCategoriesWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@",baseURL,[dataInfo valueForKey:@"task"],[dataInfo valueForKey:@"user_id"]]]];
    [request setHTTPMethod:@"Post"];
    NSLog(@"getFav url %@",request.URL);
    NSURLConnection *conntion=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!conntion) {
        NSError *err;
        self.responseBlock(nil,err);
    }
}
/*
 Function for show user favourite type	return_user_favourite_files	task
 user_id*/
-(void)getFavouriteFilesWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@&type=%@&language_type=%@",baseURL,[dataInfo valueForKey:@"task"],[dataInfo valueForKey:@"user_id"],[dataInfo valueForKey:@"type"],[dataInfo valueForKey:@"language_type"]]]];
    [request setHTTPMethod:@"Post"];
    NSLog(@"fav file with detail urrlll %@",[NSString stringWithFormat:@"%@?task=%@&user_id=%@&type=%@&language_type=%@",baseURL,[dataInfo valueForKey:@"task"],[dataInfo valueForKey:@"user_id"],[dataInfo valueForKey:@"type"],[dataInfo valueForKey:@"language_type"]]);
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if(!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

-(void)getFavoriteUserPlayListFileWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_playlist_id=%@&user_id=%@&language_type=%@",baseURL,[dataInfo valueForKey:@"task"],[dataInfo valueForKey:@"user_playlist_id"],[dataInfo valueForKey:@"user_id"],[dataInfo valueForKey:@"language_type"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if(!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
    }
    
}
/*
 Function for show  get privacy policy	get_privacy_policy	task
 */
-(void)getPrivacyPolicyWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@",baseURL,[dataInfo valueForKey:@"task"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

/*
 Function for show  g show terms conditions	get_terms_conditions	task
 */
-(void)GetTermsConditionsWithInfo:(NSMutableDictionary*)dataInfo
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@",baseURL,[dataInfo valueForKey:@"task"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

/*
 Function for show  g show terms conditions	change_notification	task
 */
-(void)ChangeNotificationWithInfo:(NSMutableDictionary*)infoData
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&user_id=%@&notification=%@",baseURL,[infoData valueForKey:@"task"],[infoData valueForKey:@"user_id"],[infoData valueForKey:@"notification"]]]];
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }
    
    
}

/*
 Function for send_random_file	task
 */
-(void)getRandomPlayListWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&language_type=%@&user_id=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"language_type"],[infoDict valueForKey:@"user_id"]]]];
   // NSLog(@"rendom  %@:",[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&language_type=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"english"]]]);
    [request setHTTPMethod:@"Post"];
    NSLog(@"random URL %@",request.URL);
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }

}
/*
 Function for task=whats_new&language_type=english
 */
-(void)getWhatsNewWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&language_type=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"language_type"]]]];
 // NSLog(@"rendom  %@:",[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&language_type=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"english"]]]);
    [request setHTTPMethod:@"Post"];
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    if (!connection) {
        NSError *err;
        self.responseBlock(nil,err);
    }


    
}


/*
 * category detailed list of videos
 */
#pragma mark Get Category Detail
-(void)getCategoryDetailWithInfo:(NSMutableDictionary*)infoDict
{
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&category_id=%@&user_id=%@",baseURL,[infoDict valueForKey:@"task"],[infoDict valueForKey:@"category_id"],[infoDict valueForKey:@"user_id"]]]];
    NSLog(@"Request URL is--- %@", request.URL);
    [request setHTTPMethod:@"Post"];
    
    NSURLConnection *connection=[NSURLConnection connectionWithRequest:request delegate:self];
    
    if (!connection)
    {
        NSError *err;
        self.responseBlock(nil,err);
    }
}

#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"connectionDidFailWithError %@",[error localizedDescription]);
    self.responseBlock(nil,error);
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData=[NSMutableData new];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self handleMainResponse:self.responseData];
}
-(void)handleMainResponse:(NSMutableData*)data
{
    NSString *responseString=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    id response=[self jsonParsingWithSting:responseString];
    self.responseBlock(response,nil);
}
-(id)jsonParsingWithSting:(NSString*)resposeString
{
    SBJsonParser *parser=[[SBJsonParser alloc]init];
    id response=[parser objectWithString:resposeString];
    return response;
}
@end
