//
//  KPParser.h
//  Klippat
//
//  Created by Ravi kumar on 24/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

typedef enum
{
    KPSignUpServiceType,
    KPSignInNormalType,
    KPSocialLoginType,
    KPForgotPasswordType,
    KPCreatePlayListType,
    KPAddFileToPlaylistType,
    KPAddFileToPFavouriteType,
    KPGetPlayListType,
    KPGetCategoryListType,
    KPGetFavoriteType,
    KPGetFavoriteDetailType,
    KPGetCategoryDetailType,
    KPGetUserPlaylistFile,
    KPGetPrivacyPolicyType,
    KPGetTermsConditionsType,
    KPSearchServiceType,
    KPChangeNotification,
    KPRandomFilePlayType,
    KPWhatsNewType,

     
    
}ServiceType;
#import <Foundation/Foundation.h>
#import "KPUtility.h"
typedef void(^CompletionBlock) (id response,NSError *error);

@interface KPParser : NSObject
@property(nonatomic,strong)CompletionBlock responseBlock;
@property(nonatomic,strong)NSMutableData *responseData;
@property(nonatomic)ServiceType serviceType;
-(void)serviceRequestWithInfo:(NSMutableDictionary*)infoDictionay serviceType:(ServiceType)type responseBlock:(CompletionBlock)responseBlock;
@end
