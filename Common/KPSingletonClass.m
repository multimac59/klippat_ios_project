//
//  KPSingletonClass.m
//  Klippat
//
//  Created by Ravi kumar on 24/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import "KPSingletonClass.h"

@implementation KPSingletonClass
static KPSingletonClass *sharedMyModel = nil;
@synthesize viaFavourites;
@synthesize viaPlayList;
@synthesize viaDownloads;

+ (id)sharedObject {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

@end
