//
//  DSActivityIndicator.m
//  Domains
//
//  Created by Dinesh Mehta on 8/12/13.
//  Copyright (c) 2013 Prashant Vashisht. All rights reserved.
//

#import "MBProgressHUD.h"
#import "DSActivityIndicator.h"

@implementation DSActivityIndicator
/**
 * Class Method is define for the getting the instanse for the same class
 **/
+(DSActivityIndicator *)progressHudInstance {
    
    if(!self) {
        //@return instance variable for the same class
        return [[DSActivityIndicator alloc] init] ;
    }
    //@return instance variable for the same class
    return (DSActivityIndicator *)self;
}

/**
 * Class Method is define for the getting the instanse for the MBProgressHUD class
 **/
+(MBProgressHUD *)mbprogressHudInstance
{
    //@return instance variable for the MBProgressHUD class
    return [[MBProgressHUD alloc] init] ;
    
}

#pragma mark --
#pragma mark Showing Activity indicator.

/**
 * Method is defining for the showing the activity indicator on view
 **/
void showActivity (UIView *view)
{
    NSLog(@"show HUD");
    //getting instance variable for the MBProgressHUD class for showing the GUI on the view
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    //Set text on the ProgressHud
	hud.labelText = @"Please Wait ...";
    //Method is called for after 120 second for the timeout . If expected result is not occued
//[[DSActivityIndicator progressHudInstance] performSelector:@selector(timeout:) withObject:[UIApplication sharedApplication].keyWindow afterDelay:360.0];
//    UIImage *small = [UIImage imageWithCGImage:[UIImage imageNamed:@""].CGImage scale:0.25 orientation:original.imageOrientation];
}

/**
 * Method is defining for the hidding the activity indicator on view
 **/
void dismissHUD (UIView *arg)
{
    //Calling class method for the MBProgressHUD class for hiding the GUI of the progressHUD on the view
    NSLog(@"hide HUD");
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
}

/**
 * Method is defining for the time out for the activity indicator on view after some time interval
 **/
+ (void)timeout:(UIView *)arg
{
    //Set text on the ProgressHud View
     NSLog(@"Timeout HUD");
    [DSActivityIndicator mbprogressHudInstance].labelText = @"Timeout!";
    //Set Detail text on the ProgressHud View
    [DSActivityIndicator mbprogressHudInstance].detailsLabelText = @"Please try again later.";
    //Set image after time out on the ProgressHud View
    [DSActivityIndicator mbprogressHudInstance].customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] ;
    //Set ProgressHud View mode
	[DSActivityIndicator mbprogressHudInstance].mode = MBProgressHUDModeCustomView;
    //Calling class method for the MBProgressHUD class for hiding the GUI of the progressHUD on the view
	[MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
}


@end
