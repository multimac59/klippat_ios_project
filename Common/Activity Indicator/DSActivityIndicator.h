//
//  DSActivityIndicator.h
//  Domains
//
//  Created by Dinesh Mehta on 8/12/13.
//  Copyright (c) 2013 Prashant Vashisht. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MBProgressHUD;
@interface DSActivityIndicator : NSObject

/**
 * Method is declared for the showing the activity indicator on view
 **/
void showActivity (UIView *view);
/**
 * Method is declared for the hiding the activity indicator on view
 **/
void dismissHUD (UIView *arg) ;
/**
 * Class Method is declared for the getting the instanse for the same class
 **/
+(MBProgressHUD *)progressHudInstance;

@end
