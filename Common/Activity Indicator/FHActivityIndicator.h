//
//  FHActivityIndicator.h
//  FitHealthy
//
//  Created by Komal Kumar on 13/03/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDIndicator.h"

@interface FHActivityIndicator : UIView

@property (nonatomic, strong) DDIndicator *ind;
+(FHActivityIndicator*)sharedMySingleton;

-(void)addIndicator:(UIView *)view;
-(void)removeIndicator;

@end
