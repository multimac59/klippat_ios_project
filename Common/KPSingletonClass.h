//
//  KPSingletonClass.h
//  Klippat
//
//  Created by Ravi kumar on 24/07/14.
//  Copyright (c) 2014 Komal kumar. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KPSingletonClass : NSObject

@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *language;

@property (nonatomic,strong) NSString *viaFavourites;
@property (nonatomic,strong) NSString *viaPlayList;
@property (nonatomic,strong) NSString *viaDownloads;
@property (nonatomic,assign) BOOL isFromFacebook;
@property(nonatomic,assign)BOOL isVideoFullScreen;

+(KPSingletonClass*)sharedObject;
@end
